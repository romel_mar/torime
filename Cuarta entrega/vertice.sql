-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-10-2017 a las 09:47:37
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vertice`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `obtener_privilegios` (`id_cuenta` INT)  BEGIN
SELECT IdPermiso FROM usuario_privilegios WHERE IdUsuario = id_cuenta;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `becario`
--

CREATE TABLE `becario` (
  `IdBecario` int(11) NOT NULL,
  `Folio` int(11) NOT NULL,
  `Nombre` text NOT NULL,
  `ApellidoP` text NOT NULL,
  `ApellidoM` text NOT NULL,
  `CURP` varchar(18) NOT NULL,
  `FechaNacimiento` date NOT NULL,
  `Email` varchar(30) DEFAULT NULL,
  `Estatus` text NOT NULL,
  `Sexo` text NOT NULL,
  `LugarNacimiento` text NOT NULL,
  `Comunidad` text NOT NULL,
  `FechaEntrevista` date DEFAULT NULL,
  `Prioridad` int(11) NOT NULL,
  `Grado` varchar(50) NOT NULL,
  `Escuela` varchar(50) DEFAULT NULL,
  `Nivel` varchar(20) DEFAULT NULL,
  `Carrera` text,
  `PromedioInicial` float NOT NULL,
  `PromedioActual` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `becario`
--

INSERT INTO `becario` (`IdBecario`, `Folio`, `Nombre`, `ApellidoP`, `ApellidoM`, `CURP`, `FechaNacimiento`, `Email`, `Estatus`, `Sexo`, `LugarNacimiento`, `Comunidad`, `FechaEntrevista`, `Prioridad`, `Grado`, `Escuela`, `Nivel`, `Carrera`, `PromedioInicial`, `PromedioActual`) VALUES
(1, 1, 'ROSALÍA', 'BALTAZAR', 'MORALES', 'BAMR011230MQTLRSA4', '2001-07-21', '-', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'EL SAUCITO', '0000-00-00', 2, 'SECUNDARIA TERMINADA', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 7.4, 7),
(2, 2, 'ELIZABETH', 'BLAS', 'DE LEÓN', 'BALE000903MQTLNLA5', '1999-10-05', '', 'Regular', 'Femenino', 'TOLIMAN', 'CERRITO PARADO', '0000-00-00', 1, '2° SEM BACHILLERATO', 'COBAQ 6', '3° SEM', '-', 8.8, 7),
(3, 3, 'NANCY', 'DE LEÓN', 'GONZÁLEZ', 'LEGN000405MQTNNNA3', '2002-05-08', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 2, '4° SEM BACHILLERATO', 'COBAQ 6', '5° SEM', '', 9.29, 9),
(4, 4, 'DANIEL', 'DE LEÓN', 'GUDIÑO', 'LEGD981206HQTNDN04', '2002-04-05', 'danileo1298@gmail.com', 'Baja', 'Masculino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, 'BACHILLERATO TERMINADO', '-', '-', '-', 8.2, 0),
(5, 5, 'JOSÉ RAUL', 'DE LEÓN', 'MORALES', 'LEMR011129HQTNRLA0', '2000-08-18', '-', 'Baja', 'Masculino', 'TOLIMAN', 'MESA DE CHAGOYA', '0000-00-00', 2, '2° SEM BACHILLERATO', 'TELEBACHILLERATO COMUNITARIO', '3° SEM', '-', 7.5, 0),
(6, 6, 'CARLA BIBIANA', 'DE LEÓN', 'SÁNCHEZ', 'LESC990123MQTNNR03', '1994-09-26', '-', 'Baja', 'Femenino', 'CADEREYTA DE MONTES', 'CERRITO PARADO', '0000-00-00', 2, '5° SEM BACHILLERATO', '-', '-', '-', 8.9, 0),
(7, 7, 'VICTOR MANUEL', 'DE LEÓN', 'TREJO', 'LETV010721HQTNRCA7', '2001-09-28', '-', 'Baja', 'Masculino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 7, 0),
(8, 8, 'YESICA', 'GONZALEZ', 'GONZÁLEZ', 'GOGY991005MQTNNS07', '2000-06-15', 'yesigonzalesjgg@gmail.com', 'Regular', 'Femenino', 'GUANAJUATO', 'LOS GONZALEZ', '0000-00-00', 2, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 8.7, 8),
(9, 9, 'MARÍA ELENA', 'DE LEÓN', 'GUDIÑO', 'LEGE020508MNENDLA4', '2001-06-19', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, 'SECUNDARIA TERMINADA', 'EMSAD 24', '1° SEM', '-', 9.8, 8),
(10, 10, 'ALEJANDRA ', 'GUDIÑO ', 'GUDIÑO', 'GUGA020405MQTDDLA9', '2001-07-10', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, 'SECUNDARIA TERMINADA', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 7.2, 7),
(11, 11, 'MELISSA', 'GUDIÑO ', 'GUDIÑO', 'GUGM000818MQTDDLA4', '2000-05-01', 'melinna1212@gmail.com', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 9.14, 9),
(12, 12, 'SOFIA', 'GUDIÑO ', 'GUDIÑO', 'GUGS940926MQTDDF06', '1999-01-14', 'sofiggextr@hotmail.com', 'Baja', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, '2° CUATRI LICENCIATURA', '-', '-', '-', 9.6, 0),
(13, 13, 'DANIELA', 'GUDIÑO ', 'PÉREZ', 'GUPD010928MQTDRNA9', '2001-11-19', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 9.2, 8),
(14, 14, 'ANA CRISTINA', 'GUDIÑO ', 'YATA', 'GUYA000615MGTDTNA2', '2001-06-23', 'cristi.yata.1517@gmail.com', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'MESA DE RAMIREZ', '0000-00-00', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 9, 8),
(15, 15, 'YESSICA', 'LUNA', 'GUDIÑO', 'LUGY010619MQTNDSA5', '2000-10-01', '4424489828', 'Regular', 'Femenino', 'TOLIMAN', 'EL SAUCITO', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 7.1, 7),
(16, 16, 'MARÍA BRISEIDA', 'MONTOYA', 'GUDIÑO', 'MOGB010710MQTNDRA2', '1996-06-12', '-', 'Baja', 'Femenino', 'TOLIMAN', 'LA PRESITA', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8.2, 0),
(17, 17, 'LUIS ALBERTO', 'MORALES ', 'DE LEÓN', 'MOLL000501HQTRNSA0', '1996-12-02', '-', 'Regular', 'Masculino', 'CADEREYTA DE MONTES', 'MESA DE CHAGOYA', '0000-00-00', 1, '4° SEM BACHILLERATO', 'TELEBACHILLERATO COMUNITARIO', '5° SEM', '-', 7.8, 7),
(18, 18, 'AURELIA', 'MORALES', 'GONZÁLEZ', 'MOGA990114MQTRNR02', '2001-09-19', 'aurelitamorales1999@gmail.com', 'Baja', 'Femenino', 'QUERETARO', 'SABINO DE SAN AMBROSIO', '0000-00-00', 1, 'BACHILLERATO TERMINADO', '-', '-', '-', 7.6, 0),
(19, 19, 'FABIOLA', 'MORALES ', 'GUDIÑO', 'MOGF011119MQTRDBA1', '2000-04-19', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, 'SECUNDARIA TERMINADA', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 7, 0),
(20, 20, 'BRENDA EDITH', 'MORALES ', 'GUERRERO', 'MOGB010623MQTRRRA1', '2000-08-15', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 7.8, 8),
(21, 21, 'BERENICE', 'MORALES ', 'MARTÍNEZ', 'MOMB001001MQTRRRB8', '2002-01-20', '-', 'Regular', 'Femenino', 'TOLIMAN', 'CERRITO PARADO', '0000-00-00', 2, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 9.2, 8),
(22, 22, 'BIBIANA', 'MORALES', 'MORALES', 'MOMB960612MQTRRB04', '1999-07-03', 'bsanches12314@gmail.com', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE CHAGOYA', '0000-00-00', 1, '7° SEM INGENIERÍA', 'ITQ-TOLIMÁN', '8° SEM', 'GESTIÓN EMPRESARIAL', 8.65, 8),
(23, 23, 'MARÍA ADELINA', 'MORALES ', 'RESÉNDIZ', 'MORA961202MQTRSD05', '1999-08-22', 'mariaadelinamora1996@gmail.com', 'Baja', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 2, '1° CUATRI LICENCIATURA', '-', '-', '-', 9.1, 0),
(24, 24, 'JOSÉ ANTONIO', 'OLGUIN', 'RESÉNDIZ', 'OURA010919HQTLSNA7', '1995-06-10', '-', 'Baja', 'Masculino', 'TOLIMAN', 'SAN PEDRO DE LOS EUCALIPTOS', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 0, 0),
(25, 25, 'MARÍA DIANELI', 'PÉREZ', 'DE LEÓN', 'PELD000419MQTRNNA2', '2000-04-19', 'dianelipdl@gmail.com', 'Baja', 'Femenino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 7.5, 0),
(26, 26, 'EMMANUEL', 'PÉREZ', 'GUDIÑO', 'PEGE000815HQTRDMA2', '2000-08-15', 'emaperz1508@gmail.com', 'Baja', 'Masculino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 7.49, 0),
(27, 27, 'LUIS OCTAVIO ', 'RESÉNDIZ', 'GONZÁLEZ', 'REGL020120HQTSNSA1', '2002-01-20', '-', 'Condicionado', 'Masculino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, 'PENDIENTE', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 7, 0),
(28, 28, 'LUIS GERARDO', 'RESÉNDIZ', 'MONTOYA', 'REML990703HQTSNS05', '1999-07-03', '-', 'Regular', 'Masculino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, 'BACHILLERATO TERMINADO', 'ITQ-TOLIMÁN', '1° SEM', 'GESTIÓN EMPRESARIAL', 8.6, 8),
(29, 29, 'GRACIELA', 'SÁNCHEZ', 'DE SÁNCHEZ', 'SASG990822MQTNNR09', '1999-08-22', '-', 'Baja', 'Femenino', 'TOLIMAN', 'ZAPOTE DE LOS URIBE', '0000-00-00', 1, 'BACHILLERATO TERMINADO', '-', '-', '', 7.5, 0),
(30, 30, 'MARCOS', 'TREJO', 'SÁNCHEZ', 'TESM950610HQTRNR00', '1995-06-10', 'mtrejo14.depad.tol@gmail.com', 'Con beca', 'Masculino', 'TOLIMAN', 'CERRITO PARADO', '0000-00-00', 2, '7° SEM INGENIERÍA', 'ITQ-TOLIMÁN', '8° SEM', 'SISTEMAS COMPUTACIONALES', 8.8, 8),
(31, 31, 'BEATRIZ', 'BLAS', 'SÁNCHEZ', 'BASB961009MQTLNT04', '1996-10-09', 'betilizblas0910@gmail.com', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'CERRITO PARADO', '2017-08-03', 1, '4° SEM LICENCIATURA', 'UAQ-CADEREYTA', '5° SEM', 'LIC. EN ADMINISTRACIÓN', 8, 8),
(32, 32, 'ALEXANDRO', 'DE SANTIAGO', 'MONTOYA ', 'SAMA001018HQTNNLA7', '2000-10-18', 'alexandrodstgmontoya@gmail.com', 'Regular', 'Masculino', 'TOLIMAN', 'EL CIPRÉS', '2017-08-04', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 7, 7),
(33, 33, 'NELLY VIRIDIANA', 'DE SANTIAGO', 'SÁNCHEZ ', 'SASN010917MQTNNLA9', '2001-09-17', '-', 'Regular', 'Femenino', 'TOLIMAN', 'CASA BLANCA', '2017-08-02', 2, '2° SEM BACHILLERATO', 'COBAQ 6', '3° SEM', '-', 9, 9),
(34, 34, 'MARÍA ISABEL', 'GONZÁLEZ', 'GONZÁLEZ', 'GOGI020118MQTNNSA3', '2002-01-10', '-', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'MESA DE RAMIREZ', '2017-08-14', 1, 'SECUNDARIA TERMINADA', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 6, 6),
(35, 35, 'MARÍA ADELIA', 'GUDIÑO ', 'SÁNCHEZ', 'GUSA011025MQTDNDB8', '2001-10-25', 'mariags1025@a.cobaq.edu.mx', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'MESA DE RAMIREZ', '2017-08-03', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8, 8),
(36, 36, 'MIGUEL ÁNGEL', 'MORALES', 'GUDIÑO', 'MOGM000423HQTRDGA7', '2000-04-23', 'michaelmike8300959@gmail.com', 'Regular', 'Masculino', 'TOLIMAN', 'MESA DE CHAGOYA', '2017-08-02', 1, 'SECUNDARIA TERMINADA', 'EMSAD 24', '1° SEM', '-', 8, 8),
(37, 37, 'CRISTIÁN', 'PÉREZ', 'LOYOLA ', 'PELC980217HQTRYR04', '1998-02-11', 'crpsloyola450@gmail.com', 'Regular', 'Masculino', 'TOLIMAN', 'MAGUEY MANSO', '2007-08-03', 2, 'BACHILLERATO TERMINADO', 'UTQ-QUERÉTARO', '1° SEM', 'MECATRÓNICA', 8, 8),
(38, 38, 'ANA LAURA', 'RESÉNDIZ', 'GUDIÑO', 'REGA020909MQTSDNA5', '2002-09-09', 'lauraresendiz230@gmail.com', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'MESA DE RAMIREZ', '2017-08-04', 1, 'SECUNDARIA TERMINADA', 'EMSAD 24', '1° SEM', '-', 7, 7),
(39, 39, 'EFRAÍN', 'RESÉNDIZ', 'HERNÁNDEZ', 'REHE980313HQTSRF09', '1998-03-13', '', 'Regular', 'Masculino', 'TOLIMAN', 'CERRITO PARADO', '2017-08-14', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8, 8),
(40, 40, 'ROCÍO', 'RESÉNDIZ', 'PÉREZ', 'REPR021215MQTSRCA9', '2002-12-15', '', 'Regular', 'Femenino', 'TOLIMAN', 'SABINO DE SAN AMBROSIO', '2017-08-04', 2, 'SECUNDARIA TERMINADA', 'EMSAD 24', '1° SEM', '-', 7, 7),
(41, 41, 'JESÚS NAZARIO', 'RESÉNDIZ', 'DE SANTIAGO ', 'RESJ001224HQTSNSA0', '2000-12-24', 'jesusrs1224@a.cobaq.edu.mx', 'Regular', 'Masculino', 'TOLIMAN', 'CERRITO PARADO', '2017-08-14', 2, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8, 8),
(42, 42, 'ROSA ELVIA', 'TREJO', 'HERNÁNDEZ', 'TEHR000618MQTRRSA8', '2000-06-18', 'rosath0618@a.cobaq.edu.mx', 'Regular', 'Femenino', 'TOLIMAN', 'CERRITO PARADO', '2017-08-14', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `benefactores`
--

CREATE TABLE `benefactores` (
  `IdBenefactores` int(11) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `ApellidoP` varchar(20) DEFAULT NULL,
  `ApellidoM` varchar(20) DEFAULT NULL,
  `RazonSocial` varchar(50) DEFAULT NULL,
  `FisicaOMoral` varchar(7) DEFAULT NULL,
  `FechaAlta` date DEFAULT NULL,
  `RFC` varchar(15) DEFAULT NULL,
  `Domicilio` varchar(50) DEFAULT NULL,
  `Municipio` varchar(25) DEFAULT NULL,
  `Estado` varchar(20) DEFAULT NULL,
  `Telefono` bigint(20) DEFAULT NULL,
  `EmailBenefactor` varchar(40) DEFAULT NULL,
  `EmailContacto` varchar(40) DEFAULT NULL,
  `RequiereFactura` varchar(2) DEFAULT NULL,
  `PeriodicidadAportacion` varchar(15) DEFAULT NULL,
  `Monto` int(11) DEFAULT NULL,
  `TipoMembresia` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `benefactores`
--

INSERT INTO `benefactores` (`IdBenefactores`, `Nombre`, `ApellidoP`, `ApellidoM`, `RazonSocial`, `FisicaOMoral`, `FechaAlta`, `RFC`, `Domicilio`, `Municipio`, `Estado`, `Telefono`, `EmailBenefactor`, `EmailContacto`, `RequiereFactura`, `PeriodicidadAportacion`, `Monto`, `TipoMembresia`) VALUES
(1, 'María Alejandra', 'Daza', 'Covarrubias', 'Consorcio Invercop S.A. de C.V.', 'Moral', '2016-09-27', 'CIN080826CVA', 'Bosques Chapultepec 307 C Col Arboledas 1era Secci', 'Celaya', 'Guanajuato', 4421863957, 'alejandra.daza@alterra.com.mx ', 'laura.malagon@alterra.me', 'Si', 'Mensual', 1500, 'Emprendedor'),
(2, 'Andres', 'Ortega', 'González', '', 'Fisica', '2016-08-29', 'OEGA49102092A', 'Carrillo 104 Col Villas del Mesón Juriquilla', 'Querétaro', 'Querétaro', 4422426762, 'aortega3322@live.com', 'aragomez.correo@gmail.com', 'Si', 'Mensual', 2000, 'Emprendedor'),
(3, 'Moises', 'Miranda', 'Álvarez', 'Firma Sien S.C.', 'Moral', '2016-08-26', 'FSI140414NJ0', 'Bernardo Quintana 439-404 Col Centro Sur', 'Querétaro', 'Querétaro', 4424021010, 'mma@firmasien.mx', 'storres@firmasien.mx', 'Si', 'Mensual', 5000, 'Corporativo'),
(4, 'Bernardo ', 'Castellanos', '', 'Comunicación Útil S.A. de C.V.', 'Moral', '2017-08-08', 'CUT0811141I5', 'Ret. Farrallón Mz. 2 Lote 8 12 SM 15A', 'Cancún', 'Quintana roo', 9982145938, 'bernardocut@gmail.com', 'castellanos.arturo@hotmail.com ', 'Si', 'Mensual', 1500, 'Emprendedor'),
(5, 'Miguel Ángel', 'Feregrino', 'Feregrino', '', 'Fisica', '2017-01-09', '', '', 'Querétaro', 'Querétaro', 4422499806, 'feregrinoferegrino@yahoo.com.mx', 'feregrinoferegrino@yahoo.com.mx', 'No', 'Mensual', 5000, 'Corporativo'),
(6, 'Jesús Alberto', 'Sánchez', 'Hernández', '', 'Fisica', '2016-08-22', ' SAHJ6311292M6', 'Manuel Herrera 20 Col San Javier', 'Querétaro', 'Querétaro', 4422421360, 'jesus.sanchez@alterra.com.mx', 'jesus.sanchez@alterra.com.mx', 'Si', 'Mensual', 2000, 'Emprendedor'),
(7, 'Gerardo', 'Proal', 'de la Isla', '', 'Fisica', '2016-08-25', 'POIG600420EQ8', 'Blvd Gobernadores 1001- 14 Col MonteBlanco III ', 'Querétaro', 'Querétaro', 4423180380, 'gproal@codigoaureo.com', 'gproal@codigoaureo.com', 'Si', 'Semestral', 6000, 'Emprendedor'),
(8, 'Mario Humberto', 'Paulin', 'Nardoni', 'PR Estudio Legal S.C.', 'Moral', '2017-10-01', 'PEL161123EX5', 'Allende Sur 27 Col Centro', 'Querétaro', 'Querétaro', 4422141476, 'mhp@paulinrivera.com', 'mng@paulinrivera.com', 'Si', 'Mensual', 2500, 'Corporativo'),
(9, 'Arturo Ricardo', 'Olivares ', 'Hernández', 'Clic magnetic eyewear', 'Fisica', '2017-06-07', 'OIHA521005JJ1', 'Avellanas 30 Valle de Los Olivos ', 'Querétaro', 'Querétaro', 4424030430, 'ricardoolivares1@hotmail.com ', 'ricardoolivares1@hotmail.com', 'Si', 'Esporadica', 5000, 'Corporativo'),
(10, 'Francisco Javier ', 'Sánchez', 'Hernández', 'Residencial Alterra I. S.A. de C.V', 'Fisica', '2016-07-07', 'SAHF650122NJ8', 'Río Ayutla 43 Col La Piedad', 'Querétaro', 'Querétaro', 4422421360, 'javier.sanchez@alterra.com.mx', 'juanjose.barrera@alterra.me', 'Si', 'Mensual', 32000, 'Corporativo'),
(20, 'Jorge', 'Sánchez', 'Hernández', '', 'Fisica', '2017-10-01', '', '', 'Querétaro', 'Querétaro', 4424124192, '', ' jackecham@hotmail.com', 'Si', 'Trimestral', 1500, 'Junior'),
(21, 'Omar', 'DeAlba', 'Garza', '', 'Moral', '2016-09-04', 'OIHA521005JJ2', 'Río Ayutla 43 Col La Piedad', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 2800, 'Junior'),
(22, 'Juan', 'Aboytes', 'García', '', 'Fisica', '2017-06-05', 'OIHA521005JJ2', 'Avellanas 30 Valle de Los Olivos ', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 5000, 'Corporativo'),
(23, 'David ', 'Lopez', 'Aboytes', '', 'Moral', '2017-10-09', 'OIHA521005JJ2', 'Avellanas 30 Valle de Los Olivos ', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 3000, 'emprendedor'),
(24, 'Juana', 'Ruiz', 'Dominguez', '', 'Fisica', '2017-06-05', 'OIHA521005JJ2', 'Allende Sur 27 Col Centro', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 504, 'emprendedor'),
(25, 'Rodrigo', 'Garcia', 'Zurita', '', 'Moral', '2008-08-08', 'OIHA521005JJ2', 'Avellanas 30 Valle de Los Olivos ', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 795, 'Junior'),
(26, 'Miguel', 'Alcantar', 'Fernandez', '', 'Fisica', '2017-10-09', 'OIHA521005JJ2', 'Allende Sur 27 Col Centro', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 456, ' emprendedor'),
(27, 'Joaquin', 'Lopez', 'Doriga', '', 'Moral', '2016-09-06', 'OIHA521005JJ2', '', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 123, 'Junior'),
(28, 'Alejandro', 'Fernandez', 'Doriga', '', 'Fisica', '2016-09-04', 'OIHA521005JJ2', 'Río Ayutla 43 Col La Piedad', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 758, 'emprendedor'),
(29, 'Javier', 'Dorantes', 'Zuñiga', '', 'Moral', '2017-10-06', 'OIHA521005JJ2', '', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 988, 'Junior'),
(30, 'Ricardo', 'Cortes ', 'Martinez', '', 'Fisica', '2016-08-07', 'OIHA521005JJ2', 'Río Ayutla 43 Col La Piedad', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 566, 'Junior'),
(40, 'Eduardo', 'Tejada', 'Flores', '', '', '2017-11-09', 'OIHA521005JJ2', '', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 266, 'emprendedor'),
(41, 'David ', 'Martinez', 'Olvera', '', '', '2017-12-09', 'OIHA521005JJ2', 'Blvd Gobernadores 1001- 14 Col MonteBlanco III ', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 233, 'Junior');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `beneficio`
--

CREATE TABLE `beneficio` (
  `IdBeneficio` int(11) NOT NULL,
  `NomBeneficio` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `beneficio`
--

INSERT INTO `beneficio` (`IdBeneficio`, `NomBeneficio`) VALUES
(1, 'PROSPERA'),
(2, 'TSUNI'),
(3, 'PAL'),
(4, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boletines`
--

CREATE TABLE `boletines` (
  `IdArchivo` int(11) NOT NULL,
  `Archivo` varchar(250) NOT NULL,
  `NombreArchivo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `boletines`
--

INSERT INTO `boletines` (`IdArchivo`, `Archivo`, `NombreArchivo`) VALUES
(13, 'boletines/CV.pdf', 'CV.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colaboradores`
--

CREATE TABLE `colaboradores` (
  `IdColaborador` int(11) NOT NULL,
  `Nombre` varchar(30) DEFAULT NULL,
  `ApellidoP` varchar(20) DEFAULT NULL,
  `ApellidoM` varchar(20) DEFAULT NULL,
  `Institucion` varchar(50) DEFAULT NULL,
  `Estatus` varchar(15) DEFAULT NULL,
  `Area` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `colaboradores`
--

INSERT INTO `colaboradores` (`IdColaborador`, `Nombre`, `ApellidoP`, `ApellidoM`, `Institucion`, `Estatus`, `Area`) VALUES
(1, 'Juanita', 'Perez', 'Martinez', 'Vertice', 'Trabajando', 'Comunicación'),
(2, 'Ana Laura', 'Herrera', 'de la Cruz', 'Vertice', 'No Trabajando', 'Informatica'),
(3, 'Maria', 'Garcia', 'Brumas', 'Vertice', 'Trabajando', 'Comunicación'),
(4, 'Francisco', 'Ramirez', 'Ramirez', 'Vertice', 'Trabajando', 'Director'),
(5, 'Luis', 'Canales', 'Castro', 'Vertice', 'No Trabajando', 'Comunicación'),
(6, 'Akel', 'Rico', 'Madrazo', 'Vertice', 'Trabajando', 'Seguridad'),
(7, 'Eluciano', 'Castro', 'Rico', 'Vertice', 'Trabajando', 'Informatica'),
(8, 'Tulipano', 'Garza', 'Garcia', 'Vertice', 'No Trabajando', 'Comunicación'),
(9, 'Elhubiera', 'de la Rosa', 'de Leon', 'Vertice', 'No Trabajando', 'Comunicación'),
(10, 'Esteban', 'Kito', 'Roto', 'Vertice', 'Trabajando', 'Seguridad'),
(13, 'Never Gonna', 'Give', 'You Up', 'Tu Corazon', 'No Trabajando', 'Comedia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentacion`
--

CREATE TABLE `documentacion` (
  `IdDoc` int(11) NOT NULL,
  `Nombre` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `documentacion`
--

INSERT INTO `documentacion` (`IdDoc`, `Nombre`) VALUES
(1, 'Acta de Nacimiento'),
(2, 'CURP'),
(3, 'Certificado secundaria'),
(4, 'Carta de maestros'),
(5, 'Constancia de recursos economicos'),
(6, 'Carta de padres de familia'),
(7, 'INE'),
(8, 'CURP Madre o Padre'),
(9, 'Boletas de calificaciones actualizada'),
(10, 'Constancia de inscripcion en la escuela'),
(11, 'Dictamen medico'),
(12, 'Constrancia trabajo comunitario'),
(13, 'Una fotografia'),
(14, 'Copia de credencial de fundacion'),
(15, 'Convenio Actualizado'),
(16, 'Tarjeta de debito'),
(17, 'Carta compromiso servicio comunitario'),
(18, 'Evaluacion de cumplimiento de convenio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enfermedad`
--

CREATE TABLE `enfermedad` (
  `IdBecario` int(11) NOT NULL,
  `IdEnfermedad` int(11) NOT NULL,
  `HayEnfermedad` text,
  `FamiliaEnfermo` text,
  `Descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `enfermedad`
--

INSERT INTO `enfermedad` (`IdBecario`, `IdEnfermedad`, `HayEnfermedad`, `FamiliaEnfermo`, `Descripcion`) VALUES
(1, 1, 'NO', '', ''),
(2, 2, 'NO', '', ''),
(3, 3, 'NO', '', ''),
(4, 4, 'NO', '', ''),
(5, 5, '', '', ''),
(6, 6, 'NO', '', ''),
(7, 7, 'NO', '', ''),
(8, 8, 'NO', '', ''),
(9, 9, 'NO', '', ''),
(10, 10, 'SÍ', 'BENEFICIARIA', 'AUDITIVA Y DEL HABLA'),
(11, 11, 'NO', '', ''),
(12, 12, 'NO', '', ''),
(13, 13, 'SÍ', 'ABUELO', 'AUDITIVO'),
(14, 14, 'SÍ', 'HERMANO MENOR', 'DEFORMACIÓN DEL OÍDO '),
(15, 15, 'NO', '', ''),
(16, 16, 'SÍ', 'BENEFICIARIA', 'ASMA'),
(17, 17, 'NO', '', ''),
(18, 18, 'SÍ', 'MADRE Y PADRE', 'DIABETES'),
(19, 19, 'NO', '', ''),
(20, 20, 'SÍ', 'PADRE', 'POSIBLE GASTRITIS'),
(21, 21, 'SÍ', 'ABUELA', 'DIABETES'),
(22, 22, 'NO', '', ''),
(23, 23, 'NO', '', ''),
(24, 24, 'NO', '', ''),
(25, 25, 'SÍ', 'PADRE', 'DOLOR MUSCULAR'),
(26, 26, 'SÍ', 'MADRE Y BENEFICIARIO', 'ASMA'),
(27, 27, 'NO', '', ''),
(28, 28, 'NO', '', ''),
(29, 29, '', '', ''),
(30, 30, 'SÍ', 'BENEFICIARIO', 'VITILIGIO'),
(31, 31, 'NO', '', ''),
(32, 32, 'SÍ', 'ABUELA', 'DIABETES'),
(33, 33, 'NO', '', ''),
(34, 34, 'SÍ', 'TÍO', 'DISCAPACIDAD INTELECTUAL'),
(35, 35, 'SÍ', 'PAPÁ', 'DIABETES'),
(36, 36, 'NO', '', ''),
(37, 37, 'NO', '', ''),
(38, 38, 'NO', '', ''),
(39, 39, 'NO', '', ''),
(40, 40, 'NO', '', ''),
(41, 41, 'NO', '', ''),
(42, 42, 'SÍ', 'PAPÁ', 'FRACTURA DE MANO NO PUEDE LEVANTAR COSAS PESADAS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etnia`
--

CREATE TABLE `etnia` (
  `IdBecario` int(11) NOT NULL,
  `IdEtnia` int(11) NOT NULL,
  `HablaLengua` text,
  `Etnia` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `etnia`
--

INSERT INTO `etnia` (`IdBecario`, `IdEtnia`, `HablaLengua`, `Etnia`) VALUES
(1, 1, 'SÍ', 'OTOMI'),
(2, 2, 'SÍ', 'OTOMI'),
(3, 3, 'SÍ', 'OTOMI'),
(4, 4, 'SÍ', 'OTOMI'),
(5, 5, 'NO', ''),
(6, 6, 'SÍ', 'OTOMI'),
(7, 7, 'SÍ', 'OTOMI'),
(8, 8, 'SÍ', 'OTOMI'),
(9, 9, 'NO', ''),
(10, 10, 'SÍ', 'OTOMI'),
(11, 11, 'SÍ', 'OTOMI'),
(12, 12, 'SÍ', 'OTOMI'),
(13, 13, 'SÍ', 'OTOMI'),
(14, 14, 'SÍ', 'OTOMI'),
(15, 15, 'SÍ', 'OTOMI'),
(16, 16, 'SÍ', 'OTOMI'),
(17, 17, 'SÍ', 'OTOMI'),
(18, 18, 'SÍ', 'OTOMI'),
(19, 19, 'SÍ', 'OTOMI'),
(20, 20, 'SÍ', 'OTOMI'),
(21, 21, 'SÍ', 'OTOMI'),
(22, 22, 'SÍ', 'OTOMI'),
(23, 23, 'SÍ', 'OTOMI'),
(24, 24, 'NO', ''),
(25, 25, 'SÍ', 'OTOMI'),
(26, 26, 'SÍ', 'OTOMI'),
(27, 27, 'SÍ', 'OTOMI'),
(28, 28, 'SÍ', 'OTOMI'),
(29, 29, 'SÍ', 'OTOMI'),
(30, 30, 'SÍ', 'OTOMI'),
(31, 31, 'SÍ', 'OTOMI'),
(32, 32, 'SÍ', 'OTOMI'),
(33, 33, 'SÍ', 'OTOMI'),
(34, 34, 'SÍ', 'OTOMI'),
(35, 35, 'SÍ', 'OTOMI'),
(36, 36, 'SÍ', 'OTOMI'),
(37, 37, 'SÍ', 'OTOMI'),
(38, 38, 'SÍ', 'OTOMI'),
(39, 39, 'SÍ', 'OTOMI'),
(40, 40, 'SÍ', 'OTOMI'),
(41, 41, 'SÍ', 'OTOMI'),
(42, 42, 'SÍ', 'OTOMI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

CREATE TABLE `eventos` (
  `IdEvento` int(11) NOT NULL,
  `Nombre` text,
  `Descripcion` longtext,
  `Direccion` varchar(250) DEFAULT NULL,
  `FechaInicio` varchar(25) DEFAULT NULL,
  `FechaFin` varchar(25) DEFAULT NULL,
  `HoraInicio` varchar(20) DEFAULT NULL,
  `HoraFin` varchar(20) DEFAULT NULL,
  `Foto` varchar(100) DEFAULT NULL,
  `NombreArchivo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `eventos`
--

INSERT INTO `eventos` (`IdEvento`, `Nombre`, `Descripcion`, `Direccion`, `FechaInicio`, `FechaFin`, `HoraInicio`, `HoraFin`, `Foto`, `NombreArchivo`) VALUES
(2, 'Feria del libro', 'Feria del libro Feria del libro Feria del libroFeria del libro999999\r\nFeria del libro\r\nFeria del libro\r\nFeria del libro\r\nFeria del libro', 'Felipe Angeles 420', '30 October, 2017', '31 October, 2017', '08:30 pm', '10:30 pm', '../IMAGENES/eventos/s-l300.jpg', 's-l300.jpg'),
(5, 'Feria del libro', 'Aqui va descripcion del evento', 'Jesus Oviedo #102', '23 October, 2017', '26 October, 2017', '08:30 pm', '10:02 am', '../IMAGENES/eventos/salty.jpg', 'salty.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familia`
--

CREATE TABLE `familia` (
  `IdBecario` int(11) NOT NULL,
  `IdFamilia` int(11) NOT NULL,
  `IngresoMin` int(11) DEFAULT NULL,
  `IngresoMax` int(11) DEFAULT NULL,
  `Mama` int(11) DEFAULT NULL,
  `Papa` int(11) DEFAULT NULL,
  `Hermanos` int(11) DEFAULT NULL,
  `Hijos` int(11) DEFAULT NULL,
  `Abuela` int(11) DEFAULT NULL,
  `Abuelo` int(11) DEFAULT NULL,
  `Otros` int(11) DEFAULT NULL,
  `Descripcion` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `familia`
--

INSERT INTO `familia` (`IdBecario`, `IdFamilia`, `IngresoMin`, `IngresoMax`, `Mama`, `Papa`, `Hermanos`, `Hijos`, `Abuela`, `Abuelo`, `Otros`, `Descripcion`) VALUES
(1, 1, 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'Originaria de El Saucito, Tolimán, su familia es de bajos recursos económicos, su papá está lastimado de una pierna y por lo mismo no puede trabajar regularmente, ella necesita la beca para seguir estudiando y en un futuro entrar al bachillerato.'),
(2, 2, 1, 1500, 1, 1, 4, 0, 0, 0, 0, 'Ha sido difícil mantenerse en la escuela ya que su papá no tiene un trabajo fijo y no puede darle el apoyo económico que requiere para su escuela. Su casa está construida de block y lámina únicamente y su familia es numerosa. '),
(3, 3, 1, 1500, 1, 1, 1, 0, 0, 0, 0, 'Nancy es una chica de 16 años que es apasionada por la lectura, le gusta mucho la ciencia ficción y demuestra mucho interés por continuar estudiando, lleva un buen promedio en la escuela, 9.2, ella utilizaría la beca para cubrir sus gastos escolares, por ejemplo el transporte ya que menciona que hace 55 minutos de su casa a la escuela.'),
(4, 4, 1, 1500, 1, 1, 2, 0, 1, 1, 0, 'Vive en la comunidad de Maguey Manso, Tolimán, su papá murió cuando tenía 4 años de edad, desde entonces su mamá ha tenido que cubrir los gastos de la casa, Daniel tuvo que abandonar sus estudios para venir a trabajar a la ciudad de Querétaro, actualmente los retomó y está cursando el 3er semestre de bachillerato, le gustaría estudiar Ingeniería Civil ya que le apasiona el tema de la construcción.'),
(5, 5, 1, 1500, 0, 0, 0, 0, 0, 0, 0, 'José Raúl es de la comunidad Mesa de Chagoya, Tolimán, durante su tiempo libre trabaja en las tierras de la familia para apoyar a su papá, el único proveedor es su papá y gana muy poco ya que es jornalero, camina diariamente una hora para poder llegar a su escuela, ocuparía la beca para los gastos de la familia y para sus estudios.'),
(6, 6, 3000, 4500, 1, 1, 8, 0, 0, 0, 0, 'Bibiana es la hija mayor y  tiene 8 hermanos, tiene 17 años y está estudiando el último año del bachillerato, tiene un promedio de 9, le gustaría estudiar medicina, por lo mismo que son 11 integrantes de la familia lo que tienen no les alcanza, su papá trabaja de jornalero y depende si hay siembra para trabajar, su mamá sale a trabajar a hacer limpieza y vende productos de catálogo. '),
(7, 7, 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'En su tiempo libre trabaja de jornalero ya que no alcanza el gasto en su familia, su mamá hace limpieza en casas y su papá no puede trabajar mucho ya que está impedido, a Víctor le gusta mucho estudiar, su materia favorita son las matemáticas y en un futuro le gustaría ser ingeniero.'),
(8, 8, 3000, 4500, 1, 1, 3, 0, 0, 0, 0, 'Su materias preferida es historia porque le gusta aprender sobre su cultura y su lengua, a ella le gustaría estudiar Derecho porque quiere ayudar a su comunidad pero continuar estudiando ha sido difícil ya que no cuenta con los recursos necesarios para cubrir sus gastos escolares, incluso tiene que caminar una hora a la escuela pues no puede pagar el transporte que la acerca a la escuela. '),
(9, 9, 1, 1500, 1, 0, 1, 0, 0, 0, 0, 'Vive en condiciones adversas puesto su padre las abandonó desde hace tiempo y ahora tiene que apoyar a su mamá y a su hermana para que las 3 puedan salir adelante.'),
(10, 10, 4500, 20000, 0, 0, 0, 0, 0, 0, 0, 'Vive en Mesa de Ramírez, Tolimán, ella tiene una condición de discapacidad que le impide escuchar y hablar bien, su mamá lee poco y no sabe escribir muy bien, su papá trabaja en la ciudad de Querétaro pero apenas les alcanza para solventar sus gastos más básicos.'),
(11, 11, 3000, 4500, 1, 1, 3, 0, 0, 0, 0, 'Melissa es originaria de una comunidad indígena de Tolimán, habla el otomí igual que su familia, le gusta mucho ir a la escuela, actualmente cursa el 5to semestre del bachillerato, le gusta aprender idiomas, sus papás le apoyan a continuar sus estudios, ella quiere estudiar la universidad.'),
(12, 12, 1, 1500, 1, 1, 1, 0, 0, 0, 0, 'Sofía siempre ha sido una chica muy estudiosa y con buenas calificaciones, sus padres están orgullosos de ella y sus maestros manifiestan que tiene gran potencial. Sin embargo, no cuenta con los recursos suficientes para continuar sus estudios, ni recibe el apoyo económico de nadie para solventar los gastos que ello conlleva, por lo que trabaja de empleada doméstica para pagar sus estudios. '),
(13, 13, 3000, 4500, 1, 1, 5, 0, 0, 1, 1, 'En su casa viven 10 personas: sus papás, sus hermanos, una tía, su abuelo y ella. Sus papás la animan a seguir estudiando aunque no tienen los recursos suficientes para apoyarla económicamente. De hecho, Daniela es una inspiración para su familia, ya que sus ganas de continuar la escuela y el esfuerzo que realiza para mantener sus buenas calificaciones motivan a sus papás y a sus hermanos para salir adelante y mejorar en lo que hacen día con día.'),
(14, 14, 3000, 4500, 1, 1, 2, 0, 0, 0, 1, 'Le echa muchas ganas a la escuela y menciona que le gustaría estudiar Ingeniería electrónica, proviene de una familia indígena, su hermano pequeño nació con una deformación en el oído lo que ha orillado a su familia a tener que hacer gastos médicos importantes, su tía que es una persona de 3era edad vive con ellos en casa y su papá es el único proveedor. Está embarazada y tiene el apoyo de su familia para seguir estudiando'),
(15, 15, 1, 1500, 1, 1, 3, 1, 0, 0, 1, 'Vive en la comunidad de El Saucito, en Tolimán junto con sus papás y sus 3 hermanos. Ella es madre soltera y estudia el 3er semestre de bachillerato y tiene 16 años, le gustan las matemáticas y le encantaría estudiar Derecho si pudiera estudiar la universidad, pero en ocasiones sus papás no pueden pagar los gastos de ella y sus hermanos.'),
(16, 16, 1, 1500, 1, 1, 4, 0, 1, 1, 0, 'Para llegar a su escuela tiene que caminar una hora de ida y otra de regreso ya que no le alcanza para el transporte, ella padece asma la cual está controlada por el médico.'),
(17, 17, 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'Es originario de la comunidad Mesa de Chagoya, Tolimán, tiene 16 años y estudia en el Telebachillerato comunitario de Mesa de Ramírez, para llegar a su escuela tiene que caminar media hora, habla Otomí, su casa es muy precaria, está hecha de piedra y láminas, refiere que no les alcanza para los gastos básicos, Luis Alberto es muy tímido, él se compromete a poner su mayor esfuerzo para continuar estudiando.'),
(18, 18, 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'Vive en la comunidad de Sabino de San Ambrosio, en Tolimán, tiene 17 años y le falta un año para concluir el bachillerato. A ella le gusta mucho la escuela pero no cuenta con los recursos suficientes para costear los gastos escolares ya que su papá y su mamá se encuentran enfermos y no pueden trabajar, por lo que ella trabaja en vacaciones para juntar los recursos necesarios para pagar su escuela. A Aurelia le gustaría estudiar Medicina o Enfermería para ayudar a sus padres, tanto en los cuidados que requieren como económicamente.'),
(19, 19, 1, 1500, 0, 0, 0, 0, 0, 0, 0, 'Ella camina a su escuela diariamente, en total son 8 personas las que viven en una casa con dos cuartos y una cocina, es un poco tímida, tiene muchas ganas de seguir estudiando como su hermana que está en el bachillerato.'),
(20, 20, 1501, 3000, 1, 1, 5, 0, 0, 0, 0, 'Vive en la comunidad de Mesa de Ramírez, en Tolimán y le gustaría estudiar algún día Ingeniería en Sistemas pero sus padres son de escasos recursos y les cuesta trabajo apoyar a ella y sus 5 hermanos en sus estudios. Además, su papá padece de una enfermedad delicada que le impide trabajar continuamente y es el único sustento de la familia. Sin embargo Brenda se encuentra muy comprometida con su comunidad y realiza actividades de recolección de pet, aseo de calles y ayuda a personas a que aprendan a leer y a escribir en el INEA. '),
(21, 21, 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'A ella le gustan las ciencias de la salud y le encantaría estudiar más adelante la Licenciatura en Criminología. Sin embargo, no cuenta con los recursos necesarios puesto que su papá se encuentra desempleado y es difícil para su familia solventar los gastos escolares de ella y de sus 2 hermanos. '),
(22, 22, 1, 1500, 1, 1, 1, 0, 0, 0, 0, 'Ella es originaria de la comunidad Mesa de Chagoya, tiene 21 años, habla otomí, estudia la Ingeniería en Gestión Empresarial en el ITQ, va muy bien en la escuela, su familia es de escasos recursos económicos por lo que no les alcanza para su transporte y sus comidas, su casa está en obra negra, menciona que cuando tiene que ir a la escuela a veces se queda con hambre y se espera a llegar a su casa por la tarde para poder comer lo que haya.'),
(23, 23, 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'Quiere ayudar a su comunidad y defender sus derechos. Entre semana trabaja como empleada doméstica en Tolimán para cubrir sus gastos escolares pues sus padres no pueden apoyarla económicamente porque también apoyan a sus hermanitos.'),
(24, 24, 1501, 3000, 1, 0, 2, 0, 0, 0, 0, 'Es de la comunidad de San Pedro de los Eucaliptos, en Tolimán y vive con su madre y sus dos hermanos únicamente puesto que su padre falleció. Su hermano mayor tuvo que abandonar la escuela para apoyar a su familia y junto su mamá ha sacado a su familia adelante. Él quiere estudiar para mejorar sus condiciones y dar una mejor vida a su familia. '),
(25, 25, 4500, 20000, 0, 0, 0, 0, 0, 0, 0, 'Estudia y trabaja, para ir al bachillerato toma un camión y hace 30 minutos de traslado, a veces se va caminando y hace una hora, su papá de Dianeli tuvo un accidente y eso le impide trabajar, era jornalero, ahora la han pasado muy difícil pues el dinero que llega a la casa no alcanza, su hermano es quien trabaja, y ella también lo ha tenido que hacer.'),
(26, 26, 1501, 3000, 1, 1, 5, 0, 0, 0, 0, 'La escuela lo ha motivado mucho a superarse y mejorar sus condiciones de vida, ya que su mamá y él padecen de asma, su papá es albañil y al tener 5 hermanos más, sus recursos son muy escasos y el mismo tiene que solventar sus gastos escolares trabajando en el campo los fines de semana. '),
(27, 27, 1, 1500, 1, 1, 3, 0, 1, 0, 0, 'A Luis le interesa seguir estudiando y mejorar su promedio, pero debido a que su familia es numerosa y sus padres no cuentan con recursos suficientes en vacaciones él va a trabajar como jornalero para juntar el dinero que necesita y cubrir sus gastos de inscripciones y útiles escolares, y que así sus papás puedan apoyar a sus otros hermanos. '),
(28, 28, 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'Estudia el 1° semestre de la Ingeniería en Gestión Empresarial en el ITQ de Tolimán, y durante los fines de semana y vacaciones él sale a trabajar como jornalero o chalan de albañil, va muy bien en la escuela.'),
(29, 29, 1, 1500, 0, 0, 0, 0, 0, 0, 0, 'Su materia favorita es biología y en un futuro le gustaría estudiar gastronomía, ella no tiene el apoyo de su papá para seguir estudiando, únicamente es su mamá quien la respalda, sin embargo tiene muchas ganas de estudiar y salir adelante, ella trabaja como jornalera, su casa tiene 2 cuartos, para llegar a su escuela tiene que caminar una hora. Necesita mucho el apoyo.'),
(30, 30, 1501, 3000, 1, 1, 3, 0, 0, 0, 0, 'Marcos estudia Ingeniería en Sistemas Computacionales, a él le gusta el tema de la programación, se ha dedicado a buscar apoyos ya que en un futuro le gustaría establecer un negocio propio, actualmente ha tenido muchas dificultades para continuar estudiando ya que su familia es de escasos recursos económicos, su papá es jornalero y gana muy poco, Marcos tiene un problema en la piel (vitíligo) lo que le impide trabajar en los empleos que normalmente ofertan ya que son bajo el sol y esto le afecta en su salud, aun así ha trabajado en el mantenimiento de las carreteras a causa de la necesidad.'),
(31, 31, 3000, 4500, 0, 1, 1, 0, 0, 0, 3, 'En su familia, la encargada de hacer la comida es su hermana, su madre falleció. Beatriz se encarga de la limpieza del hogar, su padre y su cuñado aportan al aseo de la casa también. Tiene dos sobrinos que los siente como hermanos, ellos son pequeños y no aportan mucho al aseo. Ella considera que su cuñado es como su segundo padre pues la apoya para seguir estudiando. Ella es tía de Elizabeth Blas, FAMeficiaria del programa, y por eso se enteró de la beca.'),
(32, 32, 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'Su hermana menor falleció de un problema del corazón. Actualmente él le ayuda a su mamá a las labores del hogar cuando está en la escuela y cuando no, él trabaja de chalán de albañil o en el campo.'),
(33, 33, 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'En su familia se llevan muy bien, hay respeto y convivencia. Sus materias favoritas son la lectura y redacción y la educación física, le gustaría estudiar pediatría o ginecología. Comparten casa con su abuela paterna; ellos viven en dos cuartos'),
(34, 34, 1501, 3000, 1, 1, 1, 0, 1, 0, 1, 'Le interesa la repostería. Su tío tiene discapacidad intelectual y vive con la familia de ella. Le gustan las materias de español, química y formación cívica y ética.'),
(35, 35, 1, 1500, 0, 0, 0, 0, 1, 1, 2, 'Ella se refiere a sus abuelos como sus papás, su madre la abandonó y vive en otra comunidad, no sabe quién es su padre. Su abuelo es el ínico que aporta al gasto de la familia. Ella quiere estudiar corte y confección porque le gusta la idea de crear y hacer cosas nuevas, combinar colores y texturas.'),
(36, 36, 3000, 4500, 0, 0, 0, 0, 0, 0, 0, 'Dejó de estudiar 2 añospara ayudar económicamente a su familia pero tuvo un accidente laboral, se fracturó la férula, así que hoy su familia se une para que él pueda retomar sus estudios. En verdad le gusta mucho estudiar y está muy emocionado por volver a hacerlo. Quiere hacer un proyecto en los hospitales en el que pueda motivas a las personas que se encuentran ahí a causa de algún accidente.'),
(37, 37, 1501, 3000, 0, 0, 0, 0, 0, 0, 0, 'A él le apasiona todo lo relacionado con los aparatos electrónicos y la automatización. Su papá y su hermano son los únicos que aportan económicamente a su hogar, tiene mucha relción con su hermano y no hace referencia a otros familiares. Él vivirpa en Querétaro para hacer su carrera. Por ahora tiene un empleo que se relaciona un poco con su carrera.'),
(38, 38, 1, 1500, 1, 0, 4, 0, 0, 0, 0, 'Quienes trabajan son sus hermanos mayores. Le gusta mucho la materia de matemáticas y química y le gustaría ser ingeniera porque le llama la atención poder construir cosas.'),
(39, 39, 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'Efraín dejó sus estudios en el 2014 por falta de recursos económicos y durante ese tiempo trabajó en la obra a apoyó a su familia. Su hermana ayor apoya a las labores del hogar y su hermano mayor trabaja auque no aporta diner, más bien él se cubre sus propios gastos. Le gustaría estudiar ingeniería civil.'),
(40, 40, 1, 1500, 1, 1, 1, 0, 0, 0, 0, 'Le gustan las materias de españo, química e historia, quiere ser doctora para ayudar a las personas porque en el centro de salud no hay dotores con frecuencia .'),
(41, 41, 1501, 3000, 0, 0, 0, 0, 0, 0, 0, 'Quiere estudiar ingeniería mecatrónica, está muy interesado en el funcionamiento de las máquinas ya que ha escuchado que algunos mexicanos han inventado han inventado varios robots que han hecho un gran avance en México, él quiere hacer un gran invento un día. Solamente aporta dinero su padre, y Jesús en vacaciones, es un chico motivado y trabajador con una familia cariñosa y que le brinda mucho apoyo.'),
(42, 42, 1, 1500, 1, 1, 3, 0, 0, 0, 1, 'Su padre le ayuda con los alimentos y los útiles, su hermano aporta muy poco y sus hermanas viven aparte. Quiere ser abogada o estudiar ingeniería en sistemas. En su familia tienen problemas de despojos de terrenos. Su hermano mayor tiene problemas de alcoholismo. Elvia es creativa y soñadora.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informes`
--

CREATE TABLE `informes` (
  `IdInforme` int(11) NOT NULL,
  `Archivo` varchar(250) NOT NULL,
  `NombreArchivo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `informes`
--

INSERT INTO `informes` (`IdInforme`, `Archivo`, `NombreArchivo`) VALUES
(27, 'informes/CV.pdf', 'CV.pdf'),
(28, 'informes/Howalgorithmsruletheworld.pdf', 'Howalgorithmsruletheworld.pdf'),
(29, 'informes/Howalgorithmsruletheworld.pdf', 'Howalgorithmsruletheworld.pdf'),
(30, 'informes/How algorithms rule the world.pdf', 'How algorithms rule the world.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nota`
--

CREATE TABLE `nota` (
  `IdBecario` int(11) NOT NULL,
  `IdNota` int(11) NOT NULL,
  `Descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nota`
--

INSERT INTO `nota` (`IdBecario`, `IdNota`, `Descripcion`) VALUES
(1, 1, 'SOLICITAR CERTIFICADO DE SECUNDARIA'),
(2, 2, ''),
(3, 3, ''),
(4, 4, 'NO RENOVÓ BECA. NO SE CONOCE LA RAZÓN'),
(5, 5, 'NO RENOVÓ BECA. NO SE CONOCE LA RAZÓN'),
(6, 6, 'BAJA POR ABANDONO ESCOLAR'),
(7, 7, 'SIN DERECHO A RENOVACIÓN POR INCUMPLIMIENTO DEL CONVENIO'),
(8, 8, ''),
(9, 9, ''),
(10, 10, ''),
(11, 11, ''),
(12, 12, 'BAJA POR ABANDONO ESCOLAR'),
(13, 13, ''),
(14, 14, ''),
(15, 15, ''),
(16, 16, 'SIN DERECHO A RENOVACIÓN POR INCUMPLIMIENTO DEL CONVENIO'),
(17, 17, ''),
(18, 18, 'NO RENOVÓ BECA. NO SE CONOCE LA RAZÓN'),
(19, 19, ''),
(20, 20, ''),
(21, 21, ''),
(22, 22, ''),
(23, 23, 'BAJA POR ABANDONO ESCOLAR'),
(24, 24, 'SIN DERECHO A RENOVACIÓN POR INCUMPLIMIENTO DEL CONVENIO'),
(25, 25, 'NO RENOVÓ BECA. NO SE CONOCE LA RAZÓN'),
(26, 26, 'SIN DERECHO A RENOVACIÓN POR INCUMPLIMIENTO DEL CONVENIO'),
(27, 27, 'DEBE UNA MATERIA DE SECUNDARIA. SE DARÁ PRÓRROGA PARA ENTREGAR EL CERTIFICADO'),
(28, 28, ''),
(29, 29, 'NO RENOVÓ BECA. NO SE CONOCE LA RAZÓN'),
(30, 30, ''),
(31, 31, 'AÚN NO PAGA LA REINSCRIPCIÓN A LA ESCUELA PUES LE DIERON PRÓRROGA PARA HACERLO AUNQUE YA ESTÁ INSCRITA'),
(32, 32, ''),
(33, 33, ''),
(34, 34, ''),
(35, 35, ''),
(36, 36, ''),
(37, 37, ''),
(38, 38, ''),
(39, 39, 'PEDIR COPIA DEL INE PARA REALIZAR TRÁMITE DE TARJETA DEL BANCO'),
(40, 40, ''),
(41, 41, ''),
(42, 42, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `IdNoticias` int(11) NOT NULL,
  `Nombre` text,
  `Descripcion` longtext,
  `Fecha` varchar(25) DEFAULT NULL,
  `Tag` text,
  `Foto` varchar(100) DEFAULT NULL,
  `NombreArchivo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`IdNoticias`, `Nombre`, `Descripcion`, `Fecha`, `Tag`, `Foto`, `NombreArchivo`) VALUES
(7, 'Noticia', 'sdasdasdas', '23 October, 2017', 'Peter', 'noticias/zankyou.png', 'zankyou.png'),
(8, 'Noticia 2 ', 'Otra noticia ', '23 October, 2017', 'Cultura', 'noticias/salty.jpg', 'salty.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `privilegios`
--

CREATE TABLE `privilegios` (
  `IdPermiso` int(11) NOT NULL,
  `Permiso` varchar(25) DEFAULT NULL,
  `Descripcion` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `privilegios`
--

INSERT INTO `privilegios` (`IdPermiso`, `Permiso`, `Descripcion`) VALUES
(1, 'Subir Informe', 'El usuario con este privilegio sera capaz de Subir informes'),
(2, 'Editar Informe', 'El usuario con este privilegio sera capaz de Editar Informes'),
(3, 'Eliminar Informe', 'El usuario con este privilegio sera capaz de Elminar Informes'),
(4, 'Registrar Colaborador', 'El usuario con este privilegio sera capaz de Registrar Colaboradores'),
(5, 'Editar Colaborador', 'El usuario con este privilegio sera capaz de Editar informacion de Colaboradores'),
(6, 'Eliminar Colaborador', 'El usuario con este privilegio sera capaz de Eliminar Colaboradores'),
(7, 'Registrar Boletin', 'El usuario con este privilegio sera capaz de Registrar Boletines'),
(8, 'Editar Boletin', 'El usuario con este privilegio sera capaz de Editar Boletines'),
(9, 'Eliminar Boletin', 'El usuario con este privilegio sera capaz de Eliminar Boletines'),
(10, 'Registrar Noticia', 'El usuario con este privilegio sera capaz de Registrar nuevas noticias'),
(11, 'Editar Noticia', 'El usuario con este privilegio sera capaz de Editar informacion de Noticias'),
(12, 'Eliminar Noticia', 'El usuario con este privilegio sera capaz de Eliminar Noticias'),
(13, 'Registrar Actividad', 'El usuario con este privilegio sera capaz de Registrar Actividades'),
(14, 'Eliminar Actividad', 'El usuario con este privilegio sera capaz de Eliminar Actividades'),
(15, 'Editar Actividad', 'El usuario con este privilegio sera capaz de Editar Actividades'),
(16, 'Generar ReporteA', 'El usuario con este privilegio sera capaz de generar Reportes de las Actividades'),
(17, 'Generar ReporteD', 'El usuario con este privilegio sera capaz de generar Reportes de las Donaciones'),
(18, 'Generar ReporteB', 'El usuario con este privilegio sera capaz de generar Reportes de los Becarios'),
(19, 'Registrar Benefactor', 'El usuario con este privilegio sera capaz de Registrar nuevos Benefactores'),
(20, 'Editar Benefactor', 'El usuario con este privilegio sera capaz de Editar informacion de los Benefactores'),
(21, 'Eliminar Benefactor', 'El usuario con este privilegio sera capaz de Eliminar Benefactores'),
(22, 'RegistrarEvento', 'El usuario con este privilegio sera capaz de Registrar nuevos Eventos'),
(23, 'Editar Evento', 'El usuario con este privilegio sera capaz de Editar la informacion de los Eventos'),
(24, 'Eliminar Evento', 'El usuario con este privilegio sera capaz de Eliminar Eventos'),
(25, 'Crear Cuenta', 'El administrador puede crear cuentas'),
(26, 'Eliminar Cuenta', 'El administrador puede eliminar cuentas'),
(27, 'Editar Cuenta', 'El administrador puede editar informacion de la cuenta'),
(28, 'Consultar informes', 'El usuario puede consultar informes'),
(29, 'Consultar boletines', 'El usuario puede consultar boletines'),
(30, 'Consultar colaboradores', 'El usuario puede consultar colaboradores'),
(31, 'Consultar noticias', 'El usuario puede consultar noticias'),
(32, 'Consultar actividades', 'El usuario puede consultar actividades'),
(33, 'Consultar benefactores', 'El usuario puede consultar actividades'),
(34, 'Consultar eventos', 'El usuario puede consultar eventos'),
(35, 'Consultar reportes', 'El usuario puede consultar reportes'),
(36, 'Consultar Cuentas', 'El usuario puede consultar cuentas'),
(37, 'Consultar Becario', 'El usuario puede consultar los becarios'),
(38, 'Agregar becario', 'El usuario puede agrgar becario'),
(39, 'Editar Becario', 'El usuario puede editar becario'),
(40, 'Eliminar becario', 'El usuario puede eliminar becario'),
(41, 'Consultar Donaciones', 'El usuario puede consultar donaciones');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono`
--

CREATE TABLE `telefono` (
  `IdBecario` int(11) NOT NULL,
  `IdTelefono` int(11) NOT NULL,
  `Numero` bigint(20) NOT NULL,
  `Dueño` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefono`
--

INSERT INTO `telefono` (`IdBecario`, `IdTelefono`, `Numero`, `Dueño`) VALUES
(1, 1, 4412657690, 'DELEGADA'),
(2, 2, 4411224646, ''),
(3, 3, 4411214508, ''),
(4, 4, 4411008341, ''),
(5, 5, 4411182454, ''),
(6, 6, 4411229043, ''),
(7, 7, 4424519672, ''),
(8, 8, 4411063431, ''),
(9, 9, 4411216130, ''),
(10, 10, 4411223927, ''),
(11, 11, 4411279824, ''),
(12, 12, 4411015080, ''),
(13, 13, 4411016389, ''),
(14, 14, 4411278621, ''),
(15, 15, 4411302955, ''),
(16, 16, 4424473984, ''),
(17, 17, 4412768358, 'PROPIO '),
(18, 18, 4411233869, ''),
(19, 19, 4425736714, ''),
(20, 20, 4411078237, ''),
(21, 21, 4421492508, ''),
(22, 22, 4411233283, ''),
(23, 23, 4426092076, ''),
(24, 24, 4411019761, ''),
(25, 25, 4411170634, 'PAPÁ'),
(26, 26, 4425981455, ''),
(27, 27, 4411036790, 'PROPIO'),
(28, 28, 4423977854, ''),
(29, 29, 4411012122, ''),
(30, 30, 4411205919, ''),
(31, 31, 4412653556, 'PROPIO'),
(32, 32, 4411072073, 'PROPIO'),
(33, 33, 4411221534, 'PROPIO'),
(34, 34, 4411148512, 'PROPIO'),
(35, 35, 4411359534, 'PROPIO'),
(36, 36, 4411207748, 'PROPIO'),
(37, 37, 4411364479, 'PROPIO'),
(38, 38, 4412657625, 'PROPIO'),
(39, 39, 8443596858, 'PROPIO'),
(40, 40, 4425640931, 'MAMÁ'),
(41, 41, 4411356293, 'PROPIO'),
(42, 42, 4425034332, 'PROPIO'),
(17, 43, 4411143807, 'MAMÁ'),
(27, 44, 4411000586, 'MAMÁ'),
(35, 45, 4411051818, 'MAMÁ'),
(36, 46, 4411184725, 'MAMÁ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tienebeneficio`
--

CREATE TABLE `tienebeneficio` (
  `IdBecario` int(11) NOT NULL,
  `IdBeneficio` int(11) NOT NULL,
  `Monto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tienebeneficio`
--

INSERT INTO `tienebeneficio` (`IdBecario`, `IdBeneficio`, `Monto`) VALUES
(1, 1, 1795),
(2, 1, 1485),
(3, 1, 1450),
(4, 1, 1500),
(6, 2, 1047),
(6, 1, 1047),
(7, 1, 1485),
(8, 1, 1750),
(9, 1, 1250),
(11, 1, 700),
(12, 1, 650),
(13, 1, 2270),
(14, 2, 300),
(15, 1, 1950),
(16, 1, 2337),
(17, 1, 1447),
(18, 3, 860),
(19, 3, 1400),
(20, 2, 550),
(21, 3, 800),
(20, 3, 550),
(23, 1, 1540),
(24, 1, 2120),
(25, 1, 1235),
(26, 1, 2262),
(27, 1, 560),
(27, 3, 560),
(29, 1, 1002),
(30, 1, 475),
(31, 1, 950),
(32, 1, 1500),
(33, 1, 1800),
(35, 1, 2200),
(37, 1, 1200),
(39, 1, 2300),
(40, 1, 1850),
(41, 4, 3200),
(42, 1, 1800);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajo`
--

CREATE TABLE `trabajo` (
  `IdBecario` int(11) NOT NULL,
  `IdTrabajo` int(11) NOT NULL,
  `NombreTrabajo` text,
  `Dias` text,
  `Periodo` text,
  `Horario` varchar(20) DEFAULT NULL,
  `TiempoLibre` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `trabajo`
--

INSERT INTO `trabajo` (`IdBecario`, `IdTrabajo`, `NombreTrabajo`, `Dias`, `Periodo`, `Horario`, `TiempoLibre`) VALUES
(1, 1, '', '', '', '', ''),
(2, 2, '', '', '', '', ''),
(3, 3, '', '', '', '', ''),
(4, 4, 'AYUDANTE ALBAÑIL', '', '', '', ''),
(5, 5, 'NO ESPECIFICA', '', '', '', ''),
(6, 6, '', '', '', '', ''),
(7, 7, 'JORNALERO', '', '', '', ''),
(8, 8, '', '', '', '', ''),
(9, 9, '', '', '', '', ''),
(10, 10, '', '', '', '', ''),
(11, 11, '', '', '', '', ''),
(12, 12, 'LIMPIEZA', '', '', '', ''),
(13, 13, '', '', '', '', ''),
(14, 14, '', '', '', '', ''),
(15, 15, '', '', '', '', ''),
(16, 16, '', '', '', '', ''),
(17, 17, '', '', '', '', ''),
(18, 18, 'TIENDA DE NOVEDADES', '', '', '', ''),
(19, 19, '', '', '', '', ''),
(20, 20, 'JORNALERA', '', '', '', ''),
(21, 21, '', '', '', '', ''),
(22, 22, '', '', '', '', ''),
(23, 23, 'LIMPIEZA', '', '', '', ''),
(24, 24, '', '', '', '', ''),
(25, 25, 'LIMPIEZA', '', '', '', ''),
(26, 26, 'CHALAN ALBAÑIL', '', '', '', ''),
(27, 27, 'JORNALERO', '', '', '', ''),
(28, 28, '', '', '', '', ''),
(29, 29, 'EN EL CAMPO', '', '', '', ''),
(30, 30, 'MANTENIMIENTO DE CARRETERA', '', '', '', ''),
(31, 31, 'FÁBRICA NO ESPECIFICA DE QUÉ', 'NO ESPECIFICA', 'VACACIONES', 'NO ESPECIFICA', 'LEER LIBROS Y REALIZAR TAREAS DE LA ESCUELA'),
(32, 32, 'EN EL CAMPO O DE CHALÁN DE ALBAÑIL CERCA DE QUERÉTARO', 'SÁBADO Y DOMINGO', 'A VECES EN PERIODO ESCOLAR. Y TODAS LAS VACACIONES', '5:00 AM A 5:00 PM', 'JUGAR FOOTBALL'),
(33, 33, 'PAPELERÍA', 'SÁBADOS', 'TODO EL TIEMPO', '8:00-5:00 ', 'LEER. HACER EJERCICIO. PLATICAR CON SU MAMÁ. ESCUCHAR MÚSICA. PRACTICA TAEKWONDO'),
(34, 34, '', '', '', '', 'AYUDA A SU MADRE A HACER LA LIMPIEZA DEL HOGAR'),
(35, 35, 'PLANTAR VERDURAS EN EL CAMPO', 'MARTES Y JUEVES', 'VACACIONES', '7-5 PM', 'AYUDA A SU MADRE A HACER LA LIMPIEZA DEL HOGAR'),
(36, 36, '', '', '', '', 'NO LAS MENCIONA'),
(37, 37, 'EN UNA FÁBRICA. ES OPERADOR DE UNA MÁQUNA REBABEADORA', 'TODA LA SEMANA', 'VACACIONES', '7:30 PM A 7:30 AM', 'JUGAR FOOTBALL'),
(38, 38, 'EN EL CAMPO Y A VECES EN UN CIBER', 'LUNES. MARTES. JUEVES Y SÁBADOS', 'VACACIONES', '8:00 AM A 5:00 PM', 'VER TELEVISIÓN. HACER TAREAS. ESTAR EN INTERNET Y AYUDAR A SU MAMÁ A HACER QUEHACERES DE LA CASA'),
(39, 39, '', '', '', '', 'HACER EJERCICIO Y SALIR A CORRER'),
(40, 40, '', '', '', '', 'A VECES VISITA A SUS TÍOS '),
(41, 41, 'EN LA OBRA', 'LUNES A SÁBADO', 'VACACIONES', '8:00-5:00 PM', 'AYUDA EN EL HOGAR'),
(42, 42, '', '', '', '', 'AYUDA EN LAS LABORES DE LA CASA. LE GUSTA ESCRIBIR CANCIONES. JUGAR CON SU HERMANA Y VER LA TELEVISIÓN');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transportes`
--

CREATE TABLE `transportes` (
  `IdBecario` int(11) NOT NULL,
  `IdTransporte` int(11) NOT NULL,
  `TipoTraslado` text,
  `Costo` int(11) DEFAULT NULL,
  `IdaVuelto` text,
  `Tiempo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `transportes`
--

INSERT INTO `transportes` (`IdBecario`, `IdTransporte`, `TipoTraslado`, `Costo`, `IdaVuelto`, `Tiempo`) VALUES
(1, 1, 'CAMINANDO', 0, '', '30 MINUTOS'),
(2, 2, 'CAMIÓN', 18, '', '30 MINUTOS'),
(3, 3, 'AUTOBÚS FORÁNEO', 26, 'IDA Y VUELTA', '55 MINUTOS'),
(4, 4, 'COMBI', 0, '', '15 MINUTOS'),
(5, 5, 'CAMINANDO', 0, 'IDA Y VUELTA', '60 MINUTOS'),
(6, 6, 'CAMIÓN', 18, '', '15 MINUTOS'),
(7, 7, 'CAMIÓN', 50, 'IDA Y VUELTA', '20 MINUTOS'),
(8, 8, 'CAMINANDO', 10, 'IDA', '60 MINUTOS'),
(9, 9, 'COMBI', 30, '', '60 MINUTOS'),
(10, 10, 'CAMINANDO', 0, '', '30 MINUTOS'),
(11, 11, 'CAMIÓN', 18, '', '20 MINUTOS'),
(12, 12, 'AUTOBÚS FORÁNEO', 155, 'IDA Y VUELTA', '40 MINUTOS'),
(13, 13, 'CAMIÓN', 50, '', '30 MINUTOS'),
(14, 14, 'CAMIÓN', 18, '', '20 MINUTOS'),
(15, 15, 'CAMIÓN', 18, '', '45 MINUTOS'),
(16, 16, 'CAMINANDO', 0, '', '40 MINUTOS'),
(17, 17, 'CAMINANDO', 0, '', '30 MINUTOS'),
(18, 18, 'CAMINANDO', 0, 'IDA Y VUELTA', '15 MINUTOS'),
(19, 19, 'CAMINANDO', 0, '', '15 MINUTOS'),
(20, 20, 'CAMIÓN', 35, '', '40 MINUTOS'),
(21, 21, 'CAMIÓN', 15, '', '30 MINUTOS'),
(22, 22, 'COMBI', 28, '', '30 MINUTOS'),
(23, 23, 'AUTOBÚS FORÁNEO', 150, 'IDA Y VUELTA', '240 MINUTOS'),
(24, 24, 'CAMIÓN', 15, '', '30 MINUTOS'),
(25, 25, 'AUTOBÚS FORÁNEO', 9, '', '30 MINUTOS'),
(26, 26, 'AUTOBÚS FORÁNEO', 9, 'IDA', '45 MINUTOS'),
(27, 27, 'CAMINANDO', 0, '', '15 MINUTOS'),
(28, 28, 'CAMIÓN', 9, '', '20 MINUTOS'),
(29, 29, 'CAMINANDO', 0, '', '60 MINUTOS'),
(30, 30, 'BICICLETA', 0, '', '40 MINUTOS'),
(31, 31, 'CAMIÓN', 140, 'Ida/vuelta', '90 MINUTOS'),
(32, 32, 'CAMINANDO', 0, '', '30 MINUTOS'),
(33, 33, 'EN CAMIÓN Y LUEGO CAMINANDO ', 20, 'Ida/vuelta', '45 MINUTOS'),
(34, 34, 'CAMINANDO', 0, '', '60 MINUTOS'),
(35, 35, 'CAMIÓN', 20, 'Ida/vuelta', '30 MINUTOS'),
(36, 36, 'CAMIÓN', 22, 'Ida/vuelta', '25 MINUTOS'),
(37, 37, 'TRANSPORTE ESCOLAR', 0, 'Ida/vuelta', '20 MINUTOS'),
(38, 38, 'CAMIÓN', 20, 'Ida/vuelta', '20 MINUTOS'),
(39, 39, 'CAMINANDO', 0, 'Ida/vuelta', '20 MINUTOS'),
(40, 40, 'CAMINANDO', 0, '', '20 MINUTOS'),
(41, 41, 'CAMINANDO', 0, '', '25 MINUTOS'),
(42, 42, 'CAMINANDO', 0, '', '90 MINUTOS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `IdUsuario` bigint(20) NOT NULL,
  `Usuario` varchar(15) DEFAULT NULL,
  `Nombre` varchar(15) DEFAULT NULL,
  `ApellidoP` varchar(15) DEFAULT NULL,
  `ApellidoM` varchar(15) DEFAULT NULL,
  `Correo` varchar(40) DEFAULT NULL,
  `Constraseña` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`IdUsuario`, `Usuario`, `Nombre`, `ApellidoP`, `ApellidoM`, `Correo`, `Constraseña`) VALUES
(1, 'a01064709', 'David', 'López', 'Ruiz', 'a01064709@itesm.mx', 'dava01'),
(2, 'A01207360', 'Romel', 'Martinez', 'Olvera', 'A01207360@itesm.mx', 'roma01'),
(3, 'a01701249', 'Juan Pablo', 'Aboytes', 'Lovoa', 'A01701249@itesm.mx', 'juaa01'),
(4, 'a00999546', 'Omar', 'DeAlba', 'Garza', 'A00999546@itesm.mx', 'omar01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_privilegios`
--

CREATE TABLE `usuario_privilegios` (
  `IdUsuario` bigint(20) NOT NULL,
  `IdPermiso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario_privilegios`
--

INSERT INTO `usuario_privilegios` (`IdUsuario`, `IdPermiso`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(2, 4),
(2, 30),
(2, 33),
(4, 22),
(4, 34);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `becario`
--
ALTER TABLE `becario`
  ADD PRIMARY KEY (`IdBecario`);

--
-- Indices de la tabla `benefactores`
--
ALTER TABLE `benefactores`
  ADD PRIMARY KEY (`IdBenefactores`);

--
-- Indices de la tabla `beneficio`
--
ALTER TABLE `beneficio`
  ADD PRIMARY KEY (`IdBeneficio`);

--
-- Indices de la tabla `boletines`
--
ALTER TABLE `boletines`
  ADD PRIMARY KEY (`IdArchivo`);

--
-- Indices de la tabla `colaboradores`
--
ALTER TABLE `colaboradores`
  ADD PRIMARY KEY (`IdColaborador`);

--
-- Indices de la tabla `documentacion`
--
ALTER TABLE `documentacion`
  ADD PRIMARY KEY (`IdDoc`);

--
-- Indices de la tabla `enfermedad`
--
ALTER TABLE `enfermedad`
  ADD PRIMARY KEY (`IdEnfermedad`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Indices de la tabla `etnia`
--
ALTER TABLE `etnia`
  ADD PRIMARY KEY (`IdEtnia`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Indices de la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`IdEvento`);

--
-- Indices de la tabla `familia`
--
ALTER TABLE `familia`
  ADD PRIMARY KEY (`IdFamilia`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Indices de la tabla `informes`
--
ALTER TABLE `informes`
  ADD PRIMARY KEY (`IdInforme`);

--
-- Indices de la tabla `nota`
--
ALTER TABLE `nota`
  ADD PRIMARY KEY (`IdNota`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`IdNoticias`);

--
-- Indices de la tabla `privilegios`
--
ALTER TABLE `privilegios`
  ADD PRIMARY KEY (`IdPermiso`);

--
-- Indices de la tabla `telefono`
--
ALTER TABLE `telefono`
  ADD PRIMARY KEY (`IdTelefono`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Indices de la tabla `trabajo`
--
ALTER TABLE `trabajo`
  ADD PRIMARY KEY (`IdTrabajo`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Indices de la tabla `transportes`
--
ALTER TABLE `transportes`
  ADD PRIMARY KEY (`IdTransporte`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`IdUsuario`);

--
-- Indices de la tabla `usuario_privilegios`
--
ALTER TABLE `usuario_privilegios`
  ADD PRIMARY KEY (`IdUsuario`,`IdPermiso`),
  ADD KEY `IdUsuario` (`IdUsuario`),
  ADD KEY `IdPermiso` (`IdPermiso`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `becario`
--
ALTER TABLE `becario`
  MODIFY `IdBecario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT de la tabla `benefactores`
--
ALTER TABLE `benefactores`
  MODIFY `IdBenefactores` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT de la tabla `beneficio`
--
ALTER TABLE `beneficio`
  MODIFY `IdBeneficio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `boletines`
--
ALTER TABLE `boletines`
  MODIFY `IdArchivo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `colaboradores`
--
ALTER TABLE `colaboradores`
  MODIFY `IdColaborador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `documentacion`
--
ALTER TABLE `documentacion`
  MODIFY `IdDoc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `enfermedad`
--
ALTER TABLE `enfermedad`
  MODIFY `IdEnfermedad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT de la tabla `etnia`
--
ALTER TABLE `etnia`
  MODIFY `IdEtnia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT de la tabla `eventos`
--
ALTER TABLE `eventos`
  MODIFY `IdEvento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `familia`
--
ALTER TABLE `familia`
  MODIFY `IdFamilia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT de la tabla `informes`
--
ALTER TABLE `informes`
  MODIFY `IdInforme` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT de la tabla `nota`
--
ALTER TABLE `nota`
  MODIFY `IdNota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `IdNoticias` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `telefono`
--
ALTER TABLE `telefono`
  MODIFY `IdTelefono` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT de la tabla `trabajo`
--
ALTER TABLE `trabajo`
  MODIFY `IdTrabajo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT de la tabla `transportes`
--
ALTER TABLE `transportes`
  MODIFY `IdTransporte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `usuario_privilegios`
--
ALTER TABLE `usuario_privilegios`
  ADD CONSTRAINT `usuario_privilegios_ibfk_1` FOREIGN KEY (`IdUsuario`) REFERENCES `usuario` (`IdUsuario`),
  ADD CONSTRAINT `usuario_privilegios_ibfk_2` FOREIGN KEY (`IdPermiso`) REFERENCES `privilegios` (`IdPermiso`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

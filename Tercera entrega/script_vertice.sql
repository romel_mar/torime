﻿set dateformat dmy
CREATE TABLE becario (
  IdBecario varchar(6) NOT NULL,
  Folio numeric(11) NOT NULL,
  Nombre varchar(15) NOT NULL,
  ApellidoP varchar(20) NOT NULL,
  ApellidoM varchar(20) NOT NULL,
  CURP varchar(18) NOT NULL,
  FechaNacimiento date NOT NULL,
  Email varchar(30) DEFAULT NULL,
  Estatus text NOT NULL,
  Sexo text NOT NULL,
  LugarNacimiento text NOT NULL,
  Comunidad text NOT NULL,
  FechaEntrevista date DEFAULT NULL,
  Prioridad numeric(11) NOT NULL,
  Grado varchar(50) NOT NULL,
  Escuela varchar(50) DEFAULT NULL,
  Nivel varchar(20) DEFAULT NULL,
  Carrera text,
  PromedioInicial float DEFAULT NULL,
  PromedioActual float DEFAULT NULL
) 

BULK INSERT equipo05.equipo05.[becario]
  FROM 'e:\wwwroot\equipo05\becarioss.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

  ALTER TABLE becario add constraint llavebecario PRIMARY KEY (IdBecario) 


CREATE TABLE telefono (
  IdBecario varchar(6) NOT NULL,
  IdTelefono varchar(6) NOT NULL,
  Numero numeric(20) NOT NULL,
  Dueño text
)

BULK INSERT equipo05.equipo05.[telefono]
  FROM 'e:\wwwroot\equipo05\Telefono.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

  ALTER TABLE telefono add constraint llavetelefono PRIMARY KEY (IdTelefono) 
  ALTER TABLE telefono add constraint llaveforanea foreign key (IdBecario) references becario(IdBecario); 
 

  CREATE TABLE nota (
  IdBecario varchar(6) NOT NULL,
  IdNota varchar(6) NOT NULL,
  Descripcion text
)

BULK INSERT equipo05.equipo05.[nota]
  FROM 'e:\wwwroot\equipo05\Nota.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

  ALTER TABLE nota add constraint llavenota PRIMARY KEY (IdNota) 
  ALTER TABLE nota add constraint llaveforaneanota foreign key (IdBecario) references becario(IdBecario); 

  CREATE TABLE enfermedad (
  IdBecario varchar(6) NOT NULL,
  IdEnfermedad varchar(6) NOT NULL,
  HayEnfermedad text,
  FamiliaEnfermo text,
  Descripcion text
  )

  BULK INSERT equipo05.equipo05.[enfermedad]
  FROM 'e:\wwwroot\equipo05\Enfermedad.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

  ALTER TABLE enfermedad add constraint llaveenfermedad PRIMARY KEY (IdEnfermedad) 
  ALTER TABLE enfermedad add constraint llaveforaneaEnfermedad foreign key (IdBecario) references becario(IdBecario); 

  CREATE TABLE etnia (
  IdBecario varchar(6) NOT NULL,
  IdEtnia varchar(6) NOT NULL,
  HablaLengua text,
  Etnia text
)
  BULK INSERT equipo05.equipo05.[etnia]
  FROM 'e:\wwwroot\equipo05\Etnia.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

  ALTER TABLE etnia add constraint llaveetnia PRIMARY KEY (IdEtnia) 
  ALTER TABLE etnia add constraint llaveforaneaEtnia foreign key (IdBecario) references becario(IdBecario); 

  CREATE TABLE trabajo (
  IdBecario varchar(6) NOT NULL,
  IdTrabajo varchar(6) NOT NULL,
  NombreTrabajo text,
  Dias text,
  Periodo text,
  Horario varchar(20) DEFAULT NULL,
  TiempoLibre text
) 
  BULK INSERT equipo05.equipo05.[trabajo]
  FROM 'e:\wwwroot\equipo05\Trabajo.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

  ALTER TABLE trabajo add constraint llavetrabajo PRIMARY KEY (IdTrabajo) 
  ALTER TABLE trabajo add constraint llaveforaneaTrabajo foreign key (IdBecario) references becario(IdBecario);

 CREATE TABLE familia (
  IdBecario varchar(6) NOT NULL,
  IdFamilia varchar(6) NOT NULL,
  IngresoMin numeric(11) DEFAULT NULL,
  IngresoMax numeric(11) DEFAULT NULL,
  Mama numeric(11) DEFAULT NULL,
  Papa numeric(11) DEFAULT NULL,
  Hermanos numeric(11) DEFAULT NULL,
  Hijos numeric(11) DEFAULT NULL,
  Abuela numeric(11) DEFAULT NULL,
  Abuelo numeric(11) DEFAULT NULL,
  Otros numeric(11) DEFAULT NULL,
  Descripcion varchar(250) DEFAULT NULL
) 

  ALTER TABLE familia add constraint llavefamilia PRIMARY KEY (IdFamilia) 
  ALTER TABLE familia add constraint llaveforaneafamilia foreign key (IdBecario) references becario(IdBecario);

  
 
 CREATE TABLE transportes (
  IdBecario varchar(6) NOT NULL,
  IdTransporte varchar(6) NOT NULL,
  TipoTraslado text,
  Costo numeric(11) DEFAULT NULL,
  IdaVuelto text,
  Tiempo varchar(20) DEFAULT NULL
) 

BULK INSERT equipo05.equipo05.[transportes]
  FROM 'e:\wwwroot\equipo05\Transporte.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

  ALTER TABLE  transportes add constraint llavetransporte PRIMARY KEY (IdTransporte) 
  ALTER TABLE transportes add constraint llaveforaneatransporte foreign key (IdBecario) references becario(IdBecario);

  CREATE TABLE documentacion (
  IdBecario varchar(6) NOT NULL,
  IdDoc varchar(7) NOT NULL,
  Nombre text NOT NULL
) 
BULK INSERT equipo05.equipo05.[documentacion]
  FROM 'e:\wwwroot\equipo05\Documentacion.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

  CREATE TABLE beneficio (
  IdBecario varchar(6) NOT NULL,
  IdBeneficio varchar(6) NOT NULL,
  NomBeneficio text
) 

BULK INSERT equipo05.equipo05.[beneficio]
  FROM 'e:\wwwroot\equipo05\Beneficio.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

  CREATE TABLE tienebeneficio (
  IdBecario varchar(6) NOT NULL,
  IdBeneficio varchar(6) NOT NULL,
  Monto numeric(11) DEFAULT NULL
)

BULK INSERT equipo05.equipo05.[tienebeneficio]
  FROM 'e:\wwwroot\equipo05\TieneBeneficio.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

  ALTER TABLE  tienebeneficio add constraint llavetienebeneficio PRIMARY KEY (IdBecario, IdBeneficio) 
  
  CREATE TABLE tienedocumentacion (
  IdBecario varchar(6) NOT NULL,
  IdDoc varchar(7) NOT NULL,
  ImdDoc varchar(250)
) 

BULK INSERT equipo05.equipo05.[tienedocumentacion]
  FROM 'e:\wwwroot\equipo05\TieneDocumentacion.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

    ALTER TABLE  tienedocumentacion add constraint llavetienedocumentacion PRIMARY KEY (IdBecario, IdDoc) 

	CREATE TABLE noticias (
  IdNoticias varchar(6) NOT NULL,
  Nombre text,
  Descripcion varchar(250),
  Fecha date DEFAULT NULL,
  Tag text,
  Foto varchar(100) DEFAULT NULL
) 

BULK INSERT equipo05.equipo05.[noticias]
  FROM 'e:\wwwroot\equipo05\Noticias.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

   ALTER TABLE  noticias add constraint llaveNoticias PRIMARY KEY (IdNoticias) 
   
   set dateformat dmy
   CREATE TABLE eventos (
   IdEvento varchar(6) NOT NULL,
   Nombre text,
   Descripcion varchar(250),
   Direccion text,
   FechaInicio date DEFAULT NULL,
   FechaFin  date DEFAULT NULL,
   Foto  varchar(100) DEFAULT NULL
) 

  ALTER TABLE  eventos add constraint llaveEventos PRIMARY KEY (IdEvento) 
  
  set dateformat ymd
  CREATE TABLE benefactores (
  IdBenefactores varchar(6) NOT NULL,
  Nombre text NOT NULL,
  ApellidoP text,
  ApellidoM text,
  RazonSocial text,
  FisicaOMoral text,
  FechaAlta date DEFAULT NULL,
  RFC varchar(15) DEFAULT NULL,
  Domicilio varchar(100) DEFAULT NULL,
  Municipio text,
  Estado text,
  Telefono numeric(20) DEFAULT NULL,
  EmailBenefactor varchar(40) DEFAULT NULL,
  EmailContacto varchar(40) DEFAULT NULL,
  RequiereFactura text,
  PeriodicidadAportacion text,
  Monto numeric(11) DEFAULT NULL,
  TiempoMembresia text
)

BULK INSERT equipo05.equipo05.[benefactores]
  FROM 'e:\wwwroot\equipo05\Benefactores.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

  ALTER TABLE benefactores add constraint llaveBenefactores PRIMARY KEY (IdBenefactores)
  
 
  CREATE TABLE usuario (
  IdUsuario varchar(6) NOT NULL,
  Usuario text,
  Nombre text,
  ApellidoP text,
  ApellidoM text,
  Correo varchar(40) DEFAULT NULL,
  Constrasena varchar(20) DEFAULT NULL
)

BULK INSERT equipo05.equipo05.[usuario]
  FROM 'e:\wwwroot\equipo05\Usuarios.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

  ALTER TABLE usuario add constraint llaveUsuarios PRIMARY KEY (IdUsuario)
  
  CREATE TABLE donativoespecie (
  IdDonEspecie varchar(6) NOT NULL,
  Donativo text,
  Descripcion varchar(250)
) 

BULK INSERT equipo05.equipo05.[donativoespecie]
  FROM 'e:\wwwroot\equipo05\Donativos en Especie.csv'
  WITH
  (
    CODEPAGE = 'ACP',
    FIELDTERMINATOR = ',',
    ROWTERMINATOR = '\n'
  )

  ALTER TABLE donativoespecie add constraint llavedonaespecie PRIMARY KEY (IdDonEspecie)

  set dateformat dmy
  CREATE TABLE solicitudregistro (
  IdSolicitud varchar(6) NOT NULL,
  Usuario text,
  Nombre text,
  ApellidoP text,
  ApellidoM text,
  Email varchar(40) DEFAULT NULL,
)

  ALTER TABLE solicitudregistro add constraint llavesolicitudregistro PRIMARY KEY (IdSolicitud)

  CREATE TABLE privilegios (
  IdPermiso varchar(6) NOT NULL,
  Permiso text,
  Descripcion varchar(250)
) 

  ALTER TABLE privilegios add constraint llaveprivilegios PRIMARY KEY (IdPermiso)
 
 CREATE TABLE actividades (
  IdActividad varchar(6) NOT NULL,
  Nombre text,
  DescripcionCorta text,
  Descripcion varchar(250),
  Fecha date DEFAULT NULL,
  Direccion text,
  Foto varchar(100) DEFAULT NULL
)

ALTER TABLE actividades add constraint llaveactividades PRIMARY KEY (IdActividad)

CREATE TABLE datosfacturas (
  IdFacturas varchar(8) NOT NULL,
  RazonSocial text,
  RFC varchar(15) DEFAULT NULL,
  Telefono numeric(15) DEFAULT NULL,
  Email varchar(40) DEFAULT NULL,
  Calle varchar(40) DEFAULT NULL,
  NumeroExterior numeric(11) DEFAULT NULL,
  NumeroInterior numeric(11) DEFAULT NULL,
  Colonia numeric(11) DEFAULT NULL,
  CodigoPostal varchar(50) DEFAULT NULL,
  Pais text,
  Estado text,
  Municipio text
  )

  ALTER TABLE datosfacturas add constraint llavedatosfactura PRIMARY KEY (IdFacturas)

  
  
   
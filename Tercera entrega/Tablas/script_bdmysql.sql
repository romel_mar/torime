CREATE TABLE `becario` (
  `IdBecario` varchar(6) NOT NULL,
  `Folio` int(11) NOT NULL,
  `Nombre` text NOT NULL,
  `ApellidoP` text NOT NULL,
  `ApellidoM` text NOT NULL,
  `CURP` varchar(18) NOT NULL,
  `FechaNacimiento` date NOT NULL,
  `Email` varchar(30) DEFAULT NULL,
  `Estatus` text NOT NULL,
  `Sexo` text NOT NULL,
  `LugarNacimiento` text NOT NULL,
  `Comunidad` text NOT NULL,
  `FechaEntrevista` date DEFAULT NULL,
  `Prioridad` int(11) NOT NULL,
  `Grado` varchar(50) NOT NULL,
  `Escuela` varchar(50) DEFAULT NULL,
  `Nivel` varchar(20) DEFAULT NULL,
  `Carrera` text,
  `PromedioInicial` float NOT NULL,
  `PromedioActual` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `becario`
--

INSERT INTO `becario` (`IdBecario`, `Folio`, `Nombre`, `ApellidoP`, `ApellidoM`, `CURP`, `FechaNacimiento`, `Email`, `Estatus`, `Sexo`, `LugarNacimiento`, `Comunidad`, `FechaEntrevista`, `Prioridad`, `Grado`, `Escuela`, `Nivel`, `Carrera`, `PromedioInicial`, `PromedioActual`) VALUES
('BEC001', 1, 'ROSALÍA', 'BALTAZAR', 'MORALES', 'BAMR011230MQTLRSA4', '2001-07-21', '-', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'EL SAUCITO', '0000-00-00', 2, 'SECUNDARIA TERMINADA', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 7.4, 7),
('BEC002', 2, 'ELIZABETH', 'BLAS', 'DE LEÓN', 'BALE000903MQTLNLA5', '1999-10-05', '', 'Regular', 'Femenino', 'TOLIMAN', 'CERRITO PARADO', '0000-00-00', 1, '2° SEM BACHILLERATO', 'COBAQ 6', '3° SEM', '-', 8.8, 7),
('BEC003', 3, 'NANCY', 'DE LEÓN', 'GONZÁLEZ', 'LEGN000405MQTNNNA3', '2002-05-08', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 2, '4° SEM BACHILLERATO', 'COBAQ 6', '5° SEM', '', 9.29, 9),
('BEC004', 4, 'DANIEL', 'DE LEÓN', 'GUDIÑO', 'LEGD981206HQTNDN04', '2002-04-05', 'danileo1298@gmail.com', 'Baja', 'Masculino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, 'BACHILLERATO TERMINADO', '-', '-', '-', 8.2, 0),
('BEC005', 5, 'JOSÉ RAUL', 'DE LEÓN', 'MORALES', 'LEMR011129HQTNRLA0', '2000-08-18', '-', 'Baja', 'Masculino', 'TOLIMAN', 'MESA DE CHAGOYA', '0000-00-00', 2, '2° SEM BACHILLERATO', 'TELEBACHILLERATO COMUNITARIO', '3° SEM', '-', 7.5, 0),
('BEC006', 6, 'CARLA BIBIANA', 'DE LEÓN', 'SÁNCHEZ', 'LESC990123MQTNNR03', '1994-09-26', '-', 'Baja', 'Femenino', 'CADEREYTA DE MONTES', 'CERRITO PARADO', '0000-00-00', 2, '5° SEM BACHILLERATO', '-', '-', '-', 8.9, 0),
('BEC007', 7, 'VICTOR MANUEL', 'DE LEÓN', 'TREJO', 'LETV010721HQTNRCA7', '2001-09-28', '-', 'Baja', 'Masculino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 7, 0),
('BEC008', 8, 'YESICA', 'GONZALEZ', 'GONZÁLEZ', 'GOGY991005MQTNNS07', '2000-06-15', 'yesigonzalesjgg@gmail.com', 'Regular', 'Femenino', 'GUANAJUATO', 'LOS GONZALEZ', '0000-00-00', 2, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 8.7, 8),
('BEC009', 9, 'MARÍA ELENA', 'DE LEÓN', 'GUDIÑO', 'LEGE020508MNENDLA4', '2001-06-19', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, 'SECUNDARIA TERMINADA', 'EMSAD 24', '1° SEM', '-', 9.8, 8),
('BEC010', 10, 'ALEJANDRA ', 'GUDIÑO ', 'GUDIÑO', 'GUGA020405MQTDDLA9', '2001-07-10', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, 'SECUNDARIA TERMINADA', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 7.2, 7),
('BEC020', 11, 'MELISSA', 'GUDIÑO ', 'GUDIÑO', 'GUGM000818MQTDDLA4', '2000-05-01', 'melinna1212@gmail.com', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 9.14, 9),
('BEC021', 12, 'SOFIA', 'GUDIÑO ', 'GUDIÑO', 'GUGS940926MQTDDF06', '1999-01-14', 'sofiggextr@hotmail.com', 'Baja', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, '2° CUATRI LICENCIATURA', '-', '-', '-', 9.6, 0),
('BEC022', 13, 'DANIELA', 'GUDIÑO ', 'PÉREZ', 'GUPD010928MQTDRNA9', '2001-11-19', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 9.2, 8),
('BEC023', 14, 'ANA CRISTINA', 'GUDIÑO ', 'YATA', 'GUYA000615MGTDTNA2', '2001-06-23', 'cristi.yata.1517@gmail.com', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'MESA DE RAMIREZ', '0000-00-00', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 9, 8),
('BEC024', 15, 'YESSICA', 'LUNA', 'GUDIÑO', 'LUGY010619MQTNDSA5', '2000-10-01', '4424489828', 'Regular', 'Femenino', 'TOLIMAN', 'EL SAUCITO', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 7.1, 7),
('BEC025', 16, 'MARÍA BRISEIDA', 'MONTOYA', 'GUDIÑO', 'MOGB010710MQTNDRA2', '1996-06-12', '-', 'Baja', 'Femenino', 'TOLIMAN', 'LA PRESITA', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8.2, 0),
('BEC026', 17, 'LUIS ALBERTO', 'MORALES ', 'DE LEÓN', 'MOLL000501HQTRNSA0', '1996-12-02', '-', 'Regular', 'Masculino', 'CADEREYTA DE MONTES', 'MESA DE CHAGOYA', '0000-00-00', 1, '4° SEM BACHILLERATO', 'TELEBACHILLERATO COMUNITARIO', '5° SEM', '-', 7.8, 7),
('BEC027', 18, 'AURELIA', 'MORALES', 'GONZÁLEZ', 'MOGA990114MQTRNR02', '2001-09-19', 'aurelitamorales1999@gmail.com', 'Baja', 'Femenino', 'QUERETARO', 'SABINO DE SAN AMBROSIO', '0000-00-00', 1, 'BACHILLERATO TERMINADO', '-', '-', '-', 7.6, 0),
('BEC028', 19, 'FABIOLA', 'MORALES ', 'GUDIÑO', 'MOGF011119MQTRDBA1', '2000-04-19', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, 'SECUNDARIA TERMINADA', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 7, 0),
('BEC029', 20, 'BRENDA EDITH', 'MORALES ', 'GUERRERO', 'MOGB010623MQTRRRA1', '2000-08-15', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 7.8, 8),
('BEC030', 21, 'BERENICE', 'MORALES ', 'MARTÍNEZ', 'MOMB001001MQTRRRB8', '2002-01-20', '-', 'Regular', 'Femenino', 'TOLIMAN', 'CERRITO PARADO', '0000-00-00', 2, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 9.2, 8),
('BEC040', 22, 'BIBIANA', 'MORALES', 'MORALES', 'MOMB960612MQTRRB04', '1999-07-03', 'bsanches12314@gmail.com', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE CHAGOYA', '0000-00-00', 1, '7° SEM INGENIERÍA', 'ITQ-TOLIMÁN', '8° SEM', 'GESTIÓN EMPRESARIAL', 8.65, 8),
('BEC041', 23, 'MARÍA ADELINA', 'MORALES ', 'RESÉNDIZ', 'MORA961202MQTRSD05', '1999-08-22', 'mariaadelinamora1996@gmail.com', 'Baja', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 2, '1° CUATRI LICENCIATURA', '-', '-', '-', 9.1, 0),
('BEC042', 24, 'JOSÉ ANTONIO', 'OLGUIN', 'RESÉNDIZ', 'OURA010919HQTLSNA7', '1995-06-10', '-', 'Baja', 'Masculino', 'TOLIMAN', 'SAN PEDRO DE LOS EUCALIPTOS', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 0, 0),
('BEC043', 25, 'MARÍA DIANELI', 'PÉREZ', 'DE LEÓN', 'PELD000419MQTRNNA2', '2000-04-19', 'dianelipdl@gmail.com', 'Baja', 'Femenino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 7.5, 0),
('BEC044', 26, 'EMMANUEL', 'PÉREZ', 'GUDIÑO', 'PEGE000815HQTRDMA2', '2000-08-15', 'emaperz1508@gmail.com', 'Baja', 'Masculino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 7.49, 0),
('BEC045', 27, 'LUIS OCTAVIO ', 'RESÉNDIZ', 'GONZÁLEZ', 'REGL020120HQTSNSA1', '2002-01-20', '-', 'Condicionado', 'Masculino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, 'PENDIENTE', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 7, 0),
('BEC046', 28, 'LUIS GERARDO', 'RESÉNDIZ', 'MONTOYA', 'REML990703HQTSNS05', '1999-07-03', '-', 'Regular', 'Masculino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, 'BACHILLERATO TERMINADO', 'ITQ-TOLIMÁN', '1° SEM', 'GESTIÓN EMPRESARIAL', 8.6, 8),
('BEC047', 29, 'GRACIELA', 'SÁNCHEZ', 'DE SÁNCHEZ', 'SASG990822MQTNNR09', '1999-08-22', '-', 'Baja', 'Femenino', 'TOLIMAN', 'ZAPOTE DE LOS URIBE', '0000-00-00', 1, 'BACHILLERATO TERMINADO', '-', '-', '', 7.5, 0),
('BEC048', 30, 'MARCOS', 'TREJO', 'SÁNCHEZ', 'TESM950610HQTRNR00', '1995-06-10', 'mtrejo14.depad.tol@gmail.com', 'Con beca', 'Masculino', 'TOLIMAN', 'CERRITO PARADO', '0000-00-00', 2, '7° SEM INGENIERÍA', 'ITQ-TOLIMÁN', '8° SEM', 'SISTEMAS COMPUTACIONALES', 8.8, 8),
('BEC049', 31, 'BEATRIZ', 'BLAS', 'SÁNCHEZ', 'BASB961009MQTLNT04', '1996-10-09', 'betilizblas0910@gmail.com', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'CERRITO PARADO', '2017-08-03', 1, '4° SEM LICENCIATURA', 'UAQ-CADEREYTA', '5° SEM', 'LIC. EN ADMINISTRACIÓN', 8, 8),
('BEC050', 32, 'ALEXANDRO', 'DE SANTIAGO', 'MONTOYA ', 'SAMA001018HQTNNLA7', '2000-10-18', 'alexandrodstgmontoya@gmail.com', 'Regular', 'Masculino', 'TOLIMAN', 'EL CIPRÉS', '2017-08-04', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 7, 7),
('BEC060', 33, 'NELLY VIRIDIANA', 'DE SANTIAGO', 'SÁNCHEZ ', 'SASN010917MQTNNLA9', '2001-09-17', '-', 'Regular', 'Femenino', 'TOLIMAN', 'CASA BLANCA', '2017-08-02', 2, '2° SEM BACHILLERATO', 'COBAQ 6', '3° SEM', '-', 9, 9),
('BEC061', 34, 'MARÍA ISABEL', 'GONZÁLEZ', 'GONZÁLEZ', 'GOGI020118MQTNNSA3', '2002-01-10', '-', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'MESA DE RAMIREZ', '2017-08-14', 1, 'SECUNDARIA TERMINADA', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 6, 6),
('BEC062', 35, 'MARÍA ADELIA', 'GUDIÑO ', 'SÁNCHEZ', 'GUSA011025MQTDNDB8', '2001-10-25', 'mariags1025@a.cobaq.edu.mx', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'MESA DE RAMIREZ', '2017-08-03', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8, 8),
('BEC063', 36, 'MIGUEL ÁNGEL', 'MORALES', 'GUDIÑO', 'MOGM000423HQTRDGA7', '2000-04-23', 'michaelmike8300959@gmail.com', 'Regular', 'Masculino', 'TOLIMAN', 'MESA DE CHAGOYA', '2017-08-02', 1, 'SECUNDARIA TERMINADA', 'EMSAD 24', '1° SEM', '-', 8, 8),
('BEC064', 37, 'CRISTIÁN', 'PÉREZ', 'LOYOLA ', 'PELC980217HQTRYR04', '1998-02-11', 'crpsloyola450@gmail.com', 'Regular', 'Masculino', 'TOLIMAN', 'MAGUEY MANSO', '2007-08-03', 2, 'BACHILLERATO TERMINADO', 'UTQ-QUERÉTARO', '1° SEM', 'MECATRÓNICA', 8, 8),
('BEC065', 38, 'ANA LAURA', 'RESÉNDIZ', 'GUDIÑO', 'REGA020909MQTSDNA5', '2002-09-09', 'lauraresendiz230@gmail.com', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'MESA DE RAMIREZ', '2017-08-04', 1, 'SECUNDARIA TERMINADA', 'EMSAD 24', '1° SEM', '-', 7, 7),
('BEC066', 39, 'EFRAÍN', 'RESÉNDIZ', 'HERNÁNDEZ', 'REHE980313HQTSRF09', '1998-03-13', '', 'Regular', 'Masculino', 'TOLIMAN', 'CERRITO PARADO', '2017-08-14', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8, 8),
('BEC067', 40, 'ROCÍO', 'RESÉNDIZ', 'PÉREZ', 'REPR021215MQTSRCA9', '2002-12-15', '', 'Regular', 'Femenino', 'TOLIMAN', 'SABINO DE SAN AMBROSIO', '2017-08-04', 2, 'SECUNDARIA TERMINADA', 'EMSAD 24', '1° SEM', '-', 7, 7),
('BEC068', 41, 'JESÚS NAZARIO', 'RESÉNDIZ', 'DE SANTIAGO ', 'RESJ001224HQTSNSA0', '2000-12-24', 'jesusrs1224@a.cobaq.edu.mx', 'Regular', 'Masculino', 'TOLIMAN', 'CERRITO PARADO', '2017-08-14', 2, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8, 8),
('BEC069', 42, 'ROSA ELVIA', 'TREJO', 'HERNÁNDEZ', 'TEHR000618MQTRRSA8', '2000-06-18', 'rosath0618@a.cobaq.edu.mx', 'Regular', 'Femenino', 'TOLIMAN', 'CERRITO PARADO', '2017-08-14', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8, 8);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `becario`
--
ALTER TABLE `becario`
  ADD PRIMARY KEY (`IdBecario`);
COMMIT;


CREATE TABLE `telefono` (
  `IdBecario` varchar(6) NOT NULL,
  `IdTelefono` varchar(6) NOT NULL,
  `Numero` bigint(20) NOT NULL,
  `Dueño` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefono`
--

INSERT INTO `telefono` (`IdBecario`, `IdTelefono`, `Numero`, `Dueño`) VALUES
('BEC001', 'TEL001', 4412657690, 'DELEGADA'),
('BEC002', 'TEL002', 4411224646, ''),
('BEC003', 'TEL003', 4411214508, ''),
('BEC004', 'TEL004', 4411008341, ''),
('BEC005', 'TEL005', 4411182454, ''),
('BEC006', 'TEL006', 4411229043, ''),
('BEC007', 'TEL007', 4424519672, ''),
('BEC008', 'TEL008', 4411063431, ''),
('BEC009', 'TEL009', 4411216130, ''),
('BEC010', 'TEL010', 4411223927, ''),
('BEC020', 'TEL020', 4411279824, ''),
('BEC021', 'TEL021', 4411015080, ''),
('BEC022', 'TEL022', 4411016389, ''),
('BEC023', 'TEL023', 4411278621, ''),
('BEC024', 'TEL024', 4411302955, ''),
('BEC025', 'TEL025', 4424473984, ''),
('BEC026', 'TEL026', 4412768358, 'PROPIO '),
('BEC027', 'TEL027', 4411233869, ''),
('BEC028', 'TEL028', 4425736714, ''),
('BEC029', 'TEL029', 4411078237, ''),
('BEC030', 'TEL030', 4421492508, ''),
('BEC040', 'TEL040', 4411233283, ''),
('BEC041', 'TEL041', 4426092076, ''),
('BEC042', 'TEL042', 4411019761, ''),
('BEC043', 'TEL043', 4411170634, 'PAPÁ'),
('BEC044', 'TEL044', 4425981455, ''),
('BEC045', 'TEL045', 4411036790, 'PROPIO'),
('BEC046', 'TEL046', 4423977854, ''),
('BEC047', 'TEL047', 4411012122, ''),
('BEC048', 'TEL048', 4411205919, ''),
('BEC049', 'TEL049', 4412653556, 'PROPIO'),
('BEC050', 'TEL050', 4411072073, 'PROPIO'),
('BEC060', 'TEL060', 4411221534, 'PROPIO'),
('BEC061', 'TEL061', 4411148512, 'PROPIO'),
('BEC062', 'TEL062', 4411359534, 'PROPIO'),
('BEC063', 'TEL063', 4411207748, 'PROPIO'),
('BEC064', 'TEL064', 4411364479, 'PROPIO'),
('BEC065', 'TEL065', 4412657625, 'PROPIO'),
('BEC066', 'TEL066', 8443596858, 'PROPIO'),
('BEC067', 'TEL067', 4425640931, 'MAMÁ'),
('BEC068', 'TEL068', 4411356293, 'PROPIO'),
('BEC069', 'TEL069', 4425034332, 'PROPIO'),
('BEC026', 'TEL070', 4411143807, 'MAMÁ'),
('BEC045', 'TEL071', 4411000586, 'MAMÁ'),
('BEC062', 'TEL072', 4411051818, 'MAMÁ'),
('BEC063', 'TEL073', 4411184725, 'MAMÁ');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `telefono`
--
ALTER TABLE `telefono`
  ADD PRIMARY KEY (`IdTelefono`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `telefono`
--
ALTER TABLE `telefono`
  ADD CONSTRAINT `telefono_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

CREATE TABLE `nota` (
  `IdBecario` varchar(6) NOT NULL,
  `IdNota` varchar(6) NOT NULL,
  `Descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nota`
--

INSERT INTO `nota` (`IdBecario`, `IdNota`, `Descripcion`) VALUES
('BEC001', 'NOT001', 'SOLICITAR CERTIFICADO DE SECUNDARIA'),
('BEC002', 'NOT002', ''),
('BEC003', 'NOT003', ''),
('BEC004', 'NOT004', 'NO RENOVÓ BECA. NO SE CONOCE LA RAZÓN'),
('BEC005', 'NOT005', 'NO RENOVÓ BECA. NO SE CONOCE LA RAZÓN'),
('BEC006', 'NOT006', 'BAJA POR ABANDONO ESCOLAR'),
('BEC007', 'NOT007', 'SIN DERECHO A RENOVACIÓN POR INCUMPLIMIENTO DEL CONVENIO'),
('BEC008', 'NOT008', ''),
('BEC009', 'NOT009', ''),
('BEC010', 'NOT010', ''),
('BEC020', 'NOT020', ''),
('BEC021', 'NOT021', 'BAJA POR ABANDONO ESCOLAR'),
('BEC022', 'NOT022', ''),
('BEC023', 'NOT023', ''),
('BEC024', 'NOT024', ''),
('BEC025', 'NOT025', 'SIN DERECHO A RENOVACIÓN POR INCUMPLIMIENTO DEL CONVENIO'),
('BEC026', 'NOT026', ''),
('BEC027', 'NOT027', 'NO RENOVÓ BECA. NO SE CONOCE LA RAZÓN'),
('BEC028', 'NOT028', ''),
('BEC029', 'NOT029', ''),
('BEC030', 'NOT030', ''),
('BEC040', 'NOT040', ''),
('BEC041', 'NOT041', 'BAJA POR ABANDONO ESCOLAR'),
('BEC042', 'NOT042', 'SIN DERECHO A RENOVACIÓN POR INCUMPLIMIENTO DEL CONVENIO'),
('BEC043', 'NOT043', 'NO RENOVÓ BECA. NO SE CONOCE LA RAZÓN'),
('BEC044', 'NOT044', 'SIN DERECHO A RENOVACIÓN POR INCUMPLIMIENTO DEL CONVENIO'),
('BEC045', 'NOT045', 'DEBE UNA MATERIA DE SECUNDARIA. SE DARÁ PRÓRROGA PARA ENTREGAR EL CERTIFICADO'),
('BEC046', 'NOT046', ''),
('BEC047', 'NOT047', 'NO RENOVÓ BECA. NO SE CONOCE LA RAZÓN'),
('BEC048', 'NOT048', ''),
('BEC049', 'NOT049', 'AÚN NO PAGA LA REINSCRIPCIÓN A LA ESCUELA PUES LE DIERON PRÓRROGA PARA HACERLO, AUNQUE YA ESTÁ INSCRITA'),
('BEC050', 'NOT050', ''),
('BEC060', 'NOT060', ''),
('BEC061', 'NOT061', ''),
('BEC062', 'NOT062', ''),
('BEC063', 'NOT063', ''),
('BEC064', 'NOT064', ''),
('BEC065', 'NOT065', ''),
('BEC066', 'NOT066', 'PEDIR COPIA DEL INE PARA REALIZAR TRÁMITE DE TARJETA DEL BANCO'),
('BEC067', 'NOT067', ''),
('BEC068', 'NOT068', ''),
('BEC069', 'NOT069', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `nota`
--
ALTER TABLE `nota`
  ADD PRIMARY KEY (`IdNota`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `nota`
--
ALTER TABLE `nota`
  ADD CONSTRAINT `nota_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

CREATE TABLE `etnia` (
  `IdBecario` varchar(6) NOT NULL,
  `IdEtnia` varchar(6) NOT NULL,
  `HablaLengua` text,
  `Etnia` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `etnia`
--

INSERT INTO `etnia` (`IdBecario`, `IdEtnia`, `HablaLengua`, `Etnia`) VALUES
('BEC001', 'ETN001', 'SÍ', 'OTOMI'),
('BEC002', 'ETN002', 'SÍ', 'OTOMI'),
('BEC003', 'ETN003', 'SÍ', 'OTOMI'),
('BEC004', 'ETN004', 'SÍ', 'OTOMI'),
('BEC005', 'ETN005', 'NO', ''),
('BEC006', 'ETN006', 'SÍ', 'OTOMI'),
('BEC007', 'ETN007', 'SÍ', 'OTOMI'),
('BEC008', 'ETN008', 'SÍ', 'OTOMI'),
('BEC009', 'ETN009', 'NO', ''),
('BEC010', 'ETN010', 'SÍ', 'OTOMI'),
('BEC020', 'ETN020', 'SÍ', 'OTOMI'),
('BEC021', 'ETN021', 'SÍ', 'OTOMI'),
('BEC022', 'ETN022', 'SÍ', 'OTOMI'),
('BEC023', 'ETN023', 'SÍ', 'OTOMI'),
('BEC024', 'ETN024', 'SÍ', 'OTOMI'),
('BEC025', 'ETN025', 'SÍ', 'OTOMI'),
('BEC026', 'ETN026', 'SÍ', 'OTOMI'),
('BEC027', 'ETN027', 'SÍ', 'OTOMI'),
('BEC028', 'ETN028', 'SÍ', 'OTOMI'),
('BEC029', 'ETN029', 'SÍ', 'OTOMI'),
('BEC030', 'ETN030', 'SÍ', 'OTOMI'),
('BEC040', 'ETN040', 'SÍ', 'OTOMI'),
('BEC041', 'ETN041', 'SÍ', 'OTOMI'),
('BEC042', 'ETN042', 'NO', ''),
('BEC043', 'ETN043', 'SÍ', 'OTOMI'),
('BEC044', 'ETN044', 'SÍ', 'OTOMI'),
('BEC045', 'ETN045', 'SÍ', 'OTOMI'),
('BEC046', 'ETN046', 'SÍ', 'OTOMI'),
('BEC047', 'ETN047', 'SÍ', 'OTOMI'),
('BEC048', 'ETN048', 'SÍ', 'OTOMI'),
('BEC049', 'ETN049', 'SÍ', 'OTOMI'),
('BEC050', 'ETN050', 'SÍ', 'OTOMI'),
('BEC060', 'ETN060', 'SÍ', 'OTOMI'),
('BEC061', 'ETN061', 'SÍ', 'OTOMI'),
('BEC062', 'ETN062', 'SÍ', 'OTOMI'),
('BEC063', 'ETN063', 'SÍ', 'OTOMI'),
('BEC064', 'ETN064', 'SÍ', 'OTOMI'),
('BEC065', 'ETN065', 'SÍ', 'OTOMI'),
('BEC066', 'ETN066', 'SÍ', 'OTOMI'),
('BEC067', 'ETN067', 'SÍ', 'OTOMI'),
('BEC068', 'ETN068', 'SÍ', 'OTOMI'),
('BEC069', 'ETN069', 'SÍ', 'OTOMI');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `etnia`
--
ALTER TABLE `etnia`
  ADD PRIMARY KEY (`IdEtnia`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `etnia`
--
ALTER TABLE `etnia`
  ADD CONSTRAINT `etnia_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

CREATE TABLE `beneficio` (
  `IdBecario` varchar(6) NOT NULL,
  `IdBeneficio` varchar(6) NOT NULL,
  `NomBeneficio` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `beneficio`
--

INSERT INTO `beneficio` (`IdBecario`, `IdBeneficio`, `NomBeneficio`) VALUES
('BEC001', 'BEN001', 'PROSPERA'),
('BEC002', 'BEN002', 'PROSPERA'),
('BEC003', 'BEN003', 'PROSPERA'),
('BEC004', 'BEN004', 'PROSPERA'),
('BEC005', 'BEN005', ''),
('BEC006', 'BEN006', 'PROSPERA Y TSUNI'),
('BEC007', 'BEN007', 'PROSPERA'),
('BEC008', 'BEN008', 'PROSPERA'),
('BEC009', 'BEN009', 'PROSPERA'),
('BEC010', 'BEN010', ''),
('BEC020', 'BEN020', 'PROSPERA'),
('BEC021', 'BEN021', 'PROSPERA'),
('BEC022', 'BEN022', 'PROSPERA'),
('BEC023', 'BEN023', 'TSUNI'),
('BEC024', 'BEN024', 'PROSPERA'),
('BEC025', 'BEN025', 'PROSPERA'),
('BEC026', 'BEN026', 'PROSPERA'),
('BEC027', 'BEN027', 'PAL'),
('BEC028', 'BEN028', 'PAL '),
('BEC029', 'BEN029', 'PAL Y TSUNI'),
('BEC030', 'BEN030', 'PAL '),
('BEC040', 'BEN040', ''),
('BEC041', 'BEN041', 'PROSPERA'),
('BEC042', 'BEN042', 'PROSPERA'),
('BEC043', 'BEN043', 'PROSPERA'),
('BEC044', 'BEN044', 'PROSPERA'),
('BEC045', 'BEN045', 'PROSPERA Y PAL'),
('BEC046', 'BEN046', ''),
('BEC047', 'BEN047', 'PROSPERA'),
('BEC048', 'BEN048', 'PROSPERA'),
('BEC049', 'BEN049', 'PROSPERA'),
('BEC050', 'BEN050', 'PROSPERA'),
('BEC060', 'BEN060', 'PROSPERA'),
('BEC061', 'BEN061', ''),
('BEC062', 'BEN062', 'PROSPERA'),
('BEC063', 'BEN063', ''),
('BEC064', 'BEN064', 'PROSPERA'),
('BEC065', 'BEN065', ''),
('BEC066', 'BEN066', 'PROSPERA'),
('BEC067', 'BEN067', 'PROSPERA'),
('BEC068', 'BEN068', ''),
('BEC069', 'BEN069', 'PROSPERA');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `beneficio`
--
ALTER TABLE `beneficio`
  ADD PRIMARY KEY (`IdBeneficio`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `beneficio`
--
ALTER TABLE `beneficio`
  ADD CONSTRAINT `beneficio_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;




CREATE TABLE `documentacion` (
  `IdBecario` varchar(6) NOT NULL,
  `IdDoc` varchar(7) NOT NULL,
  `Nombre` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `documentacion`
--

INSERT INTO `documentacion` (`IdBecario`, `IdDoc`, `Nombre`) VALUES
('BEC001', 'DOC0002', 'Acta de Nacimiento'),
('BEC002', 'DOC0004', 'CURP'),
('BEC003', 'DOC0006', 'Certificado secundaria'),
('BEC004', 'DOC0008', 'Carta de maestros'),
('BEC005', 'DOC0010', 'Constancia de recursos economicos'),
('BEC006', 'DOC0012', 'Carta de padres de familia'),
('BEC007', 'DOC0014', 'INE'),
('BEC008', 'DOC0016', 'CURP Madre o Padre'),
('BEC009', 'DOC0018', 'Boletas de calificaciones actualizada'),
('BEC010', 'DOC0020', 'Constancia de inscripcion en la escuela'),
('BEC020', 'DOC0022', 'Dictamen medico'),
('BEC021', 'DOC0024', 'Constrancia trabajo comunitario'),
('BEC022', 'DOC0026', 'Una fotografia'),
('BEC023', 'DOC0028', 'Copia de credencial de fundacion'),
('BEC024', 'DOC0030', 'Convenio Actualizado'),
('BEC025', 'DOC0032', 'Tarjeta de debito'),
('BEC026', 'DOC0034', 'Carta compromiso servicio comunitario'),
('BEC027', 'DOC0036', 'Evaluacion de cumplimiento de convenio'),
('BEC028', 'DOC0038', 'Acta de Nacimiento'),
('BEC029', 'DOC0040', 'CURP'),
('BEC030', 'DOC0042', 'Certificado secundaria'),
('BEC040', 'DOC0044', 'Carta de maestros'),
('BEC041', 'DOC0046', 'Constancia de recursos economicos'),
('BEC042', 'DOC0048', 'Carta de padres de familia'),
('BEC043', 'DOC0050', 'INE'),
('BEC044', 'DOC0052', 'CURP Madre o Padre'),
('BEC045', 'DOC0054', 'Boletas de calificaciones actualizada'),
('BEC046', 'DOC0056', 'Constancia de inscripcion en la escuela'),
('BEC047', 'DOC0058', 'Dictamen medico'),
('BEC048', 'DOC0060', 'Constrancia trabajo comunitario'),
('BEC049', 'DOC0062', 'Una fotografia'),
('BEC050', 'DOC0064', 'Copia de credencial de fundacion'),
('BEC060', 'DOC0066', 'Convenio Actualizado'),
('BEC061', 'DOC0068', 'Tarjeta de debito'),
('BEC062', 'DOC0070', 'Carta compromiso servicio comunitario'),
('BEC063', 'DOC0072', 'Evaluacion de cumplimiento de convenio'),
('BEC064', 'DOC0074', 'Acta de Nacimiento'),
('BEC065', 'DOC0076', 'CURP'),
('BEC066', 'DOC0078', 'Certificado secundaria'),
('BEC067', 'DOC0080', 'Carta de maestros'),
('BEC068', 'DOC0082', 'Constancia de recursos economicos'),
('BEC069', 'DOC0084', 'Carta de padres de familia'),
('BEC001', 'DOC0086', 'INE'),
('BEC002', 'DOC0088', 'CURP Madre o Padre'),
('BEC003', 'DOC0090', 'Boletas de calificaciones actualizada'),
('BEC004', 'DOC0092', 'Constancia de inscripcion en la escuela'),
('BEC005', 'DOC0094', 'Dictamen medico'),
('BEC006', 'DOC0096', 'Constrancia trabajo comunitario'),
('BEC007', 'DOC0098', 'Una fotografia'),
('BEC008', 'DOC0100', 'Copia de credencial de fundacion'),
('BEC009', 'DOC0102', 'Convenio Actualizado'),
('BEC010', 'DOC0104', 'Tarjeta de debito'),
('BEC020', 'DOC0106', 'Carta compromiso servicio comunitario'),
('BEC021', 'DOC0108', 'Evaluacion de cumplimiento de convenio'),
('BEC022', 'DOC0110', 'Acta de Nacimiento'),
('BEC023', 'DOC0112', 'CURP'),
('BEC024', 'DOC0114', 'Certificado secundaria'),
('BEC025', 'DOC0116', 'Carta de maestros'),
('BEC026', 'DOC0118', 'Constancia de recursos economicos'),
('BEC027', 'DOC0120', 'Carta de padres de familia'),
('BEC028', 'DOC0122', 'INE'),
('BEC029', 'DOC0124', 'CURP Madre o Padre'),
('BEC030', 'DOC0126', 'Boletas de calificaciones actualizada'),
('BEC040', 'DOC0128', 'Constancia de inscripcion en la escuela'),
('BEC041', 'DOC0130', 'Dictamen medico'),
('BEC042', 'DOC0132', 'Constrancia trabajo comunitario'),
('BEC043', 'DOC0134', 'Una fotografia'),
('BEC044', 'DOC0136', 'Copia de credencial de fundacion'),
('BEC045', 'DOC0138', 'Convenio Actualizado'),
('BEC046', 'DOC0140', 'Tarjeta de debito'),
('BEC047', 'DOC0142', 'Carta compromiso servicio comunitario'),
('BEC048', 'DOC0144', 'Evaluacion de cumplimiento de convenio'),
('BEC049', 'DOC0146', 'Acta de Nacimiento'),
('BEC050', 'DOC0148', 'CURP'),
('BEC060', 'DOC0150', 'Certificado secundaria'),
('BEC061', 'DOC0152', 'Carta de maestros'),
('BEC062', 'DOC0154', 'Constancia de recursos economicos'),
('BEC063', 'DOC0156', 'Carta de padres de familia'),
('BEC064', 'DOC0158', 'INE'),
('BEC065', 'DOC0160', 'CURP Madre o Padre'),
('BEC066', 'DOC0162', 'Boletas de calificaciones actualizada'),
('BEC067', 'DOC0164', 'Constancia de inscripcion en la escuela'),
('BEC068', 'DOC0166', 'Dictamen medico'),
('BEC069', 'DOC0168', 'Constrancia trabajo comunitario'),
('BEC001', 'DOC0170', 'Una fotografia'),
('BEC002', 'DOC0172', 'Copia de credencial de fundacion'),
('BEC003', 'DOC0174', 'Convenio Actualizado'),
('BEC004', 'DOC0176', 'Tarjeta de debito'),
('BEC005', 'DOC0178', 'Carta compromiso servicio comunitario'),
('BEC006', 'DOC0180', 'Evaluacion de cumplimiento de convenio'),
('BEC007', 'DOC0182', 'Acta de Nacimiento'),
('BEC008', 'DOC0184', 'CURP'),
('BEC009', 'DOC0186', 'Certificado secundaria'),
('BEC010', 'DOC0188', 'Carta de maestros'),
('BEC020', 'DOC0190', 'Constancia de recursos economicos'),
('BEC021', 'DOC0192', 'Carta de padres de familia'),
('BEC022', 'DOC0194', 'INE'),
('BEC023', 'DOC0196', 'CURP Madre o Padre'),
('BEC024', 'DOC0198', 'Boletas de calificaciones actualizada'),
('BEC025', 'DOC0200', 'Constancia de inscripcion en la escuela'),
('BEC026', 'DOC0202', 'Dictamen medico'),
('BEC027', 'DOC0204', 'Constrancia trabajo comunitario'),
('BEC028', 'DOC0206', 'Una fotografia'),
('BEC029', 'DOC0208', 'Copia de credencial de fundacion'),
('BEC030', 'DOC0210', 'Convenio Actualizado'),
('BEC040', 'DOC0212', 'Tarjeta de debito'),
('BEC041', 'DOC0214', 'Carta compromiso servicio comunitario'),
('BEC042', 'DOC0216', 'Evaluacion de cumplimiento de convenio'),
('BEC043', 'DOC0218', 'Acta de Nacimiento'),
('BEC044', 'DOC0220', 'CURP'),
('BEC045', 'DOC0222', 'Certificado secundaria'),
('BEC046', 'DOC0224', 'Carta de maestros'),
('BEC047', 'DOC0226', 'Constancia de recursos economicos'),
('BEC048', 'DOC0228', 'Carta de padres de familia'),
('BEC049', 'DOC0230', 'INE'),
('BEC050', 'DOC0232', 'CURP Madre o Padre'),
('BEC060', 'DOC0234', 'Boletas de calificaciones actualizada'),
('BEC061', 'DOC0236', 'Constancia de inscripcion en la escuela'),
('BEC062', 'DOC0238', 'Dictamen medico'),
('BEC063', 'DOC0240', 'Constrancia trabajo comunitario'),
('BEC064', 'DOC0242', 'Una fotografia'),
('BEC065', 'DOC0244', 'Copia de credencial de fundacion'),
('BEC066', 'DOC0246', 'Convenio Actualizado'),
('BEC067', 'DOC0248', 'Tarjeta de debito'),
('BEC068', 'DOC0250', 'Carta compromiso servicio comunitario'),
('BEC069', 'DOC0252', 'Evaluacion de cumplimiento de convenio'),
('BEC001', 'DOC0254', 'Acta de Nacimiento'),
('BEC002', 'DOC0256', 'CURP'),
('BEC003', 'DOC0258', 'Certificado secundaria'),
('BEC004', 'DOC0260', 'Carta de maestros'),
('BEC005', 'DOC0262', 'Constancia de recursos economicos'),
('BEC006', 'DOC0264', 'Carta de padres de familia'),
('BEC007', 'DOC0266', 'INE'),
('BEC008', 'DOC0268', 'CURP Madre o Padre'),
('BEC009', 'DOC0270', 'Boletas de calificaciones actualizada'),
('BEC010', 'DOC0272', 'Constancia de inscripcion en la escuela'),
('BEC020', 'DOC0274', 'Dictamen medico'),
('BEC021', 'DOC0276', 'Constrancia trabajo comunitario'),
('BEC022', 'DOC0278', 'Una fotografia'),
('BEC023', 'DOC0280', 'Copia de credencial de fundacion'),
('BEC024', 'DOC0282', 'Convenio Actualizado'),
('BEC025', 'DOC0284', 'Tarjeta de debito'),
('BEC026', 'DOC0286', 'Carta compromiso servicio comunitario'),
('BEC027', 'DOC0288', 'Evaluacion de cumplimiento de convenio'),
('BEC028', 'DOC0290', 'Acta de Nacimiento'),
('BEC029', 'DOC0292', 'CURP'),
('BEC030', 'DOC0294', 'Certificado secundaria'),
('BEC040', 'DOC0296', 'Carta de maestros'),
('BEC041', 'DOC0298', 'Constancia de recursos economicos'),
('BEC042', 'DOC0300', 'Carta de padres de familia'),
('BEC043', 'DOC0302', 'INE'),
('BEC044', 'DOC0304', 'CURP Madre o Padre'),
('BEC045', 'DOC0306', 'Boletas de calificaciones actualizada'),
('BEC046', 'DOC0308', 'Constancia de inscripcion en la escuela'),
('BEC047', 'DOC0310', 'Dictamen medico'),
('BEC048', 'DOC0312', 'Constrancia trabajo comunitario'),
('BEC049', 'DOC0314', 'Una fotografia'),
('BEC050', 'DOC0316', 'Copia de credencial de fundacion'),
('BEC060', 'DOC0318', 'Convenio Actualizado'),
('BEC061', 'DOC0320', 'Tarjeta de debito'),
('BEC062', 'DOC0322', 'Carta compromiso servicio comunitario'),
('BEC063', 'DOC0324', 'Evaluacion de cumplimiento de convenio'),
('BEC064', 'DOC0326', 'Acta de Nacimiento'),
('BEC065', 'DOC0328', 'CURP'),
('BEC066', 'DOC0330', 'Certificado secundaria'),
('BEC067', 'DOC0332', 'Carta de maestros'),
('BEC068', 'DOC0334', 'Constancia de recursos economicos'),
('BEC069', 'DOC0336', 'Carta de padres de familia'),
('BEC001', 'DOC0338', 'INE'),
('BEC002', 'DOC0340', 'CURP Madre o Padre'),
('BEC003', 'DOC0342', 'Boletas de calificaciones actualizada'),
('BEC004', 'DOC0344', 'Constancia de inscripcion en la escuela'),
('BEC005', 'DOC0346', 'Dictamen medico'),
('BEC006', 'DOC0348', 'Constrancia trabajo comunitario'),
('BEC007', 'DOC0350', 'Una fotografia'),
('BEC008', 'DOC0352', 'Copia de credencial de fundacion'),
('BEC009', 'DOC0354', 'Convenio Actualizado'),
('BEC010', 'DOC0356', 'Tarjeta de debito'),
('BEC020', 'DOC0358', 'Carta compromiso servicio comunitario'),
('BEC021', 'DOC0360', 'Evaluacion de cumplimiento de convenio'),
('BEC022', 'DOC0362', 'Acta de Nacimiento'),
('BEC023', 'DOC0364', 'CURP'),
('BEC024', 'DOC0366', 'Certificado secundaria'),
('BEC025', 'DOC0368', 'Carta de maestros'),
('BEC026', 'DOC0370', 'Constancia de recursos economicos'),
('BEC027', 'DOC0372', 'Carta de padres de familia'),
('BEC028', 'DOC0374', 'INE'),
('BEC029', 'DOC0376', 'CURP Madre o Padre'),
('BEC030', 'DOC0378', 'Boletas de calificaciones actualizada'),
('BEC040', 'DOC0380', 'Constancia de inscripcion en la escuela'),
('BEC041', 'DOC0382', 'Dictamen medico'),
('BEC042', 'DOC0384', 'Constrancia trabajo comunitario'),
('BEC043', 'DOC0386', 'Una fotografia'),
('BEC044', 'DOC0388', 'Copia de credencial de fundacion'),
('BEC045', 'DOC0390', 'Convenio Actualizado'),
('BEC046', 'DOC0392', 'Tarjeta de debito'),
('BEC047', 'DOC0394', 'Carta compromiso servicio comunitario'),
('BEC048', 'DOC0396', 'Evaluacion de cumplimiento de convenio'),
('BEC049', 'DOC0398', 'Acta de Nacimiento'),
('BEC050', 'DOC0400', 'CURP'),
('BEC060', 'DOC0402', 'Certificado secundaria'),
('BEC061', 'DOC0404', 'Carta de maestros'),
('BEC062', 'DOC0406', 'Constancia de recursos economicos'),
('BEC063', 'DOC0408', 'Carta de padres de familia'),
('BEC064', 'DOC0410', 'INE'),
('BEC065', 'DOC0412', 'CURP Madre o Padre'),
('BEC066', 'DOC0414', 'Boletas de calificaciones actualizada'),
('BEC067', 'DOC0416', 'Constancia de inscripcion en la escuela'),
('BEC068', 'DOC0418', 'Dictamen medico'),
('BEC069', 'DOC0420', 'Constrancia trabajo comunitario'),
('BEC001', 'DOC0422', 'Una fotografia'),
('BEC002', 'DOC0424', 'Copia de credencial de fundacion'),
('BEC003', 'DOC0426', 'Convenio Actualizado'),
('BEC004', 'DOC0428', 'Tarjeta de debito'),
('BEC005', 'DOC0430', 'Carta compromiso servicio comunitario'),
('BEC006', 'DOC0432', 'Evaluacion de cumplimiento de convenio'),
('BEC007', 'DOC0434', 'Acta de Nacimiento'),
('BEC008', 'DOC0436', 'CURP'),
('BEC009', 'DOC0438', 'Certificado secundaria'),
('BEC010', 'DOC0440', 'Carta de maestros'),
('BEC020', 'DOC0442', 'Constancia de recursos economicos'),
('BEC021', 'DOC0444', 'Carta de padres de familia'),
('BEC022', 'DOC0446', 'INE'),
('BEC023', 'DOC0448', 'CURP Madre o Padre'),
('BEC024', 'DOC0450', 'Boletas de calificaciones actualizada'),
('BEC025', 'DOC0452', 'Constancia de inscripcion en la escuela'),
('BEC026', 'DOC0454', 'Dictamen medico'),
('BEC027', 'DOC0456', 'Constrancia trabajo comunitario'),
('BEC028', 'DOC0458', 'Una fotografia'),
('BEC029', 'DOC0460', 'Copia de credencial de fundacion'),
('BEC030', 'DOC0462', 'Convenio Actualizado'),
('BEC040', 'DOC0464', 'Tarjeta de debito'),
('BEC041', 'DOC0466', 'Carta compromiso servicio comunitario'),
('BEC042', 'DOC0468', 'Evaluacion de cumplimiento de convenio'),
('BEC043', 'DOC0470', 'Acta de Nacimiento'),
('BEC044', 'DOC0472', 'CURP'),
('BEC045', 'DOC0474', 'Certificado secundaria'),
('BEC046', 'DOC0476', 'Carta de maestros'),
('BEC047', 'DOC0478', 'Constancia de recursos economicos'),
('BEC048', 'DOC0480', 'Carta de padres de familia'),
('BEC049', 'DOC0482', 'INE'),
('BEC050', 'DOC0484', 'CURP Madre o Padre'),
('BEC060', 'DOC0486', 'Boletas de calificaciones actualizada'),
('BEC061', 'DOC0488', 'Constancia de inscripcion en la escuela'),
('BEC062', 'DOC0490', 'Dictamen medico'),
('BEC063', 'DOC0492', 'Constrancia trabajo comunitario'),
('BEC064', 'DOC0494', 'Una fotografia'),
('BEC065', 'DOC0496', 'Copia de credencial de fundacion'),
('BEC066', 'DOC0498', 'Convenio Actualizado'),
('BEC067', 'DOC0500', 'Tarjeta de debito'),
('BEC068', 'DOC0502', 'Carta compromiso servicio comunitario'),
('BEC069', 'DOC0504', 'Evaluacion de cumplimiento de convenio'),
('BEC001', 'DOC0506', 'Acta de Nacimiento'),
('BEC002', 'DOC0508', 'CURP'),
('BEC003', 'DOC0510', 'Certificado secundaria'),
('BEC004', 'DOC0512', 'Carta de maestros'),
('BEC005', 'DOC0514', 'Constancia de recursos economicos'),
('BEC006', 'DOC0516', 'Carta de padres de familia'),
('BEC007', 'DOC0518', 'INE'),
('BEC008', 'DOC0520', 'CURP Madre o Padre'),
('BEC009', 'DOC0522', 'Boletas de calificaciones actualizada'),
('BEC010', 'DOC0524', 'Constancia de inscripcion en la escuela'),
('BEC020', 'DOC0526', 'Dictamen medico'),
('BEC021', 'DOC0528', 'Constrancia trabajo comunitario'),
('BEC022', 'DOC0530', 'Una fotografia'),
('BEC023', 'DOC0532', 'Copia de credencial de fundacion'),
('BEC024', 'DOC0534', 'Convenio Actualizado'),
('BEC025', 'DOC0536', 'Tarjeta de debito'),
('BEC026', 'DOC0538', 'Carta compromiso servicio comunitario'),
('BEC027', 'DOC0540', 'Evaluacion de cumplimiento de convenio'),
('BEC028', 'DOC0542', 'Acta de Nacimiento'),
('BEC029', 'DOC0544', 'CURP'),
('BEC030', 'DOC0546', 'Certificado secundaria'),
('BEC040', 'DOC0548', 'Carta de maestros'),
('BEC041', 'DOC0550', 'Constancia de recursos economicos'),
('BEC042', 'DOC0552', 'Carta de padres de familia'),
('BEC043', 'DOC0554', 'INE'),
('BEC044', 'DOC0556', 'CURP Madre o Padre'),
('BEC045', 'DOC0558', 'Boletas de calificaciones actualizada'),
('BEC046', 'DOC0560', 'Constancia de inscripcion en la escuela'),
('BEC047', 'DOC0562', 'Dictamen medico'),
('BEC048', 'DOC0564', 'Constrancia trabajo comunitario'),
('BEC049', 'DOC0566', 'Una fotografia'),
('BEC050', 'DOC0568', 'Copia de credencial de fundacion'),
('BEC060', 'DOC0570', 'Convenio Actualizado'),
('BEC061', 'DOC0572', 'Tarjeta de debito'),
('BEC062', 'DOC0574', 'Carta compromiso servicio comunitario'),
('BEC063', 'DOC0576', 'Evaluacion de cumplimiento de convenio'),
('BEC064', 'DOC0578', 'Acta de Nacimiento'),
('BEC065', 'DOC0580', 'CURP'),
('BEC066', 'DOC0582', 'Certificado secundaria'),
('BEC067', 'DOC0584', 'Carta de maestros'),
('BEC068', 'DOC0586', 'Constancia de recursos economicos'),
('BEC069', 'DOC0588', 'Carta de padres de familia'),
('BEC001', 'DOC0590', 'INE'),
('BEC002', 'DOC0592', 'CURP Madre o Padre'),
('BEC003', 'DOC0594', 'Boletas de calificaciones actualizada'),
('BEC004', 'DOC0596', 'Constancia de inscripcion en la escuela'),
('BEC005', 'DOC0598', 'Dictamen medico'),
('BEC006', 'DOC0600', 'Constrancia trabajo comunitario'),
('BEC007', 'DOC0602', 'Una fotografia'),
('BEC008', 'DOC0604', 'Copia de credencial de fundacion'),
('BEC009', 'DOC0606', 'Convenio Actualizado'),
('BEC010', 'DOC0608', 'Tarjeta de debito'),
('BEC020', 'DOC0610', 'Carta compromiso servicio comunitario'),
('BEC021', 'DOC0612', 'Evaluacion de cumplimiento de convenio'),
('BEC022', 'DOC0614', 'Acta de Nacimiento'),
('BEC023', 'DOC0616', 'CURP'),
('BEC024', 'DOC0618', 'Certificado secundaria'),
('BEC025', 'DOC0620', 'Carta de maestros'),
('BEC026', 'DOC0622', 'Constancia de recursos economicos'),
('BEC027', 'DOC0624', 'Carta de padres de familia'),
('BEC028', 'DOC0626', 'INE'),
('BEC029', 'DOC0628', 'CURP Madre o Padre'),
('BEC030', 'DOC0630', 'Boletas de calificaciones actualizada'),
('BEC040', 'DOC0632', 'Constancia de inscripcion en la escuela'),
('BEC041', 'DOC0634', 'Dictamen medico'),
('BEC042', 'DOC0636', 'Constrancia trabajo comunitario'),
('BEC043', 'DOC0638', 'Una fotografia'),
('BEC044', 'DOC0640', 'Copia de credencial de fundacion'),
('BEC045', 'DOC0642', 'Convenio Actualizado'),
('BEC046', 'DOC0644', 'Tarjeta de debito'),
('BEC047', 'DOC0646', 'Carta compromiso servicio comunitario'),
('BEC048', 'DOC0648', 'Evaluacion de cumplimiento de convenio'),
('BEC049', 'DOC0650', 'Acta de Nacimiento'),
('BEC050', 'DOC0652', 'CURP'),
('BEC060', 'DOC0654', 'Certificado secundaria'),
('BEC061', 'DOC0656', 'Carta de maestros'),
('BEC062', 'DOC0658', 'Constancia de recursos economicos'),
('BEC063', 'DOC0660', 'Carta de padres de familia'),
('BEC064', 'DOC0662', 'INE'),
('BEC065', 'DOC0664', 'CURP Madre o Padre'),
('BEC066', 'DOC0666', 'Boletas de calificaciones actualizada'),
('BEC067', 'DOC0668', 'Constancia de inscripcion en la escuela'),
('BEC068', 'DOC0670', 'Dictamen medico'),
('BEC069', 'DOC0672', 'Constrancia trabajo comunitario'),
('BEC001', 'DOC0674', 'Una fotografia'),
('BEC002', 'DOC0676', 'Copia de credencial de fundacion'),
('BEC003', 'DOC0678', 'Convenio Actualizado'),
('BEC004', 'DOC0680', 'Tarjeta de debito'),
('BEC005', 'DOC0682', 'Carta compromiso servicio comunitario'),
('BEC006', 'DOC0684', 'Evaluacion de cumplimiento de convenio'),
('BEC007', 'DOC0686', 'Acta de Nacimiento'),
('BEC008', 'DOC0688', 'CURP'),
('BEC009', 'DOC0690', 'Certificado secundaria'),
('BEC010', 'DOC0692', 'Carta de maestros'),
('BEC020', 'DOC0694', 'Constancia de recursos economicos'),
('BEC021', 'DOC0696', 'Carta de padres de familia'),
('BEC022', 'DOC0698', 'INE'),
('BEC023', 'DOC0700', 'CURP Madre o Padre'),
('BEC024', 'DOC0702', 'Boletas de calificaciones actualizada'),
('BEC025', 'DOC0704', 'Constancia de inscripcion en la escuela'),
('BEC026', 'DOC0706', 'Dictamen medico'),
('BEC027', 'DOC0708', 'Constrancia trabajo comunitario'),
('BEC028', 'DOC0710', 'Una fotografia'),
('BEC029', 'DOC0712', 'Copia de credencial de fundacion'),
('BEC030', 'DOC0714', 'Convenio Actualizado'),
('BEC040', 'DOC0716', 'Tarjeta de debito'),
('BEC041', 'DOC0718', 'Carta compromiso servicio comunitario'),
('BEC042', 'DOC0720', 'Evaluacion de cumplimiento de convenio'),
('BEC043', 'DOC0722', 'Acta de Nacimiento'),
('BEC044', 'DOC0724', 'CURP'),
('BEC045', 'DOC0726', 'Certificado secundaria'),
('BEC046', 'DOC0728', 'Carta de maestros'),
('BEC047', 'DOC0730', 'Constancia de recursos economicos'),
('BEC048', 'DOC0732', 'Carta de padres de familia'),
('BEC049', 'DOC0734', 'INE'),
('BEC050', 'DOC0736', 'CURP Madre o Padre'),
('BEC060', 'DOC0738', 'Boletas de calificaciones actualizada'),
('BEC061', 'DOC0740', 'Constancia de inscripcion en la escuela'),
('BEC062', 'DOC0742', 'Dictamen medico'),
('BEC063', 'DOC0744', 'Constrancia trabajo comunitario'),
('BEC064', 'DOC0746', 'Una fotografia'),
('BEC065', 'DOC0748', 'Copia de credencial de fundacion'),
('BEC066', 'DOC0750', 'Convenio Actualizado'),
('BEC067', 'DOC0752', 'Tarjeta de debito'),
('BEC068', 'DOC0754', 'Carta compromiso servicio comunitario'),
('BEC069', 'DOC0756', 'Evaluacion de cumplimiento de convenio'),
('BEC001', 'DOC0758', 'Acta de Nacimiento'),
('BEC002', 'DOC0760', 'CURP'),
('BEC003', 'DOC0762', 'Certificado secundaria'),
('BEC004', 'DOC0764', 'Carta de maestros'),
('BEC005', 'DOC0766', 'Constancia de recursos economicos'),
('BEC006', 'DOC0768', 'Carta de padres de familia'),
('BEC007', 'DOC0770', 'INE'),
('BEC008', 'DOC0772', 'CURP Madre o Padre'),
('BEC009', 'DOC0774', 'Boletas de calificaciones actualizada'),
('BEC010', 'DOC0776', 'Constancia de inscripcion en la escuela'),
('BEC020', 'DOC0778', 'Dictamen medico'),
('BEC021', 'DOC0780', 'Constrancia trabajo comunitario'),
('BEC022', 'DOC0782', 'Una fotografia'),
('BEC023', 'DOC0784', 'Copia de credencial de fundacion'),
('BEC024', 'DOC0786', 'Convenio Actualizado'),
('BEC025', 'DOC0788', 'Tarjeta de debito'),
('BEC026', 'DOC0790', 'Carta compromiso servicio comunitario'),
('BEC027', 'DOC0792', 'Evaluacion de cumplimiento de convenio'),
('BEC028', 'DOC0794', 'Acta de Nacimiento'),
('BEC029', 'DOC0796', 'CURP'),
('BEC030', 'DOC0798', 'Certificado secundaria'),
('BEC040', 'DOC0800', 'Carta de maestros'),
('BEC041', 'DOC0802', 'Constancia de recursos economicos'),
('BEC042', 'DOC0804', 'Carta de padres de familia'),
('BEC043', 'DOC0806', 'INE'),
('BEC044', 'DOC0808', 'CURP Madre o Padre'),
('BEC045', 'DOC0810', 'Boletas de calificaciones actualizada'),
('BEC046', 'DOC0812', 'Constancia de inscripcion en la escuela'),
('BEC047', 'DOC0814', 'Dictamen medico'),
('BEC048', 'DOC0816', 'Constrancia trabajo comunitario'),
('BEC049', 'DOC0818', 'Una fotografia'),
('BEC050', 'DOC0820', 'Copia de credencial de fundacion'),
('BEC060', 'DOC0822', 'Convenio Actualizado'),
('BEC061', 'DOC0824', 'Tarjeta de debito'),
('BEC062', 'DOC0826', 'Carta compromiso servicio comunitario'),
('BEC063', 'DOC0828', 'Evaluacion de cumplimiento de convenio'),
('BEC064', 'DOC0830', 'Acta de Nacimiento'),
('BEC065', 'DOC0832', 'CURP'),
('BEC066', 'DOC0834', 'Certificado secundaria'),
('BEC067', 'DOC0836', 'Carta de maestros'),
('BEC068', 'DOC0838', 'Constancia de recursos economicos'),
('BEC069', 'DOC0840', 'Carta de padres de familia'),
('BEC001', 'DOC0842', 'INE'),
('BEC002', 'DOC0844', 'CURP Madre o Padre'),
('BEC003', 'DOC0846', 'Boletas de calificaciones actualizada'),
('BEC004', 'DOC0848', 'Constancia de inscripcion en la escuela'),
('BEC005', 'DOC0850', 'Dictamen medico'),
('BEC006', 'DOC0852', 'Constrancia trabajo comunitario'),
('BEC007', 'DOC0854', 'Una fotografia'),
('BEC008', 'DOC0856', 'Copia de credencial de fundacion'),
('BEC009', 'DOC0858', 'Convenio Actualizado'),
('BEC010', 'DOC0860', 'Tarjeta de debito'),
('BEC020', 'DOC0862', 'Carta compromiso servicio comunitario'),
('BEC021', 'DOC0864', 'Evaluacion de cumplimiento de convenio'),
('BEC022', 'DOC0866', 'Acta de Nacimiento'),
('BEC023', 'DOC0868', 'CURP'),
('BEC024', 'DOC0870', 'Certificado secundaria'),
('BEC025', 'DOC0872', 'Carta de maestros'),
('BEC026', 'DOC0874', 'Constancia de recursos economicos'),
('BEC027', 'DOC0876', 'Carta de padres de familia'),
('BEC028', 'DOC0878', 'INE'),
('BEC029', 'DOC0880', 'CURP Madre o Padre'),
('BEC030', 'DOC0882', 'Boletas de calificaciones actualizada'),
('BEC040', 'DOC0884', 'Constancia de inscripcion en la escuela'),
('BEC041', 'DOC0886', 'Dictamen medico'),
('BEC042', 'DOC0888', 'Constrancia trabajo comunitario'),
('BEC043', 'DOC0890', 'Una fotografia'),
('BEC044', 'DOC0892', 'Copia de credencial de fundacion'),
('BEC045', 'DOC0894', 'Convenio Actualizado'),
('BEC046', 'DOC0896', 'Tarjeta de debito'),
('BEC047', 'DOC0898', 'Carta compromiso servicio comunitario'),
('BEC048', 'DOC0900', 'Evaluacion de cumplimiento de convenio'),
('BEC049', 'DOC0902', 'Acta de Nacimiento'),
('BEC050', 'DOC0904', 'CURP'),
('BEC060', 'DOC0906', 'Certificado secundaria'),
('BEC061', 'DOC0908', 'Carta de maestros'),
('BEC062', 'DOC0910', 'Constancia de recursos economicos'),
('BEC063', 'DOC0912', 'Carta de padres de familia'),
('BEC064', 'DOC0914', 'INE'),
('BEC065', 'DOC0916', 'CURP Madre o Padre'),
('BEC066', 'DOC0918', 'Boletas de calificaciones actualizada'),
('BEC067', 'DOC0920', 'Constancia de inscripcion en la escuela'),
('BEC068', 'DOC0922', 'Dictamen medico'),
('BEC069', 'DOC0924', 'Constrancia trabajo comunitario'),
('BEC001', 'DOC0926', 'Una fotografia'),
('BEC002', 'DOC0928', 'Copia de credencial de fundacion'),
('BEC003', 'DOC0930', 'Convenio Actualizado'),
('BEC004', 'DOC0932', 'Tarjeta de debito'),
('BEC005', 'DOC0934', 'Carta compromiso servicio comunitario'),
('BEC006', 'DOC0936', 'Evaluacion de cumplimiento de convenio'),
('BEC007', 'DOC0938', 'Acta de Nacimiento'),
('BEC008', 'DOC0940', 'CURP'),
('BEC009', 'DOC0942', 'Certificado secundaria'),
('BEC010', 'DOC0944', 'Carta de maestros'),
('BEC020', 'DOC0946', 'Constancia de recursos economicos'),
('BEC021', 'DOC0948', 'Carta de padres de familia'),
('BEC022', 'DOC0950', 'INE'),
('BEC023', 'DOC0952', 'CURP Madre o Padre'),
('BEC024', 'DOC0954', 'Boletas de calificaciones actualizada'),
('BEC025', 'DOC0956', 'Constancia de inscripcion en la escuela'),
('BEC026', 'DOC0958', 'Dictamen medico'),
('BEC027', 'DOC0960', 'Constrancia trabajo comunitario'),
('BEC028', 'DOC0962', 'Una fotografia'),
('BEC029', 'DOC0964', 'Copia de credencial de fundacion'),
('BEC030', 'DOC0966', 'Convenio Actualizado'),
('BEC040', 'DOC0968', 'Tarjeta de debito'),
('BEC041', 'DOC0970', 'Carta compromiso servicio comunitario'),
('BEC042', 'DOC0972', 'Evaluacion de cumplimiento de convenio'),
('BEC043', 'DOC0974', 'Acta de Nacimiento'),
('BEC044', 'DOC0976', 'CURP'),
('BEC045', 'DOC0978', 'Certificado secundaria'),
('BEC046', 'DOC0980', 'Carta de maestros'),
('BEC047', 'DOC0982', 'Constancia de recursos economicos'),
('BEC048', 'DOC0984', 'Carta de padres de familia'),
('BEC049', 'DOC0986', 'INE'),
('BEC050', 'DOC0988', 'CURP Madre o Padre'),
('BEC060', 'DOC0990', 'Boletas de calificaciones actualizada'),
('BEC061', 'DOC0992', 'Constancia de inscripcion en la escuela'),
('BEC062', 'DOC0994', 'Dictamen medico'),
('BEC063', 'DOC0996', 'Constrancia trabajo comunitario'),
('BEC064', 'DOC0998', 'Una fotografia'),
('BEC065', 'DOC1000', 'Copia de credencial de fundacion'),
('BEC066', 'DOC1002', 'Convenio Actualizado'),
('BEC067', 'DOC1004', 'Tarjeta de debito'),
('BEC068', 'DOC1006', 'Carta compromiso servicio comunitario'),
('BEC069', 'DOC1008', 'Evaluacion de cumplimiento de convenio'),
('BEC001', 'DOC1010', 'Acta de Nacimiento'),
('BEC002', 'DOC1012', 'CURP'),
('BEC003', 'DOC1014', 'Certificado secundaria'),
('BEC004', 'DOC1016', 'Carta de maestros'),
('BEC005', 'DOC1018', 'Constancia de recursos economicos'),
('BEC006', 'DOC1020', 'Carta de padres de familia'),
('BEC007', 'DOC1022', 'INE'),
('BEC008', 'DOC1024', 'CURP Madre o Padre'),
('BEC009', 'DOC1026', 'Boletas de calificaciones actualizada'),
('BEC010', 'DOC1028', 'Constancia de inscripcion en la escuela'),
('BEC020', 'DOC1030', 'Dictamen medico'),
('BEC021', 'DOC1032', 'Constrancia trabajo comunitario'),
('BEC022', 'DOC1034', 'Una fotografia'),
('BEC023', 'DOC1036', 'Copia de credencial de fundacion'),
('BEC024', 'DOC1038', 'Convenio Actualizado'),
('BEC025', 'DOC1040', 'Tarjeta de debito'),
('BEC026', 'DOC1042', 'Carta compromiso servicio comunitario'),
('BEC027', 'DOC1044', 'Evaluacion de cumplimiento de convenio'),
('BEC028', 'DOC1046', 'Acta de Nacimiento'),
('BEC029', 'DOC1048', 'CURP'),
('BEC030', 'DOC1050', 'Certificado secundaria'),
('BEC040', 'DOC1052', 'Carta de maestros'),
('BEC041', 'DOC1054', 'Constancia de recursos economicos'),
('BEC042', 'DOC1056', 'Carta de padres de familia'),
('BEC043', 'DOC1058', 'INE'),
('BEC044', 'DOC1060', 'CURP Madre o Padre'),
('BEC045', 'DOC1062', 'Boletas de calificaciones actualizada'),
('BEC046', 'DOC1064', 'Constancia de inscripcion en la escuela'),
('BEC047', 'DOC1066', 'Dictamen medico'),
('BEC048', 'DOC1068', 'Constrancia trabajo comunitario'),
('BEC049', 'DOC1070', 'Una fotografia'),
('BEC050', 'DOC1072', 'Copia de credencial de fundacion'),
('BEC060', 'DOC1074', 'Convenio Actualizado'),
('BEC061', 'DOC1076', 'Tarjeta de debito'),
('BEC062', 'DOC1078', 'Carta compromiso servicio comunitario'),
('BEC063', 'DOC1080', 'Evaluacion de cumplimiento de convenio'),
('BEC064', 'DOC1082', 'Acta de Nacimiento'),
('BEC065', 'DOC1084', 'CURP'),
('BEC066', 'DOC1086', 'Certificado secundaria'),
('BEC067', 'DOC1088', 'Carta de maestros'),
('BEC068', 'DOC1090', 'Constancia de recursos economicos'),
('BEC069', 'DOC1092', 'Carta de padres de familia'),
('BEC001', 'DOC1094', 'INE'),
('BEC002', 'DOC1096', 'CURP Madre o Padre'),
('BEC003', 'DOC1098', 'Boletas de calificaciones actualizada'),
('BEC004', 'DOC1100', 'Constancia de inscripcion en la escuela'),
('BEC005', 'DOC1102', 'Dictamen medico'),
('BEC006', 'DOC1104', 'Constrancia trabajo comunitario'),
('BEC007', 'DOC1106', 'Una fotografia'),
('BEC008', 'DOC1108', 'Copia de credencial de fundacion'),
('BEC009', 'DOC1110', 'Convenio Actualizado'),
('BEC010', 'DOC1112', 'Tarjeta de debito'),
('BEC020', 'DOC1114', 'Carta compromiso servicio comunitario'),
('BEC021', 'DOC1116', 'Evaluacion de cumplimiento de convenio'),
('BEC022', 'DOC1118', 'Acta de Nacimiento'),
('BEC023', 'DOC1120', 'CURP'),
('BEC024', 'DOC1122', 'Certificado secundaria'),
('BEC025', 'DOC1124', 'Carta de maestros'),
('BEC026', 'DOC1126', 'Constancia de recursos economicos'),
('BEC027', 'DOC1128', 'Carta de padres de familia'),
('BEC028', 'DOC1130', 'INE'),
('BEC029', 'DOC1132', 'CURP Madre o Padre'),
('BEC030', 'DOC1134', 'Boletas de calificaciones actualizada'),
('BEC040', 'DOC1136', 'Constancia de inscripcion en la escuela'),
('BEC041', 'DOC1138', 'Dictamen medico'),
('BEC042', 'DOC1140', 'Constrancia trabajo comunitario'),
('BEC043', 'DOC1142', 'Una fotografia'),
('BEC044', 'DOC1144', 'Copia de credencial de fundacion'),
('BEC045', 'DOC1146', 'Convenio Actualizado'),
('BEC046', 'DOC1148', 'Tarjeta de debito'),
('BEC047', 'DOC1150', 'Carta compromiso servicio comunitario'),
('BEC048', 'DOC1152', 'Evaluacion de cumplimiento de convenio'),
('BEC049', 'DOC1154', 'Acta de Nacimiento'),
('BEC050', 'DOC1156', 'CURP'),
('BEC060', 'DOC1158', 'Certificado secundaria'),
('BEC061', 'DOC1160', 'Carta de maestros'),
('BEC062', 'DOC1162', 'Constancia de recursos economicos'),
('BEC063', 'DOC1164', 'Carta de padres de familia'),
('BEC064', 'DOC1166', 'INE'),
('BEC065', 'DOC1168', 'CURP Madre o Padre'),
('BEC066', 'DOC1170', 'Boletas de calificaciones actualizada'),
('BEC067', 'DOC1172', 'Constancia de inscripcion en la escuela'),
('BEC068', 'DOC1174', 'Dictamen medico'),
('BEC069', 'DOC1176', 'Constrancia trabajo comunitario'),
('BEC001', 'DOC1178', 'Una fotografia'),
('BEC002', 'DOC1180', 'Copia de credencial de fundacion'),
('BEC003', 'DOC1182', 'Convenio Actualizado'),
('BEC004', 'DOC1184', 'Tarjeta de debito'),
('BEC005', 'DOC1186', 'Carta compromiso servicio comunitario'),
('BEC006', 'DOC1188', 'Evaluacion de cumplimiento de convenio'),
('BEC007', 'DOC1190', 'Acta de Nacimiento'),
('BEC008', 'DOC1192', 'CURP'),
('BEC009', 'DOC1194', 'Certificado secundaria'),
('BEC010', 'DOC1196', 'Carta de maestros'),
('BEC020', 'DOC1198', 'Constancia de recursos economicos'),
('BEC021', 'DOC1200', 'Carta de padres de familia'),
('BEC022', 'DOC1202', 'INE'),
('BEC023', 'DOC1204', 'CURP Madre o Padre'),
('BEC024', 'DOC1206', 'Boletas de calificaciones actualizada'),
('BEC025', 'DOC1208', 'Constancia de inscripcion en la escuela'),
('BEC026', 'DOC1210', 'Dictamen medico'),
('BEC027', 'DOC1212', 'Constrancia trabajo comunitario'),
('BEC028', 'DOC1214', 'Una fotografia'),
('BEC029', 'DOC1216', 'Copia de credencial de fundacion'),
('BEC030', 'DOC1218', 'Convenio Actualizado'),
('BEC040', 'DOC1220', 'Tarjeta de debito'),
('BEC041', 'DOC1222', 'Carta compromiso servicio comunitario'),
('BEC042', 'DOC1224', 'Evaluacion de cumplimiento de convenio'),
('BEC043', 'DOC1226', 'Acta de Nacimiento'),
('BEC044', 'DOC1228', 'CURP'),
('BEC045', 'DOC1230', 'Certificado secundaria'),
('BEC046', 'DOC1232', 'Carta de maestros'),
('BEC047', 'DOC1234', 'Constancia de recursos economicos'),
('BEC048', 'DOC1236', 'Carta de padres de familia'),
('BEC049', 'DOC1238', 'INE'),
('BEC050', 'DOC1240', 'CURP Madre o Padre'),
('BEC060', 'DOC1242', 'Boletas de calificaciones actualizada'),
('BEC061', 'DOC1244', 'Constancia de inscripcion en la escuela'),
('BEC062', 'DOC1246', 'Dictamen medico'),
('BEC063', 'DOC1248', 'Constrancia trabajo comunitario'),
('BEC064', 'DOC1250', 'Una fotografia'),
('BEC065', 'DOC1252', 'Copia de credencial de fundacion'),
('BEC066', 'DOC1254', 'Convenio Actualizado'),
('BEC067', 'DOC1256', 'Tarjeta de debito'),
('BEC068', 'DOC1258', 'Carta compromiso servicio comunitario'),
('BEC069', 'DOC1260', 'Evaluacion de cumplimiento de convenio'),
('BEC001', 'DOC1262', 'Acta de Nacimiento'),
('BEC002', 'DOC1264', 'CURP'),
('BEC003', 'DOC1266', 'Certificado secundaria'),
('BEC004', 'DOC1268', 'Carta de maestros'),
('BEC005', 'DOC1270', 'Constancia de recursos economicos'),
('BEC006', 'DOC1272', 'Carta de padres de familia'),
('BEC007', 'DOC1274', 'INE'),
('BEC008', 'DOC1276', 'CURP Madre o Padre'),
('BEC009', 'DOC1278', 'Boletas de calificaciones actualizada'),
('BEC010', 'DOC1280', 'Constancia de inscripcion en la escuela'),
('BEC020', 'DOC1282', 'Dictamen medico'),
('BEC021', 'DOC1284', 'Constrancia trabajo comunitario'),
('BEC022', 'DOC1286', 'Una fotografia'),
('BEC023', 'DOC1288', 'Copia de credencial de fundacion'),
('BEC024', 'DOC1290', 'Convenio Actualizado'),
('BEC025', 'DOC1292', 'Tarjeta de debito'),
('BEC026', 'DOC1294', 'Carta compromiso servicio comunitario'),
('BEC027', 'DOC1296', 'Evaluacion de cumplimiento de convenio'),
('BEC028', 'DOC1298', 'Acta de Nacimiento'),
('BEC029', 'DOC1300', 'CURP'),
('BEC030', 'DOC1302', 'Certificado secundaria'),
('BEC040', 'DOC1304', 'Carta de maestros'),
('BEC041', 'DOC1306', 'Constancia de recursos economicos'),
('BEC042', 'DOC1308', 'Carta de padres de familia'),
('BEC043', 'DOC1310', 'INE'),
('BEC044', 'DOC1312', 'CURP Madre o Padre'),
('BEC045', 'DOC1314', 'Boletas de calificaciones actualizada'),
('BEC046', 'DOC1316', 'Constancia de inscripcion en la escuela'),
('BEC047', 'DOC1318', 'Dictamen medico'),
('BEC048', 'DOC1320', 'Constrancia trabajo comunitario'),
('BEC049', 'DOC1322', 'Una fotografia'),
('BEC050', 'DOC1324', 'Copia de credencial de fundacion'),
('BEC060', 'DOC1326', 'Convenio Actualizado'),
('BEC061', 'DOC1328', 'Tarjeta de debito'),
('BEC062', 'DOC1330', 'Carta compromiso servicio comunitario'),
('BEC063', 'DOC1332', 'Evaluacion de cumplimiento de convenio'),
('BEC064', 'DOC1334', 'Acta de Nacimiento'),
('BEC065', 'DOC1336', 'CURP'),
('BEC066', 'DOC1338', 'Certificado secundaria'),
('BEC067', 'DOC1340', 'Carta de maestros'),
('BEC068', 'DOC1342', 'Constancia de recursos economicos'),
('BEC069', 'DOC1344', 'Carta de padres de familia'),
('BEC001', 'DOC1346', 'INE'),
('BEC002', 'DOC1348', 'CURP Madre o Padre'),
('BEC003', 'DOC1350', 'Boletas de calificaciones actualizada'),
('BEC004', 'DOC1352', 'Constancia de inscripcion en la escuela'),
('BEC005', 'DOC1354', 'Dictamen medico'),
('BEC006', 'DOC1356', 'Constrancia trabajo comunitario'),
('BEC007', 'DOC1358', 'Una fotografia'),
('BEC008', 'DOC1360', 'Copia de credencial de fundacion'),
('BEC009', 'DOC1362', 'Convenio Actualizado'),
('BEC010', 'DOC1364', 'Tarjeta de debito'),
('BEC020', 'DOC1366', 'Carta compromiso servicio comunitario'),
('BEC021', 'DOC1368', 'Evaluacion de cumplimiento de convenio'),
('BEC022', 'DOC1370', 'Acta de Nacimiento'),
('BEC023', 'DOC1372', 'CURP'),
('BEC024', 'DOC1374', 'Certificado secundaria'),
('BEC025', 'DOC1376', 'Carta de maestros'),
('BEC026', 'DOC1378', 'Constancia de recursos economicos'),
('BEC027', 'DOC1380', 'Carta de padres de familia'),
('BEC028', 'DOC1382', 'INE'),
('BEC029', 'DOC1384', 'CURP Madre o Padre'),
('BEC030', 'DOC1386', 'Boletas de calificaciones actualizada'),
('BEC040', 'DOC1388', 'Constancia de inscripcion en la escuela'),
('BEC041', 'DOC1390', 'Dictamen medico'),
('BEC042', 'DOC1392', 'Constrancia trabajo comunitario'),
('BEC043', 'DOC1394', 'Una fotografia'),
('BEC044', 'DOC1396', 'Copia de credencial de fundacion'),
('BEC045', 'DOC1398', 'Convenio Actualizado'),
('BEC046', 'DOC1400', 'Tarjeta de debito'),
('BEC047', 'DOC1402', 'Carta compromiso servicio comunitario'),
('BEC048', 'DOC1404', 'Evaluacion de cumplimiento de convenio'),
('BEC049', 'DOC1406', 'Acta de Nacimiento'),
('BEC050', 'DOC1408', 'CURP'),
('BEC060', 'DOC1410', 'Certificado secundaria'),
('BEC061', 'DOC1412', 'Carta de maestros'),
('BEC062', 'DOC1414', 'Constancia de recursos economicos'),
('BEC063', 'DOC1416', 'Carta de padres de familia'),
('BEC064', 'DOC1418', 'INE'),
('BEC065', 'DOC1420', 'CURP Madre o Padre'),
('BEC066', 'DOC1422', 'Boletas de calificaciones actualizada'),
('BEC067', 'DOC1424', 'Constancia de inscripcion en la escuela'),
('BEC068', 'DOC1426', 'Dictamen medico'),
('BEC069', 'DOC1428', 'Constrancia trabajo comunitario'),
('BEC001', 'DOC1430', 'Una fotografia'),
('BEC002', 'DOC1432', 'Copia de credencial de fundacion'),
('BEC003', 'DOC1434', 'Convenio Actualizado'),
('BEC004', 'DOC1436', 'Tarjeta de debito'),
('BEC005', 'DOC1438', 'Carta compromiso servicio comunitario'),
('BEC006', 'DOC1440', 'Evaluacion de cumplimiento de convenio'),
('BEC007', 'DOC1442', 'Acta de Nacimiento'),
('BEC008', 'DOC1444', 'CURP'),
('BEC009', 'DOC1446', 'Certificado secundaria'),
('BEC010', 'DOC1448', 'Carta de maestros'),
('BEC020', 'DOC1450', 'Constancia de recursos economicos'),
('BEC021', 'DOC1452', 'Carta de padres de familia'),
('BEC022', 'DOC1454', 'INE'),
('BEC023', 'DOC1456', 'CURP Madre o Padre'),
('BEC024', 'DOC1458', 'Boletas de calificaciones actualizada'),
('BEC025', 'DOC1460', 'Constancia de inscripcion en la escuela'),
('BEC026', 'DOC1462', 'Dictamen medico'),
('BEC027', 'DOC1464', 'Constrancia trabajo comunitario'),
('BEC028', 'DOC1466', 'Una fotografia'),
('BEC029', 'DOC1468', 'Copia de credencial de fundacion'),
('BEC030', 'DOC1470', 'Convenio Actualizado'),
('BEC040', 'DOC1472', 'Tarjeta de debito'),
('BEC041', 'DOC1474', 'Carta compromiso servicio comunitario'),
('BEC042', 'DOC1476', 'Evaluacion de cumplimiento de convenio'),
('BEC043', 'DOC1478', 'Acta de Nacimiento'),
('BEC044', 'DOC1480', 'CURP'),
('BEC045', 'DOC1482', 'Certificado secundaria'),
('BEC046', 'DOC1484', 'Carta de maestros'),
('BEC047', 'DOC1486', 'Constancia de recursos economicos'),
('BEC048', 'DOC1488', 'Carta de padres de familia'),
('BEC049', 'DOC1490', 'INE'),
('BEC050', 'DOC1492', 'CURP Madre o Padre'),
('BEC060', 'DOC1494', 'Boletas de calificaciones actualizada'),
('BEC061', 'DOC1496', 'Constancia de inscripcion en la escuela'),
('BEC062', 'DOC1498', 'Dictamen medico'),
('BEC063', 'DOC1500', 'Constrancia trabajo comunitario'),
('BEC064', 'DOC1502', 'Una fotografia'),
('BEC065', 'DOC1504', 'Copia de credencial de fundacion'),
('BEC066', 'DOC1506', 'Convenio Actualizado'),
('BEC067', 'DOC1508', 'Tarjeta de debito'),
('BEC068', 'DOC1510', 'Carta compromiso servicio comunitario'),
('BEC069', 'DOC1512', 'Evaluacion de cumplimiento de convenio');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `documentacion`
--
ALTER TABLE `documentacion`
  ADD PRIMARY KEY (`IdDoc`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `documentacion`
--
ALTER TABLE `documentacion`
  ADD CONSTRAINT `documentacion_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

CREATE TABLE `enfermedad` (
  `IdBecario` varchar(6) NOT NULL,
  `IdEnfermedad` varchar(6) NOT NULL,
  `HayEnfermedad` text,
  `FamiliaEnfermo` text,
  `Descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `enfermedad`
--

INSERT INTO `enfermedad` (`IdBecario`, `IdEnfermedad`, `HayEnfermedad`, `FamiliaEnfermo`, `Descripcion`) VALUES
('BEC001', 'ENF001', 'NO', '', ''),
('BEC002', 'ENF002', 'NO', '', ''),
('BEC003', 'ENF003', 'NO', '', ''),
('BEC004', 'ENF004', 'NO', '', ''),
('BEC005', 'ENF005', '', '', ''),
('BEC006', 'ENF006', 'NO', '', ''),
('BEC007', 'ENF007', 'NO', '', ''),
('BEC008', 'ENF008', 'NO', '', ''),
('BEC009', 'ENF009', 'NO', '', ''),
('BEC010', 'ENF010', 'SÍ', 'BENEFICIARIA', 'AUDITIVA Y DEL HABLA'),
('BEC020', 'ENF020', 'NO', '', ''),
('BEC021', 'ENF021', 'NO', '', ''),
('BEC022', 'ENF022', 'SÍ', 'ABUELO', 'AUDITIVO'),
('BEC023', 'ENF023', 'SÍ', 'HERMANO MENOR', 'DEFORMACIÓN DEL OÍDO '),
('BEC024', 'ENF024', 'NO', '', ''),
('BEC025', 'ENF025', 'SÍ', 'BENEFICIARIA', 'ASMA'),
('BEC026', 'ENF026', 'NO', '', ''),
('BEC027', 'ENF027', 'SÍ', 'MADRE Y PADRE', 'DIABETES'),
('BEC028', 'ENF028', 'NO', '', ''),
('BEC029', 'ENF029', 'SÍ', 'PADRE', 'POSIBLE GASTRITIS'),
('BEC030', 'ENF030', 'SÍ', 'ABUELA', 'DIABETES'),
('BEC040', 'ENF040', 'NO', '', ''),
('BEC041', 'ENF041', 'NO', '', ''),
('BEC042', 'ENF042', 'NO', '', ''),
('BEC043', 'ENF043', 'SÍ', 'PADRE', 'DOLOR MUSCULAR'),
('BEC044', 'ENF044', 'SÍ', 'MADRE Y BENEFICIARIO', 'ASMA'),
('BEC045', 'ENF045', 'NO', '', ''),
('BEC046', 'ENF046', 'NO', '', ''),
('BEC047', 'ENF047', '', '', ''),
('BEC048', 'ENF048', 'SÍ', 'BENEFICIARIO', 'VITILIGIO'),
('BEC049', 'ENF049', 'NO', '', ''),
('BEC050', 'ENF050', 'SÍ', 'ABUELA', 'DIABETES'),
('BEC060', 'ENF060', 'NO', '', ''),
('BEC061', 'ENF061', 'SÍ', 'TÍO', 'DISCAPACIDAD INTELECTUAL'),
('BEC062', 'ENF062', 'SÍ', 'PAPÁ', 'DIABETES'),
('BEC063', 'ENF063', 'NO', '', ''),
('BEC064', 'ENF064', 'NO', '', ''),
('BEC065', 'ENF065', 'NO', '', ''),
('BEC066', 'ENF066', 'NO', '', ''),
('BEC067', 'ENF067', 'NO', '', ''),
('BEC068', 'ENF068', 'NO', '', ''),
('BEC069', 'ENF069', 'SÍ', 'PAPÁ', 'FRACTURA DE MANO, NO PUEDE LEVANTAR COSAS PESADAS');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `enfermedad`
--
ALTER TABLE `enfermedad`
  ADD PRIMARY KEY (`IdEnfermedad`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `enfermedad`
--
ALTER TABLE `enfermedad`
  ADD CONSTRAINT `enfermedad_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

CREATE TABLE `familia` (
  `IdBecario` varchar(6) NOT NULL,
  `IdFamilia` varchar(6) NOT NULL,
  `IngresoMin` int(11) DEFAULT NULL,
  `IngresoMax` int(11) DEFAULT NULL,
  `Mama` int(11) DEFAULT NULL,
  `Papa` int(11) DEFAULT NULL,
  `Hermanos` int(11) DEFAULT NULL,
  `Hijos` int(11) DEFAULT NULL,
  `Abuela` int(11) DEFAULT NULL,
  `Abuelo` int(11) DEFAULT NULL,
  `Otros` int(11) DEFAULT NULL,
  `Descripcion` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `familia`
--

INSERT INTO `familia` (`IdBecario`, `IdFamilia`, `IngresoMin`, `IngresoMax`, `Mama`, `Papa`, `Hermanos`, `Hijos`, `Abuela`, `Abuelo`, `Otros`, `Descripcion`) VALUES
('BEC001', 'FAM001', 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'Originaria de El Saucito, Tolimán, su familia es de bajos recursos económicos, su papá está lastimado de una pierna y por lo mismo no puede trabajar regularmente, ella necesita la beca para seguir estudiando y en un futuro entrar al bachillerato.'),
('BEC002', 'FAM002', 1, 1500, 1, 1, 4, 0, 0, 0, 0, 'Ha sido difícil mantenerse en la escuela ya que su papá no tiene un trabajo fijo y no puede darle el apoyo económico que requiere para su escuela. Su casa está construida de block y lámina únicamente y su familia es numerosa. '),
('BEC003', 'FAM003', 1, 1500, 1, 1, 1, 0, 0, 0, 0, 'Nancy es una chica de 16 años que es apasionada por la lectura, le gusta mucho la ciencia ficción y demuestra mucho interés por continuar estudiando, lleva un buen promedio en la escuela, 9.2, ella utilizaría la beca para cubrir sus gastos escolares, por ejemplo el transporte ya que menciona que hace 55 minutos de su casa a la escuela.'),
('BEC004', 'FAM004', 1, 1500, 1, 1, 2, 0, 1, 1, 0, 'Vive en la comunidad de Maguey Manso, Tolimán, su papá murió cuando tenía 4 años de edad, desde entonces su mamá ha tenido que cubrir los gastos de la casa, Daniel tuvo que abandonar sus estudios para venir a trabajar a la ciudad de Querétaro, actualmente los retomó y está cursando el 3er semestre de bachillerato, le gustaría estudiar Ingeniería Civil ya que le apasiona el tema de la construcción.'),
('BEC005', 'FAM005', 1, 1500, 0, 0, 0, 0, 0, 0, 0, 'José Raúl es de la comunidad Mesa de Chagoya, Tolimán, durante su tiempo libre trabaja en las tierras de la familia para apoyar a su papá, el único proveedor es su papá y gana muy poco ya que es jornalero, camina diariamente una hora para poder llegar a su escuela, ocuparía la beca para los gastos de la familia y para sus estudios.'),
('BEC006', 'FAM006', 3000, 4500, 1, 1, 8, 0, 0, 0, 0, 'Bibiana es la hija mayor y  tiene 8 hermanos, tiene 17 años y está estudiando el último año del bachillerato, tiene un promedio de 9, le gustaría estudiar medicina, por lo mismo que son 11 integrantes de la familia lo que tienen no les alcanza, su papá trabaja de jornalero y depende si hay siembra para trabajar, su mamá sale a trabajar a hacer limpieza y vende productos de catálogo. '),
('BEC007', 'FAM007', 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'En su tiempo libre trabaja de jornalero ya que no alcanza el gasto en su familia, su mamá hace limpieza en casas y su papá no puede trabajar mucho ya que está impedido, a Víctor le gusta mucho estudiar, su materia favorita son las matemáticas y en un futuro le gustaría ser ingeniero.'),
('BEC008', 'FAM008', 3000, 4500, 1, 1, 3, 0, 0, 0, 0, 'Su materias preferida es historia porque le gusta aprender sobre su cultura y su lengua, a ella le gustaría estudiar Derecho porque quiere ayudar a su comunidad pero continuar estudiando ha sido difícil ya que no cuenta con los recursos necesarios para cubrir sus gastos escolares, incluso tiene que caminar una hora a la escuela pues no puede pagar el transporte que la acerca a la escuela. '),
('BEC009', 'FAM009', 1, 1500, 1, 0, 1, 0, 0, 0, 0, 'Vive en condiciones adversas puesto su padre las abandonó desde hace tiempo y ahora tiene que apoyar a su mamá y a su hermana para que las 3 puedan salir adelante.'),
('BEC010', 'FAM010', 4500, 20000, 0, 0, 0, 0, 0, 0, 0, 'Vive en Mesa de Ramírez, Tolimán, ella tiene una condición de discapacidad que le impide escuchar y hablar bien, su mamá lee poco y no sabe escribir muy bien, su papá trabaja en la ciudad de Querétaro pero apenas les alcanza para solventar sus gastos más básicos.'),
('BEC020', 'FAM020', 3000, 4500, 1, 1, 3, 0, 0, 0, 0, 'Melissa es originaria de una comunidad indígena de Tolimán, habla el otomí igual que su familia, le gusta mucho ir a la escuela, actualmente cursa el 5to semestre del bachillerato, le gusta aprender idiomas, sus papás le apoyan a continuar sus estudios, ella quiere estudiar la universidad.'),
('BEC021', 'FAM021', 1, 1500, 1, 1, 1, 0, 0, 0, 0, 'Sofía siempre ha sido una chica muy estudiosa y con buenas calificaciones, sus padres están orgullosos de ella y sus maestros manifiestan que tiene gran potencial. Sin embargo, no cuenta con los recursos suficientes para continuar sus estudios, ni recibe el apoyo económico de nadie para solventar los gastos que ello conlleva, por lo que trabaja de empleada doméstica para pagar sus estudios. '),
('BEC022', 'FAM022', 3000, 4500, 1, 1, 5, 0, 0, 1, 1, 'En su casa viven 10 personas: sus papás, sus hermanos, una tía, su abuelo y ella. Sus papás la animan a seguir estudiando aunque no tienen los recursos suficientes para apoyarla económicamente. De hecho, Daniela es una inspiración para su familia, ya que sus ganas de continuar la escuela y el esfuerzo que realiza para mantener sus buenas calificaciones motivan a sus papás y a sus hermanos para salir adelante y mejorar en lo que hacen día con día.'),
('BEC023', 'FAM023', 3000, 4500, 1, 1, 2, 0, 0, 0, 1, 'Le echa muchas ganas a la escuela y menciona que le gustaría estudiar Ingeniería electrónica, proviene de una familia indígena, su hermano pequeño nació con una deformación en el oído lo que ha orillado a su familia a tener que hacer gastos médicos importantes, su tía que es una persona de 3era edad vive con ellos en casa y su papá es el único proveedor. Está embarazada y tiene el apoyo de su familia para seguir estudiando'),
('BEC024', 'FAM024', 1, 1500, 1, 1, 3, 1, 0, 0, 1, 'Vive en la comunidad de El Saucito, en Tolimán junto con sus papás y sus 3 hermanos. Ella es madre soltera y estudia el 3er semestre de bachillerato y tiene 16 años, le gustan las matemáticas y le encantaría estudiar Derecho si pudiera estudiar la universidad, pero en ocasiones sus papás no pueden pagar los gastos de ella y sus hermanos.'),
('BEC025', 'FAM025', 1, 1500, 1, 1, 4, 0, 1, 1, 0, 'Para llegar a su escuela tiene que caminar una hora de ida y otra de regreso ya que no le alcanza para el transporte, ella padece asma la cual está controlada por el médico.'),
('BEC026', 'FAM026', 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'Es originario de la comunidad Mesa de Chagoya, Tolimán, tiene 16 años y estudia en el Telebachillerato comunitario de Mesa de Ramírez, para llegar a su escuela tiene que caminar media hora, habla Otomí, su casa es muy precaria, está hecha de piedra y láminas, refiere que no les alcanza para los gastos básicos, Luis Alberto es muy tímido, él se compromete a poner su mayor esfuerzo para continuar estudiando.'),
('BEC027', 'FAM027', 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'Vive en la comunidad de Sabino de San Ambrosio, en Tolimán, tiene 17 años y le falta un año para concluir el bachillerato. A ella le gusta mucho la escuela pero no cuenta con los recursos suficientes para costear los gastos escolares ya que su papá y su mamá se encuentran enfermos y no pueden trabajar, por lo que ella trabaja en vacaciones para juntar los recursos necesarios para pagar su escuela. A Aurelia le gustaría estudiar Medicina o Enfermería para ayudar a sus padres, tanto en los cuidados que requieren como económicamente.'),
('BEC028', 'FAM028', 1, 1500, 0, 0, 0, 0, 0, 0, 0, 'Ella camina a su escuela diariamente, en total son 8 personas las que viven en una casa con dos cuartos y una cocina, es un poco tímida, tiene muchas ganas de seguir estudiando como su hermana que está en el bachillerato.'),
('BEC029', 'FAM029', 1501, 3000, 1, 1, 5, 0, 0, 0, 0, 'Vive en la comunidad de Mesa de Ramírez, en Tolimán y le gustaría estudiar algún día Ingeniería en Sistemas pero sus padres son de escasos recursos y les cuesta trabajo apoyar a ella y sus 5 hermanos en sus estudios. Además, su papá padece de una enfermedad delicada que le impide trabajar continuamente y es el único sustento de la familia. Sin embargo Brenda se encuentra muy comprometida con su comunidad y realiza actividades de recolección de pet, aseo de calles y ayuda a personas a que aprendan a leer y a escribir en el INEA. '),
('BEC030', 'FAM030', 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'A ella le gustan las ciencias de la salud y le encantaría estudiar más adelante la Licenciatura en Criminología. Sin embargo, no cuenta con los recursos necesarios puesto que su papá se encuentra desempleado y es difícil para su familia solventar los gastos escolares de ella y de sus 2 hermanos. '),
('BEC040', 'FAM040', 1, 1500, 1, 1, 1, 0, 0, 0, 0, 'Ella es originaria de la comunidad Mesa de Chagoya, tiene 21 años, habla otomí, estudia la Ingeniería en Gestión Empresarial en el ITQ, va muy bien en la escuela, su familia es de escasos recursos económicos por lo que no les alcanza para su transporte y sus comidas, su casa está en obra negra, menciona que cuando tiene que ir a la escuela a veces se queda con hambre y se espera a llegar a su casa por la tarde para poder comer lo que haya.'),
('BEC041', 'FAM041', 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'Quiere ayudar a su comunidad y defender sus derechos. Entre semana trabaja como empleada doméstica en Tolimán para cubrir sus gastos escolares pues sus padres no pueden apoyarla económicamente porque también apoyan a sus hermanitos.'),
('BEC042', 'FAM042', 1501, 3000, 1, 0, 2, 0, 0, 0, 0, 'Es de la comunidad de San Pedro de los Eucaliptos, en Tolimán y vive con su madre y sus dos hermanos únicamente puesto que su padre falleció. Su hermano mayor tuvo que abandonar la escuela para apoyar a su familia y junto su mamá ha sacado a su familia adelante. Él quiere estudiar para mejorar sus condiciones y dar una mejor vida a su familia. '),
('BEC043', 'FAM043', 4500, 20000, 0, 0, 0, 0, 0, 0, 0, 'Estudia y trabaja, para ir al bachillerato toma un camión y hace 30 minutos de traslado, a veces se va caminando y hace una hora, su papá de Dianeli tuvo un accidente y eso le impide trabajar, era jornalero, ahora la han pasado muy difícil pues el dinero que llega a la casa no alcanza, su hermano es quien trabaja, y ella también lo ha tenido que hacer.'),
('BEC044', 'FAM044', 1501, 3000, 1, 1, 5, 0, 0, 0, 0, 'La escuela lo ha motivado mucho a superarse y mejorar sus condiciones de vida, ya que su mamá y él padecen de asma, su papá es albañil y al tener 5 hermanos más, sus recursos son muy escasos y el mismo tiene que solventar sus gastos escolares trabajando en el campo los fines de semana. '),
('BEC045', 'FAM045', 1, 1500, 1, 1, 3, 0, 1, 0, 0, 'A Luis le interesa seguir estudiando y mejorar su promedio, pero debido a que su familia es numerosa y sus padres no cuentan con recursos suficientes en vacaciones él va a trabajar como jornalero para juntar el dinero que necesita y cubrir sus gastos de inscripciones y útiles escolares, y que así sus papás puedan apoyar a sus otros hermanos. '),
('BEC046', 'FAM046', 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'Estudia el 1° semestre de la Ingeniería en Gestión Empresarial en el ITQ de Tolimán, y durante los fines de semana y vacaciones él sale a trabajar como jornalero o chalan de albañil, va muy bien en la escuela.'),
('BEC047', 'FAM047', 1, 1500, 0, 0, 0, 0, 0, 0, 0, 'Su materia favorita es biología y en un futuro le gustaría estudiar gastronomía, ella no tiene el apoyo de su papá para seguir estudiando, únicamente es su mamá quien la respalda, sin embargo tiene muchas ganas de estudiar y salir adelante, ella trabaja como jornalera, su casa tiene 2 cuartos, para llegar a su escuela tiene que caminar una hora. Necesita mucho el apoyo.'),
('BEC048', 'FAM048', 1501, 3000, 1, 1, 3, 0, 0, 0, 0, 'Marcos estudia Ingeniería en Sistemas Computacionales, a él le gusta el tema de la programación, se ha dedicado a buscar apoyos ya que en un futuro le gustaría establecer un negocio propio, actualmente ha tenido muchas dificultades para continuar estudiando ya que su familia es de escasos recursos económicos, su papá es jornalero y gana muy poco, Marcos tiene un problema en la piel (vitíligo) lo que le impide trabajar en los empleos que normalmente ofertan ya que son bajo el sol y esto le afecta en su salud, aun así ha trabajado en el mantenimiento de las carreteras a causa de la necesidad.'),
('BEC049', 'FAM049', 3000, 4500, 0, 1, 1, 0, 0, 0, 3, 'En su familia, la encargada de hacer la comida es su hermana, su madre falleció. Beatriz se encarga de la limpieza del hogar, su padre y su cuñado aportan al aseo de la casa también. Tiene dos sobrinos que los siente como hermanos, ellos son pequeños y no aportan mucho al aseo. Ella considera que su cuñado es como su segundo padre pues la apoya para seguir estudiando. Ella es tía de Elizabeth Blas, FAMeficiaria del programa, y por eso se enteró de la beca.'),
('BEC050', 'FAM050', 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'Su hermana menor falleció de un problema del corazón. Actualmente él le ayuda a su mamá a las labores del hogar cuando está en la escuela y cuando no, él trabaja de chalán de albañil o en el campo.'),
('BEC060', 'FAM060', 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'En su familia se llevan muy bien, hay respeto y convivencia. Sus materias favoritas son la lectura y redacción y la educación física, le gustaría estudiar pediatría o ginecología. Comparten casa con su abuela paterna; ellos viven en dos cuartos'),
('BEC061', 'FAM061', 1501, 3000, 1, 1, 1, 0, 1, 0, 1, 'Le interesa la repostería. Su tío tiene discapacidad intelectual y vive con la familia de ella. Le gustan las materias de español, química y formación cívica y ética.'),
('BEC062', 'FAM062', 1, 1500, 0, 0, 0, 0, 1, 1, 2, 'Ella se refiere a sus abuelos como sus papás, su madre la abandonó y vive en otra comunidad, no sabe quién es su padre. Su abuelo es el ínico que aporta al gasto de la familia. Ella quiere estudiar corte y confección porque le gusta la idea de crear y hacer cosas nuevas, combinar colores y texturas.'),
('BEC063', 'FAM063', 3000, 4500, 0, 0, 0, 0, 0, 0, 0, 'Dejó de estudiar 2 añospara ayudar económicamente a su familia pero tuvo un accidente laboral, se fracturó la férula, así que hoy su familia se une para que él pueda retomar sus estudios. En verdad le gusta mucho estudiar y está muy emocionado por volver a hacerlo. Quiere hacer un proyecto en los hospitales en el que pueda motivas a las personas que se encuentran ahí a causa de algún accidente.'),
('BEC064', 'FAM064', 1501, 3000, 0, 0, 0, 0, 0, 0, 0, 'A él le apasiona todo lo relacionado con los aparatos electrónicos y la automatización. Su papá y su hermano son los únicos que aportan económicamente a su hogar, tiene mucha relción con su hermano y no hace referencia a otros familiares. Él vivirpa en Querétaro para hacer su carrera. Por ahora tiene un empleo que se relaciona un poco con su carrera.'),
('BEC065', 'FAM065', 1, 1500, 1, 0, 4, 0, 0, 0, 0, 'Quienes trabajan son sus hermanos mayores. Le gusta mucho la materia de matemáticas y química y le gustaría ser ingeniera porque le llama la atención poder construir cosas.'),
('BEC066', 'FAM066', 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'Efraín dejó sus estudios en el 2014 por falta de recursos económicos y durante ese tiempo trabajó en la obra a apoyó a su familia. Su hermana ayor apoya a las labores del hogar y su hermano mayor trabaja auque no aporta diner, más bien él se cubre sus propios gastos. Le gustaría estudiar ingeniería civil.'),
('BEC067', 'FAM067', 1, 1500, 1, 1, 1, 0, 0, 0, 0, 'Le gustan las materias de españo, química e historia, quiere ser doctora para ayudar a las personas porque en el centro de salud no hay dotores con frecuencia .'),
('BEC068', 'FAM068', 1501, 3000, 0, 0, 0, 0, 0, 0, 0, 'Quiere estudiar ingeniería mecatrónica, está muy interesado en el funcionamiento de las máquinas ya que ha escuchado que algunos mexicanos han inventado han inventado varios robots que han hecho un gran avance en México, él quiere hacer un gran invento un día. Solamente aporta dinero su padre, y Jesús en vacaciones, es un chico motivado y trabajador con una familia cariñosa y que le brinda mucho apoyo.'),
('BEC069', 'FAM069', 1, 1500, 1, 1, 3, 0, 0, 0, 1, 'Su padre le ayuda con los alimentos y los útiles, su hermano aporta muy poco y sus hermanas viven aparte. Quiere ser abogada o estudiar ingeniería en sistemas. En su familia tienen problemas de despojos de terrenos. Su hermano mayor tiene problemas de alcoholismo. Elvia es creativa y soñadora.');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `familia`
--
ALTER TABLE `familia`
  ADD PRIMARY KEY (`IdFamilia`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `familia`
--
ALTER TABLE `familia`
  ADD CONSTRAINT `familia_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

CREATE TABLE `tienebeneficio` (
  `IdBecario` varchar(6) NOT NULL,
  `IdBeneficio` varchar(6) NOT NULL,
  `Monto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tienebeneficio`
--

INSERT INTO `tienebeneficio` (`IdBecario`, `IdBeneficio`, `Monto`) VALUES
('BEC001', 'BEN001', 1795),
('BEC002', 'BEN002', 1485),
('BEC003', 'BEN003', 1450),
('BEC004', 'BEN004', 1500),
('BEC005', 'BEN005', 0),
('BEC006', 'BEN006', 2095),
('BEC007', 'BEN007', 1485),
('BEC008', 'BEN008', 1750),
('BEC009', 'BEN009', 1250),
('BEC010', 'BEN010', 0),
('BEC020', 'BEN020', 700),
('BEC021', 'BEN021', 650),
('BEC022', 'BEN022', 2270),
('BEC023', 'BEN023', 300),
('BEC024', 'BEN024', 1950),
('BEC025', 'BEN025', 2337),
('BEC026', 'BEN026', 1447),
('BEC027', 'BEN027', 860),
('BEC028', 'BEN028', 1400),
('BEC029', 'BEN029', 1100),
('BEC030', 'BEN030', 800),
('BEC040', 'BEN040', 0),
('BEC041', 'BEN041', 1540),
('BEC042', 'BEN042', 2120),
('BEC043', 'BEN043', 1235),
('BEC044', 'BEN044', 2262),
('BEC045', 'BEN045', 1120),
('BEC046', 'BEN046', 0),
('BEC047', 'BEN047', 1002),
('BEC048', 'BEN048', 475),
('BEC049', 'BEN049', 950),
('BEC050', 'BEN050', 1500),
('BEC060', 'BEN060', 1800),
('BEC061', 'BEN061', 0),
('BEC062', 'BEN062', 2200),
('BEC063', 'BEN063', 0),
('BEC064', 'BEN064', 1200),
('BEC065', 'BEN065', 0),
('BEC066', 'BEN066', 2300),
('BEC067', 'BEN067', 1850),
('BEC068', 'BEN068', 3200),
('BEC069', 'BEN069', 1800);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tienebeneficio`
--
ALTER TABLE `tienebeneficio`
  ADD KEY `IdBecario` (`IdBecario`),
  ADD KEY `IdBeneficio` (`IdBeneficio`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tienebeneficio`
--
ALTER TABLE `tienebeneficio`
  ADD CONSTRAINT `tienebeneficio_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tienebeneficio_ibfk_2` FOREIGN KEY (`IdBeneficio`) REFERENCES `beneficio` (`IdBeneficio`) ON UPDATE CASCADE;
COMMIT;

CREATE TABLE `tienedocumentacion` (
  `IdBecario` varchar(6) NOT NULL,
  `IdDoc` varchar(7) NOT NULL,
  `ImdDoc` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tienedocumentacion`
--

INSERT INTO `tienedocumentacion` (`IdBecario`, `IdDoc`, `ImdDoc`) VALUES
('BEC001', 'DOC0002', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC0004', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC0006', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC0008', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC0010', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC0012', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC0014', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC0016', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC0018', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC0020', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC0022', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC0024', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC0026', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC0028', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC0030', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC0032', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC0034', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC0036', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC0038', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC0040', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC0042', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC0044', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC0046', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC0048', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC0050', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC0052', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC0054', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC0056', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC0058', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC0060', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC0062', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC0064', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC0066', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC0068', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC0070', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC0072', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC0074', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC0076', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC0078', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC0080', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC0082', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC0084', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC0086', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC0088', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC0090', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC0092', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC0094', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC0096', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC0098', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC0100', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC0102', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC0104', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC0106', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC0108', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC0110', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC0112', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC0114', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC0116', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC0118', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC0120', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC0122', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC0124', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC0126', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC0128', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC0130', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC0132', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC0134', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC0136', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC0138', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC0140', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC0142', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC0144', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC0146', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC0148', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC0150', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC0152', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC0154', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC0156', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC0158', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC0160', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC0162', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC0164', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC0166', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC0168', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC0170', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC0172', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC0174', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC0176', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC0178', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC0180', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC0182', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC0184', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC0186', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC0188', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC0190', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC0192', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC0194', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC0196', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC0198', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC0200', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC0202', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC0204', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC0206', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC0208', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC0210', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC0212', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC0214', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC0216', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC0218', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC0220', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC0222', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC0224', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC0226', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC0228', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC0230', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC0232', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC0234', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC0236', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC0238', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC0240', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC0242', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC0244', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC0246', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC0248', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC0250', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC0252', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC0254', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC0256', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC0258', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC0260', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC0262', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC0264', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC0266', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC0268', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC0270', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC0272', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC0274', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC0276', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC0278', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC0280', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC0282', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC0284', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC0286', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC0288', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC0290', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC0292', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC0294', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC0296', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC0298', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC0300', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC0302', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC0304', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC0306', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC0308', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC0310', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC0312', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC0314', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC0316', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC0318', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC0320', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC0322', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC0324', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC0326', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC0328', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC0330', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC0332', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC0334', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC0336', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC0338', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC0340', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC0342', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC0344', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC0346', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC0348', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC0350', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC0352', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC0354', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC0356', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC0358', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC0360', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC0362', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC0364', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC0366', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC0368', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC0370', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC0372', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC0374', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC0376', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC0378', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC0380', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC0382', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC0384', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC0386', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC0388', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC0390', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC0392', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC0394', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC0396', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC0398', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC0400', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC0402', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC0404', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC0406', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC0408', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC0410', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC0412', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC0414', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC0416', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC0418', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC0420', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC0422', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC0424', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC0426', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC0428', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC0430', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC0432', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC0434', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC0436', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC0438', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC0440', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC0442', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC0444', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC0446', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC0448', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC0450', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC0452', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC0454', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC0456', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC0458', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC0460', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC0462', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC0464', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC0466', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC0468', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC0470', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC0472', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC0474', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC0476', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC0478', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC0480', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC0482', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC0484', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC0486', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC0488', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC0490', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC0492', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC0494', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC0496', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC0498', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC0500', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC0502', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC0504', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC0506', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC0508', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC0510', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC0512', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC0514', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC0516', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC0518', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC0520', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC0522', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC0524', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC0526', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC0528', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC0530', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC0532', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC0534', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC0536', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC0538', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC0540', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC0542', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC0544', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC0546', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC0548', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC0550', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC0552', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC0554', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC0556', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC0558', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC0560', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC0562', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC0564', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC0566', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC0568', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC0570', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC0572', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC0574', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC0576', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC0578', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC0580', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC0582', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC0584', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC0586', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC0588', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC0590', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC0592', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC0594', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC0596', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC0598', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC0600', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC0602', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC0604', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC0606', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC0608', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC0610', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC0612', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC0614', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC0616', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC0618', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC0620', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC0622', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC0624', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC0626', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC0628', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC0630', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC0632', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC0634', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC0636', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC0638', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC0640', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC0642', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC0644', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC0646', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC0648', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC0650', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC0652', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC0654', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC0656', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC0658', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC0660', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC0662', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC0664', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC0666', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC0668', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC0670', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC0672', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC0674', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC0676', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC0678', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC0680', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC0682', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC0684', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC0686', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC0688', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC0690', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC0692', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC0694', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC0696', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC0698', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC0700', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC0702', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC0704', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC0706', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC0708', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC0710', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC0712', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC0714', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC0716', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC0718', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC0720', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC0722', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC0724', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC0726', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC0728', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC0730', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC0732', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC0734', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC0736', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC0738', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC0740', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC0742', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC0744', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC0746', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC0748', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC0750', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC0752', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC0754', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC0756', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC0758', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC0760', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC0762', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC0764', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC0766', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC0768', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC0770', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC0772', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC0774', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC0776', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC0778', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC0780', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC0782', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC0784', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC0786', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC0788', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC0790', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC0792', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC0794', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC0796', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC0798', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC0800', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC0802', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC0804', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC0806', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC0808', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC0810', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC0812', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC0814', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC0816', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC0818', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC0820', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC0822', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC0824', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC0826', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC0828', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC0830', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC0832', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC0834', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC0836', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC0838', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC0840', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC0842', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC0844', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC0846', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC0848', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC0850', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC0852', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC0854', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC0856', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC0858', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC0860', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC0862', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC0864', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC0866', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC0868', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC0870', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC0872', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC0874', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC0876', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC0878', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC0880', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC0882', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC0884', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC0886', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC0888', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC0890', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC0892', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC0894', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC0896', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC0898', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC0900', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC0902', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC0904', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC0906', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC0908', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC0910', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC0912', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC0914', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC0916', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC0918', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC0920', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC0922', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC0924', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC0926', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC0928', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC0930', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC0932', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC0934', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC0936', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC0938', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC0940', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC0942', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC0944', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC0946', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC0948', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC0950', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC0952', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC0954', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC0956', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC0958', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC0960', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC0962', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC0964', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC0966', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC0968', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC0970', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC0972', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC0974', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC0976', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC0978', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC0980', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC0982', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC0984', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC0986', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC0988', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC0990', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC0992', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC0994', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC0996', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC0998', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC1000', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC1002', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC1004', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC1006', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC1008', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC1010', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC1012', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC1014', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC1016', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC1018', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC1020', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC1022', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC1024', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC1026', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC1028', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC1030', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC1032', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC1034', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC1036', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC1038', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC1040', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC1042', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC1044', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC1046', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC1048', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC1050', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC1052', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC1054', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC1056', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC1058', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC1060', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC1062', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC1064', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC1066', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC1068', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC1070', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC1072', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC1074', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC1076', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC1078', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC1080', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC1082', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC1084', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC1086', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC1088', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC1090', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC1092', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC1094', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC1096', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC1098', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC1100', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC1102', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC1104', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC1106', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC1108', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC1110', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC1112', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC1114', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC1116', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC1118', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC1120', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC1122', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC1124', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC1126', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC1128', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC1130', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC1132', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC1134', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC1136', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC1138', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC1140', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC1142', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC1144', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC1146', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC1148', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC1150', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC1152', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC1154', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC1156', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC1158', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC1160', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC1162', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC1164', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC1166', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC1168', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC1170', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC1172', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC1174', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC1176', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC1178', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC1180', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC1182', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC1184', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC1186', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC1188', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC1190', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC1192', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC1194', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC1196', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC1198', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC1200', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC1202', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC1204', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC1206', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC1208', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC1210', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC1212', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC1214', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC1216', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC1218', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC1220', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC1222', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC1224', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC1226', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC1228', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC1230', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC1232', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC1234', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC1236', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC1238', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC1240', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC1242', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC1244', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC1246', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC1248', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC1250', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC1252', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC1254', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC1256', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC1258', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC1260', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC1262', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC1264', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC1266', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC1268', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC1270', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC1272', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC1274', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC1276', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC1278', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC1280', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC1282', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC1284', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC1286', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC1288', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC1290', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC1292', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC1294', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC1296', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC1298', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC1300', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC1302', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC1304', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC1306', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC1308', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC1310', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC1312', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC1314', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC1316', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC1318', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC1320', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC1322', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC1324', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC1326', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC1328', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC1330', 'C:UsersMigDesktopDocumentos de verticeTablas datos');
INSERT INTO `tienedocumentacion` (`IdBecario`, `IdDoc`, `ImdDoc`) VALUES
('BEC063', 'DOC1332', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC1334', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC1336', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC1338', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC1340', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC1342', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC1344', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC1346', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC1348', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC1350', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC1352', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC1354', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC1356', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC1358', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC1360', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC1362', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC1364', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC1366', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC1368', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC1370', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC1372', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC1374', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC1376', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC1378', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC1380', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC1382', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC1384', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC1386', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC1388', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC1390', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC1392', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC1394', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC1396', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC1398', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC1400', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC1402', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC1404', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC1406', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC1408', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC1410', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC1412', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC1414', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC1416', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC1418', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC1420', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC1422', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC1424', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC1426', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC1428', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC001', 'DOC1430', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC002', 'DOC1432', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC003', 'DOC1434', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC004', 'DOC1436', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC005', 'DOC1438', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC006', 'DOC1440', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC007', 'DOC1442', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC008', 'DOC1444', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC009', 'DOC1446', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC010', 'DOC1448', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC020', 'DOC1450', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC021', 'DOC1452', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC022', 'DOC1454', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC023', 'DOC1456', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC024', 'DOC1458', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC025', 'DOC1460', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC026', 'DOC1462', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC027', 'DOC1464', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC028', 'DOC1466', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC029', 'DOC1468', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC030', 'DOC1470', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC040', 'DOC1472', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC041', 'DOC1474', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC042', 'DOC1476', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC043', 'DOC1478', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC044', 'DOC1480', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC045', 'DOC1482', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC046', 'DOC1484', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC047', 'DOC1486', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC048', 'DOC1488', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC049', 'DOC1490', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC050', 'DOC1492', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC060', 'DOC1494', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC061', 'DOC1496', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC062', 'DOC1498', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC063', 'DOC1500', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC064', 'DOC1502', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC065', 'DOC1504', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC066', 'DOC1506', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC067', 'DOC1508', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC068', 'DOC1510', 'C:UsersMigDesktopDocumentos de verticeTablas datos'),
('BEC069', 'DOC1512', 'C:UsersMigDesktopDocumentos de verticeTablas datos');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tienedocumentacion`
--
ALTER TABLE `tienedocumentacion`
  ADD KEY `IdBecario` (`IdBecario`),
  ADD KEY `IdDoc` (`IdDoc`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tienedocumentacion`
--
ALTER TABLE `tienedocumentacion`
  ADD CONSTRAINT `tienedocumentacion_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tienedocumentacion_ibfk_2` FOREIGN KEY (`IdDoc`) REFERENCES `documentacion` (`IdDoc`) ON UPDATE CASCADE;
COMMIT;


CREATE TABLE `trabajo` (
  `IdBecario` varchar(6) NOT NULL,
  `IdTrabajo` varchar(6) NOT NULL,
  `NombreTrabajo` text,
  `Dias` text,
  `Periodo` text,
  `Horario` varchar(20) DEFAULT NULL,
  `TiempoLibre` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `trabajo`
--

INSERT INTO `trabajo` (`IdBecario`, `IdTrabajo`, `NombreTrabajo`, `Dias`, `Periodo`, `Horario`, `TiempoLibre`) VALUES
('BEC001', 'TRA001', '', '', '', '', ''),
('BEC002', 'TRA002', '', '', '', '', ''),
('BEC003', 'TRA003', '', '', '', '', ''),
('BEC004', 'TRA004', 'AYUDANTE ALBAÑIL', '', '', '', ''),
('BEC005', 'TRA005', 'NO ESPECIFICA', '', '', '', ''),
('BEC006', 'TRA006', '', '', '', '', ''),
('BEC007', 'TRA007', 'JORNALERO', '', '', '', ''),
('BEC008', 'TRA008', '', '', '', '', ''),
('BEC009', 'TRA009', '', '', '', '', ''),
('BEC010', 'TRA010', '', '', '', '', ''),
('BEC020', 'TRA020', '', '', '', '', ''),
('BEC021', 'TRA021', 'LIMPIEZA', '', '', '', ''),
('BEC022', 'TRA022', '', '', '', '', ''),
('BEC023', 'TRA023', '', '', '', '', ''),
('BEC024', 'TRA024', '', '', '', '', ''),
('BEC025', 'TRA025', '', '', '', '', ''),
('BEC026', 'TRA026', '', '', '', '', ''),
('BEC027', 'TRA027', 'TIENDA DE NOVEDADES', '', '', '', ''),
('BEC028', 'TRA028', '', '', '', '', ''),
('BEC029', 'TRA029', 'JORNALERA', '', '', '', ''),
('BEC030', 'TRA030', '', '', '', '', ''),
('BEC040', 'TRA040', '', '', '', '', ''),
('BEC041', 'TRA041', 'LIMPIEZA', '', '', '', ''),
('BEC042', 'TRA042', '', '', '', '', ''),
('BEC043', 'TRA043', 'LIMPIEZA', '', '', '', ''),
('BEC044', 'TRA044', 'CHALAN ALBAÑIL', '', '', '', ''),
('BEC045', 'TRA045', 'JORNALERO', '', '', '', ''),
('BEC046', 'TRA046', '', '', '', '', ''),
('BEC047', 'TRA047', 'EN EL CAMPO', '', '', '', ''),
('BEC048', 'TRA048', 'MANTENIMIENTO DE CARRETERA', '', '', '', ''),
('BEC049', 'TRA049', 'FÁBRICA, NO ESPECIFICA DE QUÉ', 'NO ESPECIFICA', 'VACACIONES', 'NO ESPECIFICA', 'LEER LIBROS Y REALIZAR TAREAS DE LA ESCUELA'),
('BEC050', 'TRA050', 'EN EL CAMPO O DE CHALÁN DE ALBAÑIL, CERCA DE QUERÉTARO', 'SÁBADO Y DOMINGO', 'A VECES EN PERIODO ESCOLAR, Y TODAS LAS VACACIONES', '5:00 AM A 5:00 PM', 'JUGAR FOOTBALL'),
('BEC060', 'TRA060', 'PAPELERÍA', 'SÁBADOS', 'TODO EL TIEMPO', '8:00-5:00 ', 'LEER, HACER EJERCICIO, PLATICAR CON SU MAMÁ, ESCUCHAR MÚSICA. PRACTICA TAEKWONDO'),
('BEC061', 'TRA061', '', '', '', '', 'AYUDA A SU MADRE A HACER LA LIMPIEZA DEL HOGAR'),
('BEC062', 'TRA062', 'PLANTAR VERDURAS EN EL CAMPO', 'MARTES Y JUEVES', 'VACACIONES', '7-5 PM', 'AYUDA A SU MADRE A HACER LA LIMPIEZA DEL HOGAR'),
('BEC063', 'TRA063', '', '', '', '', 'NO LAS MENCIONA'),
('BEC064', 'TRA064', 'EN UNA FÁBRICA, ES OPERADOR DE UNA MÁQUNA REBABEADORA', 'TODA LA SEMANA', 'VACACIONES', '7:30 PM A 7:30 AM', 'JUGAR FOOTBALL'),
('BEC065', 'TRA065', 'EN EL CAMPO Y A VECES EN UN CIBER', 'LUNES, MARTES, JUEVES Y SÁBADOS', 'VACACIONES', '8:00 AM A 5:00 PM', 'VER TELEVISIÓN, HACER TAREAS, ESTAR EN INTERNET Y AYUDAR A SU MAMÁ A HACER QUEHACERES DE LA CASA'),
('BEC066', 'TRA066', '', '', '', '', 'HACER EJERCICIO Y SALIR A CORRER'),
('BEC067', 'TRA067', '', '', '', '', 'A VECES VISITA A SUS TÍOS '),
('BEC068', 'TRA068', 'EN LA OBRA', 'LUNES A SÁBADO', 'VACACIONES', '8:00-5:00 PM', 'AYUDA EN EL HOGAR'),
('BEC069', 'TRA069', '', '', '', '', 'AYUDA EN LAS LABORES DE LA CASA, LE GUSTA ESCRIBIR CANCIONES, JUGAR CON SU HERMANA Y VER LA TELEVISIÓN');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `trabajo`
--
ALTER TABLE `trabajo`
  ADD PRIMARY KEY (`IdTrabajo`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `trabajo`
--
ALTER TABLE `trabajo`
  ADD CONSTRAINT `trabajo_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

CREATE TABLE `transportes` (
  `IdBecario` varchar(6) NOT NULL,
  `IdTransporte` varchar(6) NOT NULL,
  `TipoTraslado` text,
  `Costo` int(11) DEFAULT NULL,
  `IdaVuelto` text,
  `Tiempo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `transportes`
--

INSERT INTO `transportes` (`IdBecario`, `IdTransporte`, `TipoTraslado`, `Costo`, `IdaVuelto`, `Tiempo`) VALUES
('BEC001', 'TRN001', 'CAMINANDO', 0, '', '30 MINUTOS'),
('BEC002', 'TRN002', 'CAMIÓN', 18, '', '30 MINUTOS'),
('BEC003', 'TRN003', 'AUTOBÚS FORÁNEO', 26, 'IDA Y VUELTA', '55 MINUTOS'),
('BEC004', 'TRN004', 'COMBI', 0, '', '15 MINUTOS'),
('BEC005', 'TRN005', 'CAMINANDO', 0, 'IDA Y VUELTA', '60 MINUTOS'),
('BEC006', 'TRN006', 'CAMIÓN', 18, '', '15 MINUTOS'),
('BEC007', 'TRN007', 'CAMIÓN', 50, 'IDA Y VUELTA', '20 MINUTOS'),
('BEC008', 'TRN008', 'CAMINANDO', 10, 'IDA', '60 MINUTOS'),
('BEC009', 'TRN009', 'COMBI', 30, '', '60 MINUTOS'),
('BEC010', 'TRN010', 'CAMINANDO', 0, '', '30 MINUTOS'),
('BEC020', 'TRN020', 'CAMIÓN', 18, '', '20 MINUTOS'),
('BEC021', 'TRN021', 'AUTOBÚS FORÁNEO', 155, 'IDA Y VUELTA', '40 MINUTOS'),
('BEC022', 'TRN022', 'CAMIÓN', 50, '', '30 MINUTOS'),
('BEC023', 'TRN023', 'CAMIÓN', 18, '', '20 MINUTOS'),
('BEC024', 'TRN024', 'CAMIÓN', 18, '', '45 MINUTOS'),
('BEC025', 'TRN025', 'CAMINANDO', 0, '', '40 MINUTOS'),
('BEC026', 'TRN026', 'CAMINANDO', 0, '', '30 MINUTOS'),
('BEC027', 'TRN027', 'CAMINANDO', 0, 'IDA Y VUELTA', '15 MINUTOS'),
('BEC028', 'TRN028', 'CAMINANDO', 0, '', '15 MINUTOS'),
('BEC029', 'TRN029', 'CAMIÓN', 35, '', '40 MINUTOS'),
('BEC030', 'TRN030', 'CAMIÓN', 15, '', '30 MINUTOS'),
('BEC040', 'TRN040', 'COMBI', 28, '', '30 MINUTOS'),
('BEC041', 'TRN041', 'AUTOBÚS FORÁNEO', 150, 'IDA Y VUELTA', '240 MINUTOS'),
('BEC042', 'TRN042', 'CAMIÓN', 15, '', '30 MINUTOS'),
('BEC043', 'TRN043', 'AUTOBÚS FORÁNEO', 9, '', '30 MINUTOS'),
('BEC044', 'TRN044', 'AUTOBÚS FORÁNEO', 9, 'IDA', '45 MINUTOS'),
('BEC045', 'TRN045', 'CAMINANDO', 0, '', '15 MINUTOS'),
('BEC046', 'TRN046', 'CAMIÓN', 9, '', '20 MINUTOS'),
('BEC047', 'TRN047', 'CAMINANDO', 0, '', '60 MINUTOS'),
('BEC048', 'TRN048', 'BICICLETA', 0, '', '40 MINUTOS'),
('BEC049', 'TRN049', 'CAMIÓN', 140, 'Ida/vuelta', '90 MINUTOS'),
('BEC050', 'TRN050', 'CAMINANDO', 0, '', '30 MINUTOS'),
('BEC060', 'TRN060', 'EN CAMIÓN Y LUEGO CAMINANDO ', 20, 'Ida/vuelta', '45 MINUTOS'),
('BEC061', 'TRN061', 'CAMINANDO', 0, '', '60 MINUTOS'),
('BEC062', 'TRN062', 'CAMIÓN', 20, 'Ida/vuelta', '30 MINUTOS'),
('BEC063', 'TRN063', 'CAMIÓN', 22, 'Ida/vuelta', '25 MINUTOS'),
('BEC064', 'TRN064', 'TRANSPORTE ESCOLAR', 0, 'Ida/vuelta', '20 MINUTOS'),
('BEC065', 'TRN065', 'CAMIÓN', 20, 'Ida/vuelta', '20 MINUTOS'),
('BEC066', 'TRN066', 'CAMINANDO', 0, 'Ida/vuelta', '20 MINUTOS'),
('BEC067', 'TRN067', 'CAMINANDO', 0, '', '20 MINUTOS'),
('BEC068', 'TRN068', 'CAMINANDO', 0, '', '25 MINUTOS'),
('BEC069', 'TRN069', 'CAMINANDO', 0, '', '90 MINUTOS');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `transportes`
--
ALTER TABLE `transportes`
  ADD PRIMARY KEY (`IdTransporte`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `transportes`
--
ALTER TABLE `transportes`
  ADD CONSTRAINT `transportes_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

CREATE TABLE `eventos` (
  `IdEvento` varchar(6) NOT NULL,
  `Nombre` text,
  `Descripcion` longtext,
  `Direccion` text,
  `FechaInicio` date DEFAULT NULL,
  `FechaFin` date DEFAULT NULL,
  `Foto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Eventos`
--

INSERT INTO `Eventos` (`IdEvento`, `Nombre`, `Descripcion`, `Direccion`, `FechaInicio`, `FechaFin`, `Foto`) VALUES
('Eve001', 'Fiesta de Pedrito', 'Pedrito tiene una fiesta, acompañalo a celebrar!', 'Casa de Pedrito', '2017-05-20', '2017-05-20', '0b91dfa4a2c5e48524db8dc1a20f8a97_280_200.jpg'),
('Eve002', 'Almuerzo Otomi', 'Comida organizada por la comunidad Otomi', 'Sierra Zapaliname', '2017-05-21', '2017-05-21', '9a85932f47168c230fc3a0539f61785e.jpg'),
('Eve003', 'Recaudacion de fondos', 'Ayudanos a recaudar fondos para ayudar a personas con menos recursos', 'Centro de Queretaro', '2017-05-22', '2017-05-22', '769_10154506254891840_7531465388807112875_n.jpg'),
('Eve004', 'Carrera 5k', 'Carrera para promocionar el ejercicio y las costumbres sanas ', 'Calle Epigmenio Gonzalez', '2017-05-23', '2017-05-23', '56975_1.jpg'),
('Eve005', 'Pelea de palos', '¡Diviertete con nosotros en esta pelea epica! Palos incluidos', 'Centro de Queretaro', '2017-05-24', '2017-05-24', '167569.jpg'),
('Eve006', '¡VIVA PIÑATA!', 'Celebremos el compleaños de todos los presentes ¡Traigan piñata!', 'Centro Nacional de las Piñatas', '2017-05-25', '2017-05-25', '11212750_10154209149376840_408322369398830252_n.jpg'),
('Eve007', 'Feria del huarache', 'Feria donde se regalan huaraches; muy comodos y resistentes', 'Feria de Ciudad Mante', '2017-05-26', '2017-05-26', '12219331_1045407088823057_1956404803850185851_n.jpg'),
('Eve008', 'Concurso de lanzar piedras', 'Demuestra tus habilidades para lanzar piedras mas lejos que nadie mas', 'A las afueras de Queretaro', '2017-05-27', '2017-05-27', '12239932_1091026414282557_7700174394024266061_n.jpg'),
('Eve009', 'Casa embrujada', 'Esta temporada de Halloween ven a visitar nuestra casa embrujada', 'Casa abandonada ', '2017-05-28', '2017-05-28', '12417715_10154397465926840_3234060867181142092_n.jpg'),
('Eve010', 'Regalando obsequios de navidad', 'No todos tenemos la misma navidad, ayudanos a que todos tengan regalos', 'Centro de Queretaro', '2017-05-29', '2017-05-29', '12801145_10154500912386840_6327556507017575751_n.jpg'),
('Eve011', 'Juego de las lagartijas', 'Ustedes corren y nosotros los apedreamos', 'Calle Epigmenio Gonzalez', '2017-05-30', '2017-05-30', '12802735_1579057448983383_9072556779418108043_n.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Eventos`
--
ALTER TABLE `Eventos`
  ADD PRIMARY KEY (`IdEvento`);
  
CREATE TABLE `actividades` (
  `IdActividad` varchar(6) NOT NULL,
  `Nombre` text,
  `DescripcionCorta` text,
  `Descripcion` longtext,
  `Fecha` date DEFAULT NULL,
  `Direccion` text,
  `Foto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `actividades`
--

INSERT INTO `actividades` (`IdActividad`, `Nombre`, `DescripcionCorta`, `Descripcion`, `Fecha`, `Direccion`, `Foto`) VALUES
('Act001', 'Fiesta de Pedrito', 'Celebra con Pedrito', 'Pedrito tiene una fiesta, acompañalo a celebrar!', '2017-11-19', 'Casa de Pedrito', '0b91dfa4a2c5e48524db8dc1a20f8a97_280_200.jpg'),
('Act002', 'Almuerzo Otomi', '¡Comida estilo Otomi!', 'Comida organizada por la comunidad Otomi', '2017-11-20', 'Sierra Zapaliname', '9a85932f47168c230fc3a0539f61785e.jpg'),
('Act003', 'Recaudacion de fondos', '¡Ayuden a recaudar!', 'Ayudanos a recaudar fondos para ayudar a personas con menos recursos', '2017-11-21', 'Centro de Queretaro', '769_10154506254891840_7531465388807112875_n.jpg'),
('Act004', 'Carrera 5k', 'Promocion de ejercicio', 'Carrera para promocionar el ejercicio y las costumbres sanas ', '2017-11-22', 'Calle Epigmenio Gonzalez', '56975_1.jpg'),
('Act005', 'Pelea de palos', 'Batalla epica de palos', '¡Diviertete con nosotros en esta pelea epica! Palos incluidos', '2017-11-23', 'Centro de Queretaro', '167569.jpg'),
('Act006', '¡VIVA PIÑATA!', '¡Trae tu piñata!', 'Celebremos el compleaños de todos los presentes ¡Traigan piñata!', '2017-11-24', 'Centro Nacional de las Piñatas', '11212750_10154209149376840_408322369398830252_n.jpg'),
('Act007', 'Feria del huarache', 'Regalando Huaraches', 'Feria donde se regalan huaraches; muy comodos y resistentes', '2017-11-25', 'Feria de Ciudad Mante', '12219331_1045407088823057_1956404803850185851_n.jpg'),
('Act008', 'Concurso de lanzar piedras', '¿Qué tan buen brazo tienes?', 'Demuestra tus habilidades para lanzar piedras mas lejos que nadie mas', '2017-11-26', 'A las afueras de Queretaro', '12239932_1091026414282557_7700174394024266061_n.jpg'),
('Act009', 'Casa embrujada', 'MADRE SANTA, UN FANTASMA', 'Esta temporada de Halloween ven a visitar nuestra casa embrujada', '2017-11-27', 'Casa abandonada ', '12417715_10154397465926840_3234060867181142092_n.jpg'),
('Act010', 'Regalando obsequios de navidad', '¡Feliz navidad a todos!', 'No todos tenemos la misma navidad, ayudanos a que todos tengan regalos', '2017-11-28', 'Centro de Queretaro', '12801145_10154500912386840_6327556507017575751_n.jpg'),
('Eve011', 'Juego de las lagartijas', 'Jueguen a las Lagartijas', 'Ustedes corren y nosotros los apedreamos', '2017-11-29', 'Calle Epigmenio Gonzalez', '12802735_1579057448983383_9072556779418108043_n.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD PRIMARY KEY (`IdActividad`);


CREATE TABLE `datosfacturas` (
  `IdFacturas` varchar(8) NOT NULL,
  `RazonSocial` text,
  `RFC` varchar(15) DEFAULT NULL,
  `Telefono` bigint(15) DEFAULT NULL,
  `Email` varchar(40) DEFAULT NULL,
  `Calle` varchar(40) DEFAULT NULL,
  `NumeroExterior` int(11) DEFAULT NULL,
  `NumeroInterior` int(11) DEFAULT NULL,
  `Colonia` int(11) DEFAULT NULL,
  `CodigoPostal` varchar(50) DEFAULT NULL,
  `Pais` text,
  `Estado` text,
  `Municipio` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `DatosFacturas`
--

INSERT INTO `DatosFacturas` (`IdFacturas`, `RazonSocial`, `RFC`, `Telefono`, `Email`, `Calle`, `NumeroExterior`, `NumeroInterior`, `Colonia`, `CodigoPostal`, `Pais`, `Estado`, `Municipio`) VALUES
('FACT0002', 'El Glogo SA de CV', 'CUPU800825569', 4422019432, 'glogo@hotmail.com', 'Ignacio Ramirez', 1, 7, 0, '31120', 'Mexico', 'Guanajuato', 'Jaral del Progreso'),
('FACT0004', 'COCA COLA COMPANY', 'GUCH6411244W0 ', 4111174290, 'cocacola@hotmail.com', 'Ignacio Aldama', 4, 3, 0, '75030', 'Mexico', 'Queretaro', 'San Juan del Rio'),
('FACT0006', 'Alfarrico S.A.', 'DIHM711110PC7 ', 4113032440, 'alfarico@hotmail.com', 'Jesus Oviedo Avendaño', 5, 4, 0, '76000', 'Mexico', 'Guanajuato', 'Cortazar'),
('FACT0008', 'BIMBO', 'MAPA780604123 ', 4613943447, 'bimbo@hotmail.com', 'Miguel Hidalgo', 4, 6, 0, '76010', 'Mexico', 'Queretaro', 'Peñamiller'),
('FACT0010', 'Hermanos Gómez y Ripoldi S.R.L.', 'AAGR740904JM1 ', 4611322331, 'hermanogr@hotmail.com', 'Benito Juarez', 10, 5, 0, '76020', 'Mexico', 'Queretaro', 'San Joaquin'),
('FACT0012', 'PRICEWATERHOUSE AND COUPERS', 'CACX051080LF1', 4611233420, 'price@hotmail.com', 'Emiliano Zapata', 23, 9, 0, '76027', 'Mexico', 'Guanajuato', 'Salvatierra'),
('FACT0014', 'La Guitarra S.A', 'BAFJ070401AZ2', 4422179421, 'lag@hotmail.com', 'Lazaro Cardenas', 12, 2, 0, '76028', 'Mexico', 'Guanajuato', 'Celaya'),
('FACT0016', 'UNIVERSIDAD NACIONAL AUTONOMA DE MEXICO ', 'OIPF290225PAI', 4112010834, 'unam@hotmail.com', 'Francisco I. Madero', 2, 67, 0, '76029', 'Mexico', 'Queretaro', 'Jalpan de Serra'),
('FACT0018', 'Sol Dorado', 'CAGM240618', 4426930132, 'sold@hotmail.com', 'Miguel Miramon', 9, 8, 0, '76030', 'Mexico', 'Guanajuato', 'Villagran'),
('FACT0020', 'El Grupo Ramos', 'CALF450228', 4112032412, 'gramos@hotmail.com', 'Juventino Rosas', 18, 10, 0, '76046', 'Mexico', 'Queretaro', 'Pinal de Amoles');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `DatosFacturas`
--
ALTER TABLE `DatosFacturas`
  ADD PRIMARY KEY (`IdFacturas`);
COMMIT;

CREATE TABLE `donativoespecie` (
  `IdDonEspecie` varchar(6) NOT NULL,
  `Donativo` text,
  `Descripcion` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `DonativoEspecie`
--

INSERT INTO `DonativoEspecie` (`IdDonEspecie`, `Donativo`, `Descripcion`) VALUES
('DoE001', 'Cobijas', 'Cobijas para cama de cualquier tamaño'),
('DoE002', 'Material escolar', 'Calculadoras, lapices, borradores, plumas, etc.'),
('DoE003', 'Tenis', 'Tenis de cualquier tipo en buen estado'),
('DoE004', 'Comida enlatada', 'Comida de cualquier tipo'),
('DoE005', 'Ropa', 'Playeras, Camisas, Pantalones'),
('DoE006', 'Ropa de invierno', 'Chaquetas, Gorros, Guantes'),
('DoE007', 'Libros', 'Libros escolares en buen estado'),
('DoE008', 'Cuadernos', 'Cuadernos utilizables y en buen estado'),
('DoE009', 'Agua', 'Agua embotellada'),
('DoE010', 'Dispositivos Electronicos', 'Cualquier tipo de dispositivo no dañado');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `DonativoEspecie`
--
ALTER TABLE `DonativoEspecie`
  ADD PRIMARY KEY (`IdDonEspecie`);
CREATE TABLE `tallerista` (
  `IdTallerista` varchar(6) NOT NULL,
  `Materia` text,
  `Descripcion` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Tallerista`
--

INSERT INTO `Tallerista` (`IdTallerista`, `Materia`, `Descripcion`) VALUES
('Tal001', 'Matematicas', 'Maestro de matematicas basicas o avanzadas'),
('Tal002', 'Español', 'Maestro de español de nivel avanzado'),
('Tal003', 'Filosofia', 'Maestro de filosofia basica'),
('Tal004', 'Ingles', 'Maestro de ingles de nivel basico o avanzado'),
('Tal005', 'Fisica', 'Maestro de Fisica de nivel preparatoria'),
('Tal006', 'Historia', 'Maestro de historia de México'),
('Tal007', 'Ciencias Naturales', 'Maestro de Ciencias Naturales'),
('Tal008', 'Quimica', 'Maestro de Quimica'),
('Tal009', 'Computacion', 'Maestro de computacion de nivel medio o avanzado'),
('Tal010', 'Musica', 'Maestro de musica. Instrumentos: Guitarra, Flauta,Triangulo');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Tallerista`
--
ALTER TABLE `Tallerista`
  ADD PRIMARY KEY (`IdTallerista`);
CREATE TABLE `usuario` (
  `IdUsuario` varchar(6) NOT NULL,
  `Usuario` text,
  `Nombre` text,
  `ApellidoP` text,
  `ApellidoM` text,
  `Correo` varchar(40) DEFAULT NULL,
  `Constraseña` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Usuario`
--

INSERT INTO `Usuario` (`IdUsuario`, `Usuario`, `Nombre`, `ApellidoP`, `ApellidoM`, `Correo`, `Constraseña`) VALUES
('USU001', 'a01064709', 'David', 'López', 'Ruiz', 'a01064709@itesm.mx', 'dava01'),
('USU002', 'A01207360', 'Romel', 'Martinez', 'Olvera', 'A01207360@itesm.mx', 'roma01'),
('USU003', 'a01701249', 'Juan Pablo', 'Aboytes', 'Lovoa', 'A01701249@itesm.mx', 'juaa01'),
('USU004', 'a00999546', 'Omar', 'DeAlba', 'Garza', 'A00999546@itesm.mx', 'omar01');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD PRIMARY KEY (`IdUsuario`);

CREATE TABLE `donacion` (
  `IdDonacion` varchar(6) NOT NULL,
  `Monto` int(11) DEFAULT NULL,
  `Fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `donacion`
--

INSERT INTO `donacion` (`IdDonacion`, `Monto`, `Fecha`) VALUES
('DON001', 200, '2017-03-12'),
('DON002', 213, '2017-03-13'),
('DON003', 243, '2017-03-14'),
('DON004', 234, '2017-03-15'),
('DON005', 112, '2017-03-16'),
('DON006', 566, '2017-03-17'),
('DON007', 256, '2017-03-18'),
('DON008', 344, '2017-03-19'),
('DON009', 234, '2017-03-20'),
('DON010', 700, '2017-03-21'),
('DON020', 234, '2017-03-22'),
('DON021', 122, '2017-03-23'),
('DON022', 566, '2017-03-24'),
('DON023', 1233, '2017-03-25'),
('DON024', 233, '2017-03-26'),
('DON025', 34, '2017-03-27'),
('DON026', 56, '2017-03-28'),
('DON027', 255, '2017-03-29'),
('DON028', 233, '2017-03-30'),
('DON029', 122, '2017-03-31'),
('DON030', 7555, '2017-04-01');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `donacion`
--
ALTER TABLE `donacion`
  ADD PRIMARY KEY (`IdDonacion`);
COMMIT;

CREATE TABLE `noticias` (
  `IdNoticias` varchar(6) NOT NULL,
  `Nombre` text,
  `Descripcion` longtext,
  `Fecha` date DEFAULT NULL,
  `Tag` text,
  `Foto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`IdNoticias`, `Nombre`, `Descripcion`, `Fecha`, `Tag`, `Foto`) VALUES
('NOT001', 'Terremoto', 'Hoy surfrimos un grave accidente en mexico', '2017-12-12', 'Politica', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT002', 'Concierto', 'Hoy fuimso a concierto a pasarla bien ', '2017-12-13', 'Entretenimiento', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT003', 'Acopio', 'Puedes llevar aquí en los centros de acopio', '2017-12-14', 'Sociedad ', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT004', 'Vertice', 'Vertice esta recolectando viveres', '2017-12-15', 'Sociedad ', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT005', 'Tec', 'Estamos solicitando personas para que vengan', '2017-12-16', 'Sociedad ', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT006', 'Lluvias', 'Hay lluvias en todo queretaro', '2017-12-17', 'Clima', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT007', 'Ayuda', 'Hay muchos becarios que necesitan tu ayuda', '2017-12-18', 'Sociedad ', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT008', 'Lluvias', 'Lluvias muy fuertes', '2017-12-19', 'Clima', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT009', 'México y tu', 'Chequen estos articulos', '2017-12-20', 'Politica', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT010', 'Agua limpia', 'Miren este articulo sobre el agua', '2017-12-21', 'Cultura', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT020', 'México gana', 'México ganó el mundial', '2017-12-22', 'Deportes', 'C:UsersRomel MartinezMusicBases de datosIMGecado');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`IdNoticias`);
COMMIT;

CREATE TABLE `benefactores` (
  `IdBenefactores` varchar(6) NOT NULL,
  `Nombre` text NOT NULL,
  `ApellidoP` text,
  `ApellidoM` text,
  `RazonSocial` text,
  `FisicaOMoral` text,
  `FechaAlta` date DEFAULT NULL,
  `RFC` varchar(15) DEFAULT NULL,
  `Domicilio` varchar(50) DEFAULT NULL,
  `Municipio` text,
  `Estado` text,
  `Telefono` bigint(20) DEFAULT NULL,
  `EmailBenefactor` varchar(40) DEFAULT NULL,
  `EmailContacto` varchar(40) DEFAULT NULL,
  `RequiereFactura` text,
  `PeriodicidadAportacion` text,
  `Monto` int(11) DEFAULT NULL,
  `TiempoMembresia` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `benefactores`
--

INSERT INTO `benefactores` (`IdBenefactores`, `Nombre`, `ApellidoP`, `ApellidoM`, `RazonSocial`, `FisicaOMoral`, `FechaAlta`, `RFC`, `Domicilio`, `Municipio`, `Estado`, `Telefono`, `EmailBenefactor`, `EmailContacto`, `RequiereFactura`, `PeriodicidadAportacion`, `Monto`, `TiempoMembresia`) VALUES
('BFT001', 'María Alejandra', 'Daza', 'Covarrubias', 'Consorcio Invercop S.A. de C.V.', 'Moral', '2016-09-27', 'CIN080826CVA', 'Bosques Chapultepec 307 C Col Arboledas 1era Secci', 'Celaya', 'Guanajuato', 4421863957, 'alejandra.daza@alterra.com.mx ', 'laura.malagon@alterra.me', 'Si', 'Mensual', 1500, 'Emprendedor'),
('BFT002', 'Andres', 'Ortega', 'González', '', 'Fisica', '2016-08-29', 'OEGA49102092A', 'Carrillo 104 Col Villas del Mesón Juriquilla', 'Querétaro', 'Querétaro', 4422426762, 'aortega3322@live.com', 'aragomez.correo@gmail.com', 'Si', 'Mensual', 2000, 'Emprendedor'),
('BFT003', 'Moises', 'Miranda', 'Álvarez', 'Firma Sien S.C.', 'Moral', '2016-08-26', 'FSI140414NJ0', 'Bernardo Quintana 439-404 Col Centro Sur', 'Querétaro', 'Querétaro', 4424021010, 'mma@firmasien.mx', 'storres@firmasien.mx', 'Si', 'Mensual', 5000, 'Corporativo'),
('BFT004', 'Bernardo ', 'Castellanos', '', 'Comunicación Útil S.A. de C.V.', 'Moral', '2017-08-08', 'CUT0811141I5', 'Ret. Farrallón Mz. 2 Lote 8 12 SM 15A', 'Cancún', 'Quintana roo', 9982145938, 'bernardocut@gmail.com', 'castellanos.arturo@hotmail.com ', 'Si', 'Mensual', 1500, 'Emprendedor'),
('BFT005', 'Miguel Ángel', 'Feregrino', 'Feregrino', '', 'Fisica', '2017-01-09', '', '', 'Querétaro', 'Querétaro', 4422499806, 'feregrinoferegrino@yahoo.com.mx', 'feregrinoferegrino@yahoo.com.mx', 'No', 'Mensual', 5000, 'Corporativo'),
('BFT006', 'Jesús Alberto', 'Sánchez', 'Hernández', '', 'Fisica', '2016-08-22', ' SAHJ6311292M6', 'Manuel Herrera 20 Col San Javier', 'Querétaro', 'Querétaro', 4422421360, 'jesus.sanchez@alterra.com.mx', 'jesus.sanchez@alterra.com.mx', 'Si', 'Mensual', 2000, 'Emprendedor'),
('BFT007', 'Gerardo', 'Proal', 'de la Isla', '', 'Fisica', '2016-08-25', 'POIG600420EQ8', 'Blvd Gobernadores 1001- 14 Col MonteBlanco III ', 'Querétaro', 'Querétaro', 4423180380, 'gproal@codigoaureo.com', 'gproal@codigoaureo.com', 'Si', 'Semestral', 6000, 'Emprendedor'),
('BFT008', 'Mario Humberto', 'Paulin', 'Nardoni', 'PR Estudio Legal S.C.', 'Moral', '2017-10-01', 'PEL161123EX5', 'Allende Sur 27 Col Centro', 'Querétaro', 'Querétaro', 4422141476, 'mhp@paulinrivera.com', 'mng@paulinrivera.com', 'Si', 'Mensual', 2500, 'Corporativo'),
('BFT009', 'Arturo Ricardo', 'Olivares ', 'Hernández', 'Clic magnetic eyewear', 'Fisica', '2017-06-07', 'OIHA521005JJ1', 'Avellanas 30 Valle de Los Olivos ', 'Querétaro', 'Querétaro', 4424030430, 'ricardoolivares1@hotmail.com ', 'ricardoolivares1@hotmail.com', 'Si', 'Esporadica', 5000, 'Corporativo'),
('BFT010', 'Francisco Javier ', 'Sánchez', 'Hernández', 'Residencial Alterra I. S.A. de C.V', 'Fisica', '2016-07-07', 'SAHF650122NJ8', 'Río Ayutla 43 Col La Piedad', 'Querétaro', 'Querétaro', 4422421360, 'javier.sanchez@alterra.com.mx', 'juanjose.barrera@alterra.me', 'Si', 'Mensual', 32000, 'Corporativo'),
('BFT020', 'Jorge', 'Sánchez', 'Hernández', '', 'Fisica', '2017-10-01', '', '', 'Querétaro', 'Querétaro', 4424124192, '', ' jackecham@hotmail.com', 'Si', 'Trimestral', 1500, 'Junior'),
('BFT021', 'Omar', 'DeAlba', 'Garza', '', 'Moral', '2016-09-04', 'OIHA521005JJ2', 'Río Ayutla 43 Col La Piedad', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 2800, 'Junior'),
('BFT022', 'Juan', 'Aboytes', 'García', '', 'Fisica', '2017-06-05', 'OIHA521005JJ2', 'Avellanas 30 Valle de Los Olivos ', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 5000, 'Corporativo'),
('BFT023', 'David ', 'Lopez', 'Aboytes', '', 'Moral', '2017-10-09', 'OIHA521005JJ2', 'Avellanas 30 Valle de Los Olivos ', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 3000, 'emprendedor'),
('BFT024', 'Juana', 'Ruiz', 'Dominguez', '', 'Fisica', '2017-06-05', 'OIHA521005JJ2', 'Allende Sur 27 Col Centro', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 504, 'emprendedor'),
('BFT025', 'Rodrigo', 'Garcia', 'Zurita', '', 'Moral', '2008-08-08', 'OIHA521005JJ2', 'Avellanas 30 Valle de Los Olivos ', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 795, 'Junior'),
('BFT026', 'Miguel', 'Alcantar', 'Fernandez', '', 'Fisica', '2017-10-09', 'OIHA521005JJ2', 'Allende Sur 27 Col Centro', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 456, ' emprendedor'),
('BFT027', 'Joaquin', 'Lopez', 'Doriga', '', 'Moral', '2016-09-06', 'OIHA521005JJ2', '', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 123, 'Junior'),
('BFT028', 'Alejandro', 'Fernandez', 'Doriga', '', 'Fisica', '2016-09-04', 'OIHA521005JJ2', 'Río Ayutla 43 Col La Piedad', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 758, 'emprendedor'),
('BFT029', 'Javier', 'Dorantes', 'Zuñiga', '', 'Moral', '2017-10-06', 'OIHA521005JJ2', '', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 988, 'Junior'),
('BFT030', 'Ricardo', 'Cortes ', 'Martinez', '', 'Fisica', '2016-08-07', 'OIHA521005JJ2', 'Río Ayutla 43 Col La Piedad', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 566, 'Junior'),
('BFT040', 'Eduardo', 'Tejada', 'Flores', '', '', '2017-11-09', 'OIHA521005JJ2', '', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 266, 'emprendedor'),
('BFT041', 'David ', 'Martinez', 'Olvera', '', '', '2017-12-09', 'OIHA521005JJ2', 'Blvd Gobernadores 1001- 14 Col MonteBlanco III ', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 233, 'Junior');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `benefactores`
--
ALTER TABLE `benefactores`
  ADD PRIMARY KEY (`IdBenefactores`);
COMMIT;

CREATE TABLE `solicitudregistro` (
  `IdSolicitud` varchar(6) NOT NULL,
  `Usuario` text,
  `Nombre` text,
  `ApellidoP` text,
  `ApellidoM` text,
  `Email` varchar(40) DEFAULT NULL,
  `FechaRegistro` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `SolicitudRegistro`
--

INSERT INTO `SolicitudRegistro` (`IdSolicitud`, `Usuario`, `Nombre`, `ApellidoP`, `ApellidoM`, `Email`, `FechaRegistro`) VALUES
('RUS001', 'ricky', 'Ricardo', 'Gonzalez', 'Delgado', 'ricky@hotmail.com', '0000-00-00'),
('RUS002', 'lalo', 'Eduardo', 'Martinez', 'Mendez', 'lalo@hotmail.com', '0000-00-00'),
('RUS003', 'german', 'Gerardo', 'Gutierrez', 'Castillo', 'german@hotmail.com', '0000-00-00'),
('RUS004', 'charly', 'Carlos', 'Fuentes', 'Marquez', 'charly@hotmail.com', '0000-00-00'),
('RUS005', 'carol', 'Caro', 'Ramirez', 'Cruz', 'carol@hotmail.com', '0000-00-00'),
('RUS006', 'james', 'Jaime', 'Roman', 'Medina', 'james@hotmail.com', '0000-00-00'),
('RUS007', 'jhon', 'Juan', 'Casas', 'Iglesias', 'jhon@hotmail.com', '0000-00-00'),
('RUS008', 'franco', 'Francisco', 'Rodriguez', 'Gomez', 'franco@hotmail.com', '0000-00-00'),
('RUS009', 'emma', 'Emmanuel', 'Ruiz', 'Rojas', 'emma@hotmail.com', '0000-00-00'),
('RUS010', 'manny', 'Manuel', 'Garcia', 'Reyes', 'manny@hotmail.com', '0000-00-00'),
('RUS020', 'elhenry', 'Enrique', 'Palacios', 'Luna', 'elhenry@hotmail.com', '0000-00-00'),
('RUS021', 'sara', 'Sara', 'Garza', 'Campos', 'sara@hotmail.com', '0000-00-00'),
('RUS022', 'dianita', 'Diana', 'Perez', 'Rubio', 'dianita@hotmail.com', '0000-00-00'),
('RUS023', 'brends', 'Brenda', 'Diaz', 'Leon', 'brends@hotmail.com', '0000-00-00'),
('RUS024', 'paty', 'Patricia', 'Alvarez', 'Aguilar', 'paty@hotmail.com', '0000-00-00'),
('RUS025', 'ann', 'Ana', 'Moreno', 'Cano', 'ann@hotmail.com', '0000-00-00'),
('RUS026', 'sofy', 'Sofia', 'Navarro', 'Herrero', 'sofy@hotmail.com', '0000-00-00'),
('RUS027', 'enamerica', 'Laura', 'Serrano', 'Guerrero', 'enamerica@hotmail.com', '0000-00-00'),
('RUS028', 'pao', 'Paola', 'Ortiz', 'Cortes', 'pao@hotmail.com', '0000-00-00'),
('RUS029', 'mary', 'Mariana', 'Ortega', 'Rios', 'mary@hotmail.com', '0000-00-00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `SolicitudRegistro`
--
ALTER TABLE `SolicitudRegistro`
  ADD PRIMARY KEY (`IdSolicitud`);

CREATE TABLE `privilegios` (
  `IdPermiso` varchar(6) NOT NULL,
  `Permiso` text,
  `Descripcion` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Privilegios`
--

INSERT INTO `Privilegios` (`IdPermiso`, `Permiso`, `Descripcion`) VALUES
('Per001', 'Subir Informe', 'El usuario con este privilegio sera capaz de Subir informes'),
('Per002', 'Editar Informe', 'El usuario con este privilegio sera capaz de Editar Informes'),
('Per003', 'Eliminar Informe', 'El usuario con este privilegio sera capaz de Elminar Informes'),
('Per004', 'Registrar Colaborador', 'El usuario con este privilegio sera capaz de Registrar Colaboradores'),
('Per005', 'Editar Colaborador', 'El usuario con este privilegio sera capaz de Editar informacion de Colaboradores'),
('Per006', 'Eliminar Colaborador', 'El usuario con este privilegio sera capaz de Eliminar Colaboradores'),
('Per007', 'Registrar Boletin', 'El usuario con este privilegio sera capaz de Registrar Boletines'),
('Per008', 'Editar Boletin', 'El usuario con este privilegio sera capaz de Editar Boletines'),
('Per009', 'Eliminar Boletin', 'El usuario con este privilegio sera capaz de Eliminar Boletines'),
('Per010', 'Registrar Noticia', 'El usuario con este privilegio sera capaz de Registrar nuevas noticias'),
('Per011', 'Editar Noticia', 'El usuario con este privilegio sera capaz de Editar informacion de Noticias'),
('Per012', 'Eliminar Noticia', 'El usuario con este privilegio sera capaz de Eliminar Noticias'),
('Per013', 'Registrar Actividad', 'El usuario con este privilegio sera capaz de Registrar Actividades'),
('Per014', 'Eliminar Actividad', 'El usuario con este privilegio sera capaz de Eliminar Actividades'),
('Per015', 'Editar Actividad', 'El usuario con este privilegio sera capaz de Editar Actividades'),
('Per016', 'Generar ReporteA', 'El usuario con este privilegio sera capaz de generar Reportes de las Actividades'),
('Per017', 'Generar ReporteD', 'El usuario con este privilegio sera capaz de generar Reportes de las Donaciones'),
('Per018', 'Generar ReporteB', 'El usuario con este privilegio sera capaz de generar Reportes de los Becarios'),
('Per019', 'Registrar Benefactor', 'El usuario con este privilegio sera capaz de Registrar nuevos Benefactores'),
('Per020', 'Editar Benefactor', 'El usuario con este privilegio sera capaz de Editar informacion de los Benefactores'),
('Per021', 'Eliminar Benefactor', 'El usuario con este privilegio sera capaz de Eliminar Benefactores'),
('Per022', 'RegistrarEvento', 'El usuario con este privilegio sera capaz de Registrar nuevos Eventos'),
('Per023', 'Editar Evento', 'El usuario con este privilegio sera capaz de Editar la informacion de los Eventos'),
('Per024', 'Eliminar Evento', 'El usuario con este privilegio sera capaz de Eliminar Eventos');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Privilegios`
--
ALTER TABLE `Privilegios`
  ADD PRIMARY KEY (`IdPermiso`);







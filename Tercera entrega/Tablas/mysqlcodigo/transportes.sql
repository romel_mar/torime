﻿-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-10-2017 a las 05:47:13
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vertice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transportes`
--






CREATE TABLE `transportes` (
  `IdBecario` varchar(6) NOT NULL,
  `IdTransporte` varchar(6) NOT NULL,
  `TipoTraslado` text,
  `Costo` int(11) DEFAULT NULL,
  `IdaVuelto` text,
  `Tiempo` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `transportes`
--

INSERT INTO `transportes` (`IdBecario`, `IdTransporte`, `TipoTraslado`, `Costo`, `IdaVuelto`, `Tiempo`) VALUES
('BEC001', 'TRN001', 'CAMINANDO', 0, '', '30 MINUTOS'),
('BEC002', 'TRN002', 'CAMIÓN', 18, '', '30 MINUTOS'),
('BEC003', 'TRN003', 'AUTOBÚS FORÁNEO', 26, 'IDA Y VUELTA', '55 MINUTOS'),
('BEC004', 'TRN004', 'COMBI', 0, '', '15 MINUTOS'),
('BEC005', 'TRN005', 'CAMINANDO', 0, 'IDA Y VUELTA', '60 MINUTOS'),
('BEC006', 'TRN006', 'CAMIÓN', 18, '', '15 MINUTOS'),
('BEC007', 'TRN007', 'CAMIÓN', 50, 'IDA Y VUELTA', '20 MINUTOS'),
('BEC008', 'TRN008', 'CAMINANDO', 10, 'IDA', '60 MINUTOS'),
('BEC009', 'TRN009', 'COMBI', 30, '', '60 MINUTOS'),
('BEC010', 'TRN010', 'CAMINANDO', 0, '', '30 MINUTOS'),
('BEC020', 'TRN020', 'CAMIÓN', 18, '', '20 MINUTOS'),
('BEC021', 'TRN021', 'AUTOBÚS FORÁNEO', 155, 'IDA Y VUELTA', '40 MINUTOS'),
('BEC022', 'TRN022', 'CAMIÓN', 50, '', '30 MINUTOS'),
('BEC023', 'TRN023', 'CAMIÓN', 18, '', '20 MINUTOS'),
('BEC024', 'TRN024', 'CAMIÓN', 18, '', '45 MINUTOS'),
('BEC025', 'TRN025', 'CAMINANDO', 0, '', '40 MINUTOS'),
('BEC026', 'TRN026', 'CAMINANDO', 0, '', '30 MINUTOS'),
('BEC027', 'TRN027', 'CAMINANDO', 0, 'IDA Y VUELTA', '15 MINUTOS'),
('BEC028', 'TRN028', 'CAMINANDO', 0, '', '15 MINUTOS'),
('BEC029', 'TRN029', 'CAMIÓN', 35, '', '40 MINUTOS'),
('BEC030', 'TRN030', 'CAMIÓN', 15, '', '30 MINUTOS'),
('BEC040', 'TRN040', 'COMBI', 28, '', '30 MINUTOS'),
('BEC041', 'TRN041', 'AUTOBÚS FORÁNEO', 150, 'IDA Y VUELTA', '240 MINUTOS'),
('BEC042', 'TRN042', 'CAMIÓN', 15, '', '30 MINUTOS'),
('BEC043', 'TRN043', 'AUTOBÚS FORÁNEO', 9, '', '30 MINUTOS'),
('BEC044', 'TRN044', 'AUTOBÚS FORÁNEO', 9, 'IDA', '45 MINUTOS'),
('BEC045', 'TRN045', 'CAMINANDO', 0, '', '15 MINUTOS'),
('BEC046', 'TRN046', 'CAMIÓN', 9, '', '20 MINUTOS'),
('BEC047', 'TRN047', 'CAMINANDO', 0, '', '60 MINUTOS'),
('BEC048', 'TRN048', 'BICICLETA', 0, '', '40 MINUTOS'),
('BEC049', 'TRN049', 'CAMIÓN', 140, 'Ida/vuelta', '90 MINUTOS'),
('BEC050', 'TRN050', 'CAMINANDO', 0, '', '30 MINUTOS'),
('BEC060', 'TRN060', 'EN CAMIÓN Y LUEGO CAMINANDO ', 20, 'Ida/vuelta', '45 MINUTOS'),
('BEC061', 'TRN061', 'CAMINANDO', 0, '', '60 MINUTOS'),
('BEC062', 'TRN062', 'CAMIÓN', 20, 'Ida/vuelta', '30 MINUTOS'),
('BEC063', 'TRN063', 'CAMIÓN', 22, 'Ida/vuelta', '25 MINUTOS'),
('BEC064', 'TRN064', 'TRANSPORTE ESCOLAR', 0, 'Ida/vuelta', '20 MINUTOS'),
('BEC065', 'TRN065', 'CAMIÓN', 20, 'Ida/vuelta', '20 MINUTOS'),
('BEC066', 'TRN066', 'CAMINANDO', 0, 'Ida/vuelta', '20 MINUTOS'),
('BEC067', 'TRN067', 'CAMINANDO', 0, '', '20 MINUTOS'),
('BEC068', 'TRN068', 'CAMINANDO', 0, '', '25 MINUTOS'),
('BEC069', 'TRN069', 'CAMINANDO', 0, '', '90 MINUTOS');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `transportes`
--
ALTER TABLE `transportes`
  ADD PRIMARY KEY (`IdTransporte`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `transportes`
--
ALTER TABLE `transportes`
  ADD CONSTRAINT `transportes_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

﻿-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-10-2017 a las 05:40:22
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vertice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `becario`
--




CREATE TABLE `becario` (
  `IdBecario` varchar(6) NOT NULL,
  `Folio` int(11) NOT NULL,
  `Nombre` text NOT NULL,
  `ApellidoP` text NOT NULL,
  `ApellidoM` text NOT NULL,
  `CURP` varchar(18) NOT NULL,
  `FechaNacimiento` date NOT NULL,
  `Email` varchar(30) DEFAULT NULL,
  `Estatus` text NOT NULL,
  `Sexo` text NOT NULL,
  `LugarNacimiento` text NOT NULL,
  `Comunidad` text NOT NULL,
  `FechaEntrevista` date NOT NULL,
  `Prioridad` int(11) NOT NULL,
  `Grado` varchar(50) NOT NULL,
  `Escuela` varchar(50) DEFAULT NULL,
  `Nivel` varchar(20) DEFAULT NULL,
  `Carrera` text,
  `PromedioInicial` float NOT NULL,
  `PromedioActual` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `becario`
--

INSERT INTO `becario` (`IdBecario`, `Folio`, `Nombre`, `ApellidoP`, `ApellidoM`, `CURP`, `FechaNacimiento`, `Email`, `Estatus`, `Sexo`, `LugarNacimiento`, `Comunidad`, `FechaEntrevista`, `Prioridad`, `Grado`, `Escuela`, `Nivel`, `Carrera`, `PromedioInicial`, `PromedioActual`) VALUES
('BEC001', 1, 'ROSALÍA', 'BALTAZAR', 'MORALES', 'BAMR011230MQTLRSA4', '0000-00-00', '-', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'EL SAUCITO', '0000-00-00', 2, 'SECUNDARIA TERMINADA', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 7.4, 7),
('BEC002', 2, 'ELIZABETH', 'BLAS', 'DE LEÓN', 'BALE000903MQTLNLA5', '0000-00-00', '', 'Regular', 'Femenino', 'TOLIMAN', 'CERRITO PARADO', '0000-00-00', 1, '2° SEM BACHILLERATO', 'COBAQ 6', '3° SEM', '-', 8.8, 7),
('BEC003', 3, 'NANCY', 'DE LEÓN', 'GONZÁLEZ', 'LEGN000405MQTNNNA3', '0000-00-00', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 2, '4° SEM BACHILLERATO', 'COBAQ 6', '5° SEM', '', 9.29, 9),
('BEC004', 4, 'DANIEL', 'DE LEÓN', 'GUDIÑO', 'LEGD981206HQTNDN04', '0000-00-00', 'danileo1298@gmail.com', 'Baja', 'Masculino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, 'BACHILLERATO TERMINADO', '-', '-', '-', 8.2, 0),
('BEC005', 5, 'JOSÉ RAUL', 'DE LEÓN', 'MORALES', 'LEMR011129HQTNRLA0', '0000-00-00', '-', 'Baja', 'Masculino', 'TOLIMAN', 'MESA DE CHAGOYA', '0000-00-00', 2, '2° SEM BACHILLERATO', 'TELEBACHILLERATO COMUNITARIO', '3° SEM', '-', 7.5, 0),
('BEC006', 6, 'CARLA BIBIANA', 'DE LEÓN', 'SÁNCHEZ', 'LESC990123MQTNNR03', '0000-00-00', '-', 'Baja', 'Femenino', 'CADEREYTA DE MONTES', 'CERRITO PARADO', '0000-00-00', 2, '5° SEM BACHILLERATO', '-', '-', '-', 8.9, 0),
('BEC007', 7, 'VICTOR MANUEL', 'DE LEÓN', 'TREJO', 'LETV010721HQTNRCA7', '0000-00-00', '-', 'Baja', 'Masculino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 7, 0),
('BEC008', 8, 'YESICA', 'GONZALEZ', 'GONZÁLEZ', 'GOGY991005MQTNNS07', '0000-00-00', 'yesigonzalesjgg@gmail.com', 'Regular', 'Femenino', 'GUANAJUATO', 'LOS GONZALEZ', '0000-00-00', 2, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 8.7, 8),
('BEC009', 9, 'MARÍA ELENA', 'DE LEÓN', 'GUDIÑO', 'LEGE020508MNENDLA4', '0000-00-00', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, 'SECUNDARIA TERMINADA', 'EMSAD 24', '1° SEM', '-', 9.8, 8),
('BEC010', 10, 'ALEJANDRA ', 'GUDIÑO ', 'GUDIÑO', 'GUGA020405MQTDDLA9', '0000-00-00', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, 'SECUNDARIA TERMINADA', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 7.2, 7),
('BEC020', 11, 'MELISSA', 'GUDIÑO ', 'GUDIÑO', 'GUGM000818MQTDDLA4', '0000-00-00', 'melinna1212@gmail.com', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 9.14, 9),
('BEC021', 12, 'SOFIA', 'GUDIÑO ', 'GUDIÑO', 'GUGS940926MQTDDF06', '0000-00-00', 'sofiggextr@hotmail.com', 'Baja', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, '2° CUATRI LICENCIATURA', '-', '-', '-', 9.6, 0),
('BEC022', 13, 'DANIELA', 'GUDIÑO ', 'PÉREZ', 'GUPD010928MQTDRNA9', '0000-00-00', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 9.2, 8),
('BEC023', 14, 'ANA CRISTINA', 'GUDIÑO ', 'YATA', 'GUYA000615MGTDTNA2', '0000-00-00', 'cristi.yata.1517@gmail.com', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'MESA DE RAMIREZ', '0000-00-00', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 9, 8),
('BEC024', 15, 'YESSICA', 'LUNA', 'GUDIÑO', 'LUGY010619MQTNDSA5', '0000-00-00', '4424489828', 'Regular', 'Femenino', 'TOLIMAN', 'EL SAUCITO', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 7.1, 7),
('BEC025', 16, 'MARÍA BRISEIDA', 'MONTOYA', 'GUDIÑO', 'MOGB010710MQTNDRA2', '0000-00-00', '-', 'Baja', 'Femenino', 'TOLIMAN', 'LA PRESITA', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8.2, 0),
('BEC026', 17, 'LUIS ALBERTO', 'MORALES ', 'DE LEÓN', 'MOLL000501HQTRNSA0', '0000-00-00', '-', 'Regular', 'Masculino', 'CADEREYTA DE MONTES', 'MESA DE CHAGOYA', '0000-00-00', 1, '4° SEM BACHILLERATO', 'TELEBACHILLERATO COMUNITARIO', '5° SEM', '-', 7.8, 7),
('BEC027', 18, 'AURELIA', 'MORALES', 'GONZÁLEZ', 'MOGA990114MQTRNR02', '0000-00-00', 'aurelitamorales1999@gmail.com', 'Baja', 'Femenino', 'QUERETARO', 'SABINO DE SAN AMBROSIO', '0000-00-00', 1, 'BACHILLERATO TERMINADO', '-', '-', '-', 7.6, 0),
('BEC028', 19, 'FABIOLA', 'MORALES ', 'GUDIÑO', 'MOGF011119MQTRDBA1', '0000-00-00', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, 'SECUNDARIA TERMINADA', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 7, 0),
('BEC029', 20, 'BRENDA EDITH', 'MORALES ', 'GUERRERO', 'MOGB010623MQTRRRA1', '0000-00-00', '-', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 7.8, 8),
('BEC030', 21, 'BERENICE', 'MORALES ', 'MARTÍNEZ', 'MOMB001001MQTRRRB8', '0000-00-00', '-', 'Regular', 'Femenino', 'TOLIMAN', 'CERRITO PARADO', '0000-00-00', 2, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 9.2, 8),
('BEC040', 22, 'BIBIANA', 'MORALES', 'MORALES', 'MOMB960612MQTRRB04', '0000-00-00', 'bsanches12314@gmail.com', 'Regular', 'Femenino', 'TOLIMAN', 'MESA DE CHAGOYA', '0000-00-00', 1, '7° SEM INGENIERÍA', 'ITQ-TOLIMÁN', '8° SEM', 'GESTIÓN EMPRESARIAL', 8.65, 8),
('BEC041', 23, 'MARÍA ADELINA', 'MORALES ', 'RESÉNDIZ', 'MORA961202MQTRSD05', '0000-00-00', 'mariaadelinamora1996@gmail.com', 'Baja', 'Femenino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 2, '1° CUATRI LICENCIATURA', '-', '-', '-', 9.1, 0),
('BEC042', 24, 'JOSÉ ANTONIO', 'OLGUIN', 'RESÉNDIZ', 'OURA010919HQTLSNA7', '0000-00-00', '-', 'Baja', 'Masculino', 'TOLIMAN', 'SAN PEDRO DE LOS EUCALIPTOS', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 0, 0),
('BEC043', 25, 'MARÍA DIANELI', 'PÉREZ', 'DE LEÓN', 'PELD000419MQTRNNA2', '0000-00-00', 'dianelipdl@gmail.com', 'Baja', 'Femenino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 7.5, 0),
('BEC044', 26, 'EMMANUEL', 'PÉREZ', 'GUDIÑO', 'PEGE000815HQTRDMA2', '0000-00-00', 'emaperz1508@gmail.com', 'Baja', 'Masculino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 7.49, 0),
('BEC045', 27, 'LUIS OCTAVIO ', 'RESÉNDIZ', 'GONZÁLEZ', 'REGL020120HQTSNSA1', '0000-00-00', '-', 'Condicionado', 'Masculino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, 'PENDIENTE', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 7, 0),
('BEC046', 28, 'LUIS GERARDO', 'RESÉNDIZ', 'MONTOYA', 'REML990703HQTSNS05', '0000-00-00', '-', 'Regular', 'Masculino', 'TOLIMAN', 'MESA DE RAMIREZ', '0000-00-00', 1, 'BACHILLERATO TERMINADO', 'ITQ-TOLIMÁN', '1° SEM', 'GESTIÓN EMPRESARIAL', 8.6, 8),
('BEC047', 29, 'GRACIELA', 'SÁNCHEZ', 'DE SÁNCHEZ', 'SASG990822MQTNNR09', '0000-00-00', '-', 'Baja', 'Femenino', 'TOLIMAN', 'ZAPOTE DE LOS URIBE', '0000-00-00', 1, 'BACHILLERATO TERMINADO', '-', '-', '', 7.5, 0),
('BEC048', 30, 'MARCOS', 'TREJO', 'SÁNCHEZ', 'TESM950610HQTRNR00', '0000-00-00', 'mtrejo14.depad.tol@gmail.com', 'Con beca', 'Masculino', 'TOLIMAN', 'CERRITO PARADO', '0000-00-00', 2, '7° SEM INGENIERÍA', 'ITQ-TOLIMÁN', '8° SEM', 'SISTEMAS COMPUTACIONALES', 8.8, 8),
('BEC049', 31, 'BEATRIZ', 'BLAS', 'SÁNCHEZ', 'BASB961009MQTLNT04', '0000-00-00', 'betilizblas0910@gmail.com', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'CERRITO PARADO', '0000-00-00', 1, '4° SEM LICENCIATURA', 'UAQ-CADEREYTA', '5° SEM', 'LIC. EN ADMINISTRACIÓN', 8, 8),
('BEC050', 32, 'ALEXANDRO', 'DE SANTIAGO', 'MONTOYA ', 'SAMA001018HQTNNLA7', '0000-00-00', 'alexandrodstgmontoya@gmail.com', 'Regular', 'Masculino', 'TOLIMAN', 'EL CIPRÉS', '0000-00-00', 1, '4° SEM BACHILLERATO', 'EMSAD 24', '5° SEM', '-', 7, 7),
('BEC060', 33, 'NELLY VIRIDIANA', 'DE SANTIAGO', 'SÁNCHEZ ', 'SASN010917MQTNNLA9', '0000-00-00', '-', 'Regular', 'Femenino', 'TOLIMAN', 'CASA BLANCA', '0000-00-00', 2, '2° SEM BACHILLERATO', 'COBAQ 6', '3° SEM', '-', 9, 9),
('BEC061', 34, 'MARÍA ISABEL', 'GONZÁLEZ', 'GONZÁLEZ', 'GOGI020118MQTNNSA3', '0000-00-00', '-', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'MESA DE RAMIREZ', '0000-00-00', 1, 'SECUNDARIA TERMINADA', 'TELEBACHILLERATO COMUNITARIO', '1° SEM', '-', 6, 6),
('BEC062', 35, 'MARÍA ADELIA', 'GUDIÑO ', 'SÁNCHEZ', 'GUSA011025MQTDNDB8', '0000-00-00', 'mariags1025@a.cobaq.edu.mx', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'MESA DE RAMIREZ', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8, 8),
('BEC063', 36, 'MIGUEL ÁNGEL', 'MORALES', 'GUDIÑO', 'MOGM000423HQTRDGA7', '0000-00-00', 'michaelmike8300959@gmail.com', 'Regular', 'Masculino', 'TOLIMAN', 'MESA DE CHAGOYA', '0000-00-00', 1, 'SECUNDARIA TERMINADA', 'EMSAD 24', '1° SEM', '-', 8, 8),
('BEC064', 37, 'CRISTIÁN', 'PÉREZ', 'LOYOLA ', 'PELC980217HQTRYR04', '0000-00-00', 'crpsloyola450@gmail.com', 'Regular', 'Masculino', 'TOLIMAN', 'MAGUEY MANSO', '0000-00-00', 2, 'BACHILLERATO TERMINADO', 'UTQ-QUERÉTARO', '1° SEM', 'MECATRÓNICA', 8, 8),
('BEC065', 38, 'ANA LAURA', 'RESÉNDIZ', 'GUDIÑO', 'REGA020909MQTSDNA5', '0000-00-00', 'lauraresendiz230@gmail.com', 'Regular', 'Femenino', 'CADEREYTA DE MONTES', 'MESA DE RAMIREZ', '0000-00-00', 1, 'SECUNDARIA TERMINADA', 'EMSAD 24', '1° SEM', '-', 7, 7),
('BEC066', 39, 'EFRAÍN', 'RESÉNDIZ', 'HERNÁNDEZ', 'REHE980313HQTSRF09', '0000-00-00', '', 'Regular', 'Masculino', 'TOLIMAN', 'CERRITO PARADO', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8, 8),
('BEC067', 40, 'ROCÍO', 'RESÉNDIZ', 'PÉREZ', 'REPR021215MQTSRCA9', '0000-00-00', '', 'Regular', 'Femenino', 'TOLIMAN', 'SABINO DE SAN AMBROSIO', '0000-00-00', 2, 'SECUNDARIA TERMINADA', 'EMSAD 24', '1° SEM', '-', 7, 7),
('BEC068', 41, 'JESÚS NAZARIO', 'RESÉNDIZ', 'DE SANTIAGO ', 'RESJ001224HQTSNSA0', '0000-00-00', 'jesusrs1224@a.cobaq.edu.mx', 'Regular', 'Masculino', 'TOLIMAN', 'CERRITO PARADO', '0000-00-00', 2, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8, 8),
('BEC069', 42, 'ROSA ELVIA', 'TREJO', 'HERNÁNDEZ', 'TEHR000618MQTRRSA8', '0000-00-00', 'rosath0618@a.cobaq.edu.mx', 'Regular', 'Femenino', 'TOLIMAN', 'CERRITO PARADO', '0000-00-00', 1, '2° SEM BACHILLERATO', 'EMSAD 24', '3° SEM', '-', 8, 8);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `becario`
--
ALTER TABLE `becario`
  ADD PRIMARY KEY (`IdBecario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

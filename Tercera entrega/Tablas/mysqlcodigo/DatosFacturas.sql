-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-10-2017 a las 00:38:26
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vertice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DatosFacturas`
--

CREATE TABLE `datosfacturas` (
  `IdFacturas` varchar(8) NOT NULL,
  `RazonSocial` text,
  `RFC` varchar(15) DEFAULT NULL,
  `Telefono` bigint(15) DEFAULT NULL,
  `Email` varchar(40) DEFAULT NULL,
  `Calle` varchar(40) DEFAULT NULL,
  `NumeroExterior` int(11) DEFAULT NULL,
  `NumeroInterior` int(11) DEFAULT NULL,
  `Colonia` int(11) DEFAULT NULL,
  `CodigoPostal` varchar(50) DEFAULT NULL,
  `Pais` text,
  `Estado` text,
  `Municipio` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `DatosFacturas`
--

INSERT INTO `DatosFacturas` (`IdFacturas`, `RazonSocial`, `RFC`, `Telefono`, `Email`, `Calle`, `NumeroExterior`, `NumeroInterior`, `Colonia`, `CodigoPostal`, `Pais`, `Estado`, `Municipio`) VALUES
('FACT0002', 'El Glogo SA de CV', 'CUPU800825569', 4422019432, 'glogo@hotmail.com', 'Ignacio Ramirez', 1, 7, 0, '31120', 'Mexico', 'Guanajuato', 'Jaral del Progreso'),
('FACT0004', 'COCA COLA COMPANY', 'GUCH6411244W0 ', 4111174290, 'cocacola@hotmail.com', 'Ignacio Aldama', 4, 3, 0, '75030', 'Mexico', 'Queretaro', 'San Juan del Rio'),
('FACT0006', 'Alfarrico S.A.', 'DIHM711110PC7 ', 4113032440, 'alfarico@hotmail.com', 'Jesus Oviedo Avendaño', 5, 4, 0, '76000', 'Mexico', 'Guanajuato', 'Cortazar'),
('FACT0008', 'BIMBO', 'MAPA780604123 ', 4613943447, 'bimbo@hotmail.com', 'Miguel Hidalgo', 4, 6, 0, '76010', 'Mexico', 'Queretaro', 'Peñamiller'),
('FACT0010', 'Hermanos Gómez y Ripoldi S.R.L.', 'AAGR740904JM1 ', 4611322331, 'hermanogr@hotmail.com', 'Benito Juarez', 10, 5, 0, '76020', 'Mexico', 'Queretaro', 'San Joaquin'),
('FACT0012', 'PRICEWATERHOUSE AND COUPERS', 'CACX051080LF1', 4611233420, 'price@hotmail.com', 'Emiliano Zapata', 23, 9, 0, '76027', 'Mexico', 'Guanajuato', 'Salvatierra'),
('FACT0014', 'La Guitarra S.A', 'BAFJ070401AZ2', 4422179421, 'lag@hotmail.com', 'Lazaro Cardenas', 12, 2, 0, '76028', 'Mexico', 'Guanajuato', 'Celaya'),
('FACT0016', 'UNIVERSIDAD NACIONAL AUTONOMA DE MEXICO ', 'OIPF290225PAI', 4112010834, 'unam@hotmail.com', 'Francisco I. Madero', 2, 67, 0, '76029', 'Mexico', 'Queretaro', 'Jalpan de Serra'),
('FACT0018', 'Sol Dorado', 'CAGM240618', 4426930132, 'sold@hotmail.com', 'Miguel Miramon', 9, 8, 0, '76030', 'Mexico', 'Guanajuato', 'Villagran'),
('FACT0020', 'El Grupo Ramos', 'CALF450228', 4112032412, 'gramos@hotmail.com', 'Juventino Rosas', 18, 10, 0, '76046', 'Mexico', 'Queretaro', 'Pinal de Amoles');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `DatosFacturas`
--
ALTER TABLE `DatosFacturas`
  ADD PRIMARY KEY (`IdFacturas`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

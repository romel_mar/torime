﻿-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-10-2017 a las 01:51:47
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vertice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--



CREATE TABLE `noticias` (
  `IdNoticias` varchar(6) NOT NULL,
  `Nombre` text,
  `Descripcion` longtext,
  `Fecha` date DEFAULT NULL,
  `Tag` text,
  `Foto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`IdNoticias`, `Nombre`, `Descripcion`, `Fecha`, `Tag`, `Foto`) VALUES
('NOT001', 'Terremoto', 'Hoy surfrimos un grave accidente en mexico', '2017-12-12', 'Politica', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT002', 'Concierto', 'Hoy fuimso a concierto a pasarla bien ', '2017-12-13', 'Entretenimiento', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT003', 'Acopio', 'Puedes llevar aquí en los centros de acopio', '2017-12-14', 'Sociedad ', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT004', 'Vertice', 'Vertice esta recolectando viveres', '2017-12-15', 'Sociedad ', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT005', 'Tec', 'Estamos solicitando personas para que vengan', '2017-12-16', 'Sociedad ', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT006', 'Lluvias', 'Hay lluvias en todo queretaro', '2017-12-17', 'Clima', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT007', 'Ayuda', 'Hay muchos becarios que necesitan tu ayuda', '2017-12-18', 'Sociedad ', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT008', 'Lluvias', 'Lluvias muy fuertes', '2017-12-19', 'Clima', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT009', 'México y tu', 'Chequen estos articulos', '2017-12-20', 'Politica', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT010', 'Agua limpia', 'Miren este articulo sobre el agua', '2017-12-21', 'Cultura', 'C:UsersRomel MartinezMusicBases de datosIMGecado'),
('NOT020', 'México gana', 'México ganó el mundial', '2017-12-22', 'Deportes', 'C:UsersRomel MartinezMusicBases de datosIMGecado');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`IdNoticias`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

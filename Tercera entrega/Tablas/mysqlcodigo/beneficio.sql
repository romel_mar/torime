﻿-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-10-2017 a las 05:40:45
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vertice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `beneficio`
--





CREATE TABLE `beneficio` (
  `IdBecario` varchar(6) NOT NULL,
  `IdBeneficio` varchar(6) NOT NULL,
  `NomBeneficio` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `beneficio`
--

INSERT INTO `beneficio` (`IdBecario`, `IdBeneficio`, `NomBeneficio`) VALUES
('BEC001', 'BEN001', 'PROSPERA'),
('BEC002', 'BEN002', 'PROSPERA'),
('BEC003', 'BEN003', 'PROSPERA'),
('BEC004', 'BEN004', 'PROSPERA'),
('BEC005', 'BEN005', ''),
('BEC006', 'BEN006', 'PROSPERA Y TSUNI'),
('BEC007', 'BEN007', 'PROSPERA'),
('BEC008', 'BEN008', 'PROSPERA'),
('BEC009', 'BEN009', 'PROSPERA'),
('BEC010', 'BEN010', ''),
('BEC020', 'BEN020', 'PROSPERA'),
('BEC021', 'BEN021', 'PROSPERA'),
('BEC022', 'BEN022', 'PROSPERA'),
('BEC023', 'BEN023', 'TSUNI'),
('BEC024', 'BEN024', 'PROSPERA'),
('BEC025', 'BEN025', 'PROSPERA'),
('BEC026', 'BEN026', 'PROSPERA'),
('BEC027', 'BEN027', 'PAL'),
('BEC028', 'BEN028', 'PAL '),
('BEC029', 'BEN029', 'PAL Y TSUNI'),
('BEC030', 'BEN030', 'PAL '),
('BEC040', 'BEN040', ''),
('BEC041', 'BEN041', 'PROSPERA'),
('BEC042', 'BEN042', 'PROSPERA'),
('BEC043', 'BEN043', 'PROSPERA'),
('BEC044', 'BEN044', 'PROSPERA'),
('BEC045', 'BEN045', 'PROSPERA Y PAL'),
('BEC046', 'BEN046', ''),
('BEC047', 'BEN047', 'PROSPERA'),
('BEC048', 'BEN048', 'PROSPERA'),
('BEC049', 'BEN049', 'PROSPERA'),
('BEC050', 'BEN050', 'PROSPERA'),
('BEC060', 'BEN060', 'PROSPERA'),
('BEC061', 'BEN061', ''),
('BEC062', 'BEN062', 'PROSPERA'),
('BEC063', 'BEN063', ''),
('BEC064', 'BEN064', 'PROSPERA'),
('BEC065', 'BEN065', ''),
('BEC066', 'BEN066', 'PROSPERA'),
('BEC067', 'BEN067', 'PROSPERA'),
('BEC068', 'BEN068', ''),
('BEC069', 'BEN069', 'PROSPERA');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `beneficio`
--
ALTER TABLE `beneficio`
  ADD PRIMARY KEY (`IdBeneficio`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `beneficio`
--
ALTER TABLE `beneficio`
  ADD CONSTRAINT `beneficio_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

﻿-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-10-2017 a las 05:43:14
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vertice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono`
--



CREATE TABLE `telefono` (
  `IdBecario` varchar(6) NOT NULL,
  `IdTelefono` varchar(6) NOT NULL,
  `Numero` bigint(20) NOT NULL,
  `Dueño` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefono`
--

INSERT INTO `telefono` (`IdBecario`, `IdTelefono`, `Numero`, `Dueño`) VALUES
('BEC001', 'TEL001', 4412657690, 'DELEGADA'),
('BEC002', 'TEL002', 4411224646, ''),
('BEC003', 'TEL003', 4411214508, ''),
('BEC004', 'TEL004', 4411008341, ''),
('BEC005', 'TEL005', 4411182454, ''),
('BEC006', 'TEL006', 4411229043, ''),
('BEC007', 'TEL007', 4424519672, ''),
('BEC008', 'TEL008', 4411063431, ''),
('BEC009', 'TEL009', 4411216130, ''),
('BEC010', 'TEL010', 4411223927, ''),
('BEC020', 'TEL020', 4411279824, ''),
('BEC021', 'TEL021', 4411015080, ''),
('BEC022', 'TEL022', 4411016389, ''),
('BEC023', 'TEL023', 4411278621, ''),
('BEC024', 'TEL024', 4411302955, ''),
('BEC025', 'TEL025', 4424473984, ''),
('BEC026', 'TEL026', 4412768358, 'PROPIO '),
('BEC027', 'TEL027', 4411233869, ''),
('BEC028', 'TEL028', 4425736714, ''),
('BEC029', 'TEL029', 4411078237, ''),
('BEC030', 'TEL030', 4421492508, ''),
('BEC040', 'TEL040', 4411233283, ''),
('BEC041', 'TEL041', 4426092076, ''),
('BEC042', 'TEL042', 4411019761, ''),
('BEC043', 'TEL043', 4411170634, 'PAPÁ'),
('BEC044', 'TEL044', 4425981455, ''),
('BEC045', 'TEL045', 4411036790, 'PROPIO'),
('BEC046', 'TEL046', 4423977854, ''),
('BEC047', 'TEL047', 4411012122, ''),
('BEC048', 'TEL048', 4411205919, ''),
('BEC049', 'TEL049', 4412653556, 'PROPIO'),
('BEC050', 'TEL050', 4411072073, 'PROPIO'),
('BEC060', 'TEL060', 4411221534, 'PROPIO'),
('BEC061', 'TEL061', 4411148512, 'PROPIO'),
('BEC062', 'TEL062', 4411359534, 'PROPIO'),
('BEC063', 'TEL063', 4411207748, 'PROPIO'),
('BEC064', 'TEL064', 4411364479, 'PROPIO'),
('BEC065', 'TEL065', 4412657625, 'PROPIO'),
('BEC066', 'TEL066', 8443596858, 'PROPIO'),
('BEC067', 'TEL067', 4425640931, 'MAMÁ'),
('BEC068', 'TEL068', 4411356293, 'PROPIO'),
('BEC069', 'TEL069', 4425034332, 'PROPIO'),
('BEC026', 'TEL070', 4411143807, 'MAMÁ'),
('BEC045', 'TEL071', 4411000586, 'MAMÁ'),
('BEC062', 'TEL072', 4411051818, 'MAMÁ'),
('BEC063', 'TEL073', 4411184725, 'MAMÁ');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `telefono`
--
ALTER TABLE `telefono`
  ADD PRIMARY KEY (`IdTelefono`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `telefono`
--
ALTER TABLE `telefono`
  ADD CONSTRAINT `telefono_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

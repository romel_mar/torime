﻿-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-10-2017 a las 05:43:31
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vertice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tienebeneficio`
--



CREATE TABLE `tienebeneficio` (
  `IdBecario` varchar(6) NOT NULL,
  `IdBeneficio` varchar(6) NOT NULL,
  `Monto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tienebeneficio`
--

INSERT INTO `tienebeneficio` (`IdBecario`, `IdBeneficio`, `Monto`) VALUES
('BEC001', 'BEN001', 1795),
('BEC002', 'BEN002', 1485),
('BEC003', 'BEN003', 1450),
('BEC004', 'BEN004', 1500),
('BEC005', 'BEN005', 0),
('BEC006', 'BEN006', 2095),
('BEC007', 'BEN007', 1485),
('BEC008', 'BEN008', 1750),
('BEC009', 'BEN009', 1250),
('BEC010', 'BEN010', 0),
('BEC020', 'BEN020', 700),
('BEC021', 'BEN021', 650),
('BEC022', 'BEN022', 2270),
('BEC023', 'BEN023', 300),
('BEC024', 'BEN024', 1950),
('BEC025', 'BEN025', 2337),
('BEC026', 'BEN026', 1447),
('BEC027', 'BEN027', 860),
('BEC028', 'BEN028', 1400),
('BEC029', 'BEN029', 1100),
('BEC030', 'BEN030', 800),
('BEC040', 'BEN040', 0),
('BEC041', 'BEN041', 1540),
('BEC042', 'BEN042', 2120),
('BEC043', 'BEN043', 1235),
('BEC044', 'BEN044', 2262),
('BEC045', 'BEN045', 1120),
('BEC046', 'BEN046', 0),
('BEC047', 'BEN047', 1002),
('BEC048', 'BEN048', 475),
('BEC049', 'BEN049', 950),
('BEC050', 'BEN050', 1500),
('BEC060', 'BEN060', 1800),
('BEC061', 'BEN061', 0),
('BEC062', 'BEN062', 2200),
('BEC063', 'BEN063', 0),
('BEC064', 'BEN064', 1200),
('BEC065', 'BEN065', 0),
('BEC066', 'BEN066', 2300),
('BEC067', 'BEN067', 1850),
('BEC068', 'BEN068', 3200),
('BEC069', 'BEN069', 1800);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tienebeneficio`
--
ALTER TABLE `tienebeneficio`
  ADD KEY `IdBecario` (`IdBecario`),
  ADD KEY `IdBeneficio` (`IdBeneficio`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tienebeneficio`
--
ALTER TABLE `tienebeneficio`
  ADD CONSTRAINT `tienebeneficio_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tienebeneficio_ibfk_2` FOREIGN KEY (`IdBeneficio`) REFERENCES `beneficio` (`IdBeneficio`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

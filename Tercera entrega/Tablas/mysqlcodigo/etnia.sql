﻿-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-10-2017 a las 05:41:48
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vertice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etnia`
--




CREATE TABLE `etnia` (
  `IdBecario` varchar(6) NOT NULL,
  `IdEtnia` varchar(6) NOT NULL,
  `HablaLengua` text,
  `Etnia` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `etnia`
--

INSERT INTO `etnia` (`IdBecario`, `IdEtnia`, `HablaLengua`, `Etnia`) VALUES
('BEC001', 'ETN001', 'SÍ', 'OTOMI'),
('BEC002', 'ETN002', 'SÍ', 'OTOMI'),
('BEC003', 'ETN003', 'SÍ', 'OTOMI'),
('BEC004', 'ETN004', 'SÍ', 'OTOMI'),
('BEC005', 'ETN005', 'NO', ''),
('BEC006', 'ETN006', 'SÍ', 'OTOMI'),
('BEC007', 'ETN007', 'SÍ', 'OTOMI'),
('BEC008', 'ETN008', 'SÍ', 'OTOMI'),
('BEC009', 'ETN009', 'NO', ''),
('BEC010', 'ETN010', 'SÍ', 'OTOMI'),
('BEC020', 'ETN020', 'SÍ', 'OTOMI'),
('BEC021', 'ETN021', 'SÍ', 'OTOMI'),
('BEC022', 'ETN022', 'SÍ', 'OTOMI'),
('BEC023', 'ETN023', 'SÍ', 'OTOMI'),
('BEC024', 'ETN024', 'SÍ', 'OTOMI'),
('BEC025', 'ETN025', 'SÍ', 'OTOMI'),
('BEC026', 'ETN026', 'SÍ', 'OTOMI'),
('BEC027', 'ETN027', 'SÍ', 'OTOMI'),
('BEC028', 'ETN028', 'SÍ', 'OTOMI'),
('BEC029', 'ETN029', 'SÍ', 'OTOMI'),
('BEC030', 'ETN030', 'SÍ', 'OTOMI'),
('BEC040', 'ETN040', 'SÍ', 'OTOMI'),
('BEC041', 'ETN041', 'SÍ', 'OTOMI'),
('BEC042', 'ETN042', 'NO', ''),
('BEC043', 'ETN043', 'SÍ', 'OTOMI'),
('BEC044', 'ETN044', 'SÍ', 'OTOMI'),
('BEC045', 'ETN045', 'SÍ', 'OTOMI'),
('BEC046', 'ETN046', 'SÍ', 'OTOMI'),
('BEC047', 'ETN047', 'SÍ', 'OTOMI'),
('BEC048', 'ETN048', 'SÍ', 'OTOMI'),
('BEC049', 'ETN049', 'SÍ', 'OTOMI'),
('BEC050', 'ETN050', 'SÍ', 'OTOMI'),
('BEC060', 'ETN060', 'SÍ', 'OTOMI'),
('BEC061', 'ETN061', 'SÍ', 'OTOMI'),
('BEC062', 'ETN062', 'SÍ', 'OTOMI'),
('BEC063', 'ETN063', 'SÍ', 'OTOMI'),
('BEC064', 'ETN064', 'SÍ', 'OTOMI'),
('BEC065', 'ETN065', 'SÍ', 'OTOMI'),
('BEC066', 'ETN066', 'SÍ', 'OTOMI'),
('BEC067', 'ETN067', 'SÍ', 'OTOMI'),
('BEC068', 'ETN068', 'SÍ', 'OTOMI'),
('BEC069', 'ETN069', 'SÍ', 'OTOMI');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `etnia`
--
ALTER TABLE `etnia`
  ADD PRIMARY KEY (`IdEtnia`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `etnia`
--
ALTER TABLE `etnia`
  ADD CONSTRAINT `etnia_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

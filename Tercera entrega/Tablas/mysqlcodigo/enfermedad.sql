﻿-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-10-2017 a las 05:41:23
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vertice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enfermedad`
--



CREATE TABLE `enfermedad` (
  `IdBecario` varchar(6) NOT NULL,
  `IdEnfermedad` varchar(6) NOT NULL,
  `HayEnfermedad` text,
  `FamiliaEnfermo` text,
  `Descripcion` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `enfermedad`
--

INSERT INTO `enfermedad` (`IdBecario`, `IdEnfermedad`, `HayEnfermedad`, `FamiliaEnfermo`, `Descripcion`) VALUES
('BEC001', 'ENF001', 'NO', '', ''),
('BEC002', 'ENF002', 'NO', '', ''),
('BEC003', 'ENF003', 'NO', '', ''),
('BEC004', 'ENF004', 'NO', '', ''),
('BEC005', 'ENF005', '', '', ''),
('BEC006', 'ENF006', 'NO', '', ''),
('BEC007', 'ENF007', 'NO', '', ''),
('BEC008', 'ENF008', 'NO', '', ''),
('BEC009', 'ENF009', 'NO', '', ''),
('BEC010', 'ENF010', 'SÍ', 'BENEFICIARIA', 'AUDITIVA Y DEL HABLA'),
('BEC020', 'ENF020', 'NO', '', ''),
('BEC021', 'ENF021', 'NO', '', ''),
('BEC022', 'ENF022', 'SÍ', 'ABUELO', 'AUDITIVO'),
('BEC023', 'ENF023', 'SÍ', 'HERMANO MENOR', 'DEFORMACIÓN DEL OÍDO '),
('BEC024', 'ENF024', 'NO', '', ''),
('BEC025', 'ENF025', 'SÍ', 'BENEFICIARIA', 'ASMA'),
('BEC026', 'ENF026', 'NO', '', ''),
('BEC027', 'ENF027', 'SÍ', 'MADRE Y PADRE', 'DIABETES'),
('BEC028', 'ENF028', 'NO', '', ''),
('BEC029', 'ENF029', 'SÍ', 'PADRE', 'POSIBLE GASTRITIS'),
('BEC030', 'ENF030', 'SÍ', 'ABUELA', 'DIABETES'),
('BEC040', 'ENF040', 'NO', '', ''),
('BEC041', 'ENF041', 'NO', '', ''),
('BEC042', 'ENF042', 'NO', '', ''),
('BEC043', 'ENF043', 'SÍ', 'PADRE', 'DOLOR MUSCULAR'),
('BEC044', 'ENF044', 'SÍ', 'MADRE Y BENEFICIARIO', 'ASMA'),
('BEC045', 'ENF045', 'NO', '', ''),
('BEC046', 'ENF046', 'NO', '', ''),
('BEC047', 'ENF047', '', '', ''),
('BEC048', 'ENF048', 'SÍ', 'BENEFICIARIO', 'VITILIGIO'),
('BEC049', 'ENF049', 'NO', '', ''),
('BEC050', 'ENF050', 'SÍ', 'ABUELA', 'DIABETES'),
('BEC060', 'ENF060', 'NO', '', ''),
('BEC061', 'ENF061', 'SÍ', 'TÍO', 'DISCAPACIDAD INTELECTUAL'),
('BEC062', 'ENF062', 'SÍ', 'PAPÁ', 'DIABETES'),
('BEC063', 'ENF063', 'NO', '', ''),
('BEC064', 'ENF064', 'NO', '', ''),
('BEC065', 'ENF065', 'NO', '', ''),
('BEC066', 'ENF066', 'NO', '', ''),
('BEC067', 'ENF067', 'NO', '', ''),
('BEC068', 'ENF068', 'NO', '', ''),
('BEC069', 'ENF069', 'SÍ', 'PAPÁ', 'FRACTURA DE MANO, NO PUEDE LEVANTAR COSAS PESADAS');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `enfermedad`
--
ALTER TABLE `enfermedad`
  ADD PRIMARY KEY (`IdEnfermedad`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `enfermedad`
--
ALTER TABLE `enfermedad`
  ADD CONSTRAINT `enfermedad_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

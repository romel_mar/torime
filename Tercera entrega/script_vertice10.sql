CREATE TABLE `privilegios` (
  `IdPermiso` int NOT NULL,
  `Permiso` varchar(25),
  `Descripcion` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `Privilegios`
--

INSERT INTO `Privilegios` (`IdPermiso`, `Permiso`, `Descripcion`) VALUES
(1, 'Subir Informe', 'El usuario con este privilegio sera capaz de Subir informes'),
(2, 'Editar Informe', 'El usuario con este privilegio sera capaz de Editar Informes'),
(3, 'Eliminar Informe', 'El usuario con este privilegio sera capaz de Elminar Informes'),
(4, 'Registrar Colaborador', 'El usuario con este privilegio sera capaz de Registrar Colaboradores'),
(5, 'Editar Colaborador', 'El usuario con este privilegio sera capaz de Editar informacion de Colaboradores'),
(6, 'Eliminar Colaborador', 'El usuario con este privilegio sera capaz de Eliminar Colaboradores'),
(7, 'Registrar Boletin', 'El usuario con este privilegio sera capaz de Registrar Boletines'),
(8, 'Editar Boletin', 'El usuario con este privilegio sera capaz de Editar Boletines'),
(9, 'Eliminar Boletin', 'El usuario con este privilegio sera capaz de Eliminar Boletines'),
(10, 'Registrar Noticia', 'El usuario con este privilegio sera capaz de Registrar nuevas noticias'),
(11, 'Editar Noticia', 'El usuario con este privilegio sera capaz de Editar informacion de Noticias'),
(12, 'Eliminar Noticia', 'El usuario con este privilegio sera capaz de Eliminar Noticias'),
(13, 'Registrar Actividad', 'El usuario con este privilegio sera capaz de Registrar Actividades'),
(14, 'Eliminar Actividad', 'El usuario con este privilegio sera capaz de Eliminar Actividades'),
(15, 'Editar Actividad', 'El usuario con este privilegio sera capaz de Editar Actividades'),
(16, 'Generar ReporteA', 'El usuario con este privilegio sera capaz de generar Reportes de las Actividades'),
(17, 'Generar ReporteD', 'El usuario con este privilegio sera capaz de generar Reportes de las Donaciones'),
(18, 'Generar ReporteB', 'El usuario con este privilegio sera capaz de generar Reportes de los Becarios'),
(19, 'Registrar Benefactor', 'El usuario con este privilegio sera capaz de Registrar nuevos Benefactores'),
(20, 'Editar Benefactor', 'El usuario con este privilegio sera capaz de Editar informacion de los Benefactores'),
(21, 'Eliminar Benefactor', 'El usuario con este privilegio sera capaz de Eliminar Benefactores'),
(22, 'RegistrarEvento', 'El usuario con este privilegio sera capaz de Registrar nuevos Eventos'),
(23, 'Editar Evento', 'El usuario con este privilegio sera capaz de Editar la informacion de los Eventos'),
(24, 'Eliminar Evento', 'El usuario con este privilegio sera capaz de Eliminar Eventos');


--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Privilegios`
--
ALTER TABLE `Privilegios`
  ADD PRIMARY KEY (`IdPermiso`);


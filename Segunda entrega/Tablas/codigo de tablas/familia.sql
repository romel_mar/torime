﻿-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-10-2017 a las 05:42:40
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vertice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familia`
--



CREATE TABLE `familia` (
  `IdBecario` varchar(6) NOT NULL,
  `IdFamilia` varchar(6) NOT NULL,
  `IngresoMin` int(11) DEFAULT NULL,
  `IngresoMax` int(11) DEFAULT NULL,
  `Mama` int(11) DEFAULT NULL,
  `Papa` int(11) DEFAULT NULL,
  `Hermanos` int(11) DEFAULT NULL,
  `Hijos` int(11) DEFAULT NULL,
  `Abuela` int(11) DEFAULT NULL,
  `Abuelo` int(11) DEFAULT NULL,
  `Otros` int(11) DEFAULT NULL,
  `Descripcion` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `familia`
--

INSERT INTO `familia` (`IdBecario`, `IdFamilia`, `IngresoMin`, `IngresoMax`, `Mama`, `Papa`, `Hermanos`, `Hijos`, `Abuela`, `Abuelo`, `Otros`, `Descripcion`) VALUES
('BEC001', 'FAM001', 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'Originaria de El Saucito, Tolimán, su familia es de bajos recursos económicos, su papá está lastimado de una pierna y por lo mismo no puede trabajar regularmente, ella necesita la beca para seguir estudiando y en un futuro entrar al bachillerato.'),
('BEC002', 'FAM002', 1, 1500, 1, 1, 4, 0, 0, 0, 0, 'Ha sido difícil mantenerse en la escuela ya que su papá no tiene un trabajo fijo y no puede darle el apoyo económico que requiere para su escuela. Su casa está construida de block y lámina únicamente y su familia es numerosa. '),
('BEC003', 'FAM003', 1, 1500, 1, 1, 1, 0, 0, 0, 0, 'Nancy es una chica de 16 años que es apasionada por la lectura, le gusta mucho la ciencia ficción y demuestra mucho interés por continuar estudiando, lleva un buen promedio en la escuela, 9.2, ella utilizaría la beca para cubrir sus gastos escolares, por ejemplo el transporte ya que menciona que hace 55 minutos de su casa a la escuela.'),
('BEC004', 'FAM004', 1, 1500, 1, 1, 2, 0, 1, 1, 0, 'Vive en la comunidad de Maguey Manso, Tolimán, su papá murió cuando tenía 4 años de edad, desde entonces su mamá ha tenido que cubrir los gastos de la casa, Daniel tuvo que abandonar sus estudios para venir a trabajar a la ciudad de Querétaro, actualmente los retomó y está cursando el 3er semestre de bachillerato, le gustaría estudiar Ingeniería Civil ya que le apasiona el tema de la construcción.'),
('BEC005', 'FAM005', 1, 1500, 0, 0, 0, 0, 0, 0, 0, 'José Raúl es de la comunidad Mesa de Chagoya, Tolimán, durante su tiempo libre trabaja en las tierras de la familia para apoyar a su papá, el único proveedor es su papá y gana muy poco ya que es jornalero, camina diariamente una hora para poder llegar a su escuela, ocuparía la beca para los gastos de la familia y para sus estudios.'),
('BEC006', 'FAM006', 3000, 4500, 1, 1, 8, 0, 0, 0, 0, 'Bibiana es la hija mayor y  tiene 8 hermanos, tiene 17 años y está estudiando el último año del bachillerato, tiene un promedio de 9, le gustaría estudiar medicina, por lo mismo que son 11 integrantes de la familia lo que tienen no les alcanza, su papá trabaja de jornalero y depende si hay siembra para trabajar, su mamá sale a trabajar a hacer limpieza y vende productos de catálogo. '),
('BEC007', 'FAM007', 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'En su tiempo libre trabaja de jornalero ya que no alcanza el gasto en su familia, su mamá hace limpieza en casas y su papá no puede trabajar mucho ya que está impedido, a Víctor le gusta mucho estudiar, su materia favorita son las matemáticas y en un futuro le gustaría ser ingeniero.'),
('BEC008', 'FAM008', 3000, 4500, 1, 1, 3, 0, 0, 0, 0, 'Su materias preferida es historia porque le gusta aprender sobre su cultura y su lengua, a ella le gustaría estudiar Derecho porque quiere ayudar a su comunidad pero continuar estudiando ha sido difícil ya que no cuenta con los recursos necesarios para cubrir sus gastos escolares, incluso tiene que caminar una hora a la escuela pues no puede pagar el transporte que la acerca a la escuela. '),
('BEC009', 'FAM009', 1, 1500, 1, 0, 1, 0, 0, 0, 0, 'Vive en condiciones adversas puesto su padre las abandonó desde hace tiempo y ahora tiene que apoyar a su mamá y a su hermana para que las 3 puedan salir adelante.'),
('BEC010', 'FAM010', 4500, 20000, 0, 0, 0, 0, 0, 0, 0, 'Vive en Mesa de Ramírez, Tolimán, ella tiene una condición de discapacidad que le impide escuchar y hablar bien, su mamá lee poco y no sabe escribir muy bien, su papá trabaja en la ciudad de Querétaro pero apenas les alcanza para solventar sus gastos más básicos.'),
('BEC020', 'FAM020', 3000, 4500, 1, 1, 3, 0, 0, 0, 0, 'Melissa es originaria de una comunidad indígena de Tolimán, habla el otomí igual que su familia, le gusta mucho ir a la escuela, actualmente cursa el 5to semestre del bachillerato, le gusta aprender idiomas, sus papás le apoyan a continuar sus estudios, ella quiere estudiar la universidad.'),
('BEC021', 'FAM021', 1, 1500, 1, 1, 1, 0, 0, 0, 0, 'Sofía siempre ha sido una chica muy estudiosa y con buenas calificaciones, sus padres están orgullosos de ella y sus maestros manifiestan que tiene gran potencial. Sin embargo, no cuenta con los recursos suficientes para continuar sus estudios, ni recibe el apoyo económico de nadie para solventar los gastos que ello conlleva, por lo que trabaja de empleada doméstica para pagar sus estudios. '),
('BEC022', 'FAM022', 3000, 4500, 1, 1, 5, 0, 0, 1, 1, 'En su casa viven 10 personas: sus papás, sus hermanos, una tía, su abuelo y ella. Sus papás la animan a seguir estudiando aunque no tienen los recursos suficientes para apoyarla económicamente. De hecho, Daniela es una inspiración para su familia, ya que sus ganas de continuar la escuela y el esfuerzo que realiza para mantener sus buenas calificaciones motivan a sus papás y a sus hermanos para salir adelante y mejorar en lo que hacen día con día.'),
('BEC023', 'FAM023', 3000, 4500, 1, 1, 2, 0, 0, 0, 1, 'Le echa muchas ganas a la escuela y menciona que le gustaría estudiar Ingeniería electrónica, proviene de una familia indígena, su hermano pequeño nació con una deformación en el oído lo que ha orillado a su familia a tener que hacer gastos médicos importantes, su tía que es una persona de 3era edad vive con ellos en casa y su papá es el único proveedor. Está embarazada y tiene el apoyo de su familia para seguir estudiando'),
('BEC024', 'FAM024', 1, 1500, 1, 1, 3, 1, 0, 0, 1, 'Vive en la comunidad de El Saucito, en Tolimán junto con sus papás y sus 3 hermanos. Ella es madre soltera y estudia el 3er semestre de bachillerato y tiene 16 años, le gustan las matemáticas y le encantaría estudiar Derecho si pudiera estudiar la universidad, pero en ocasiones sus papás no pueden pagar los gastos de ella y sus hermanos.'),
('BEC025', 'FAM025', 1, 1500, 1, 1, 4, 0, 1, 1, 0, 'Para llegar a su escuela tiene que caminar una hora de ida y otra de regreso ya que no le alcanza para el transporte, ella padece asma la cual está controlada por el médico.'),
('BEC026', 'FAM026', 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'Es originario de la comunidad Mesa de Chagoya, Tolimán, tiene 16 años y estudia en el Telebachillerato comunitario de Mesa de Ramírez, para llegar a su escuela tiene que caminar media hora, habla Otomí, su casa es muy precaria, está hecha de piedra y láminas, refiere que no les alcanza para los gastos básicos, Luis Alberto es muy tímido, él se compromete a poner su mayor esfuerzo para continuar estudiando.'),
('BEC027', 'FAM027', 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'Vive en la comunidad de Sabino de San Ambrosio, en Tolimán, tiene 17 años y le falta un año para concluir el bachillerato. A ella le gusta mucho la escuela pero no cuenta con los recursos suficientes para costear los gastos escolares ya que su papá y su mamá se encuentran enfermos y no pueden trabajar, por lo que ella trabaja en vacaciones para juntar los recursos necesarios para pagar su escuela. A Aurelia le gustaría estudiar Medicina o Enfermería para ayudar a sus padres, tanto en los cuidados que requieren como económicamente.'),
('BEC028', 'FAM028', 1, 1500, 0, 0, 0, 0, 0, 0, 0, 'Ella camina a su escuela diariamente, en total son 8 personas las que viven en una casa con dos cuartos y una cocina, es un poco tímida, tiene muchas ganas de seguir estudiando como su hermana que está en el bachillerato.'),
('BEC029', 'FAM029', 1501, 3000, 1, 1, 5, 0, 0, 0, 0, 'Vive en la comunidad de Mesa de Ramírez, en Tolimán y le gustaría estudiar algún día Ingeniería en Sistemas pero sus padres son de escasos recursos y les cuesta trabajo apoyar a ella y sus 5 hermanos en sus estudios. Además, su papá padece de una enfermedad delicada que le impide trabajar continuamente y es el único sustento de la familia. Sin embargo Brenda se encuentra muy comprometida con su comunidad y realiza actividades de recolección de pet, aseo de calles y ayuda a personas a que aprendan a leer y a escribir en el INEA. '),
('BEC030', 'FAM030', 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'A ella le gustan las ciencias de la salud y le encantaría estudiar más adelante la Licenciatura en Criminología. Sin embargo, no cuenta con los recursos necesarios puesto que su papá se encuentra desempleado y es difícil para su familia solventar los gastos escolares de ella y de sus 2 hermanos. '),
('BEC040', 'FAM040', 1, 1500, 1, 1, 1, 0, 0, 0, 0, 'Ella es originaria de la comunidad Mesa de Chagoya, tiene 21 años, habla otomí, estudia la Ingeniería en Gestión Empresarial en el ITQ, va muy bien en la escuela, su familia es de escasos recursos económicos por lo que no les alcanza para su transporte y sus comidas, su casa está en obra negra, menciona que cuando tiene que ir a la escuela a veces se queda con hambre y se espera a llegar a su casa por la tarde para poder comer lo que haya.'),
('BEC041', 'FAM041', 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'Quiere ayudar a su comunidad y defender sus derechos. Entre semana trabaja como empleada doméstica en Tolimán para cubrir sus gastos escolares pues sus padres no pueden apoyarla económicamente porque también apoyan a sus hermanitos.'),
('BEC042', 'FAM042', 1501, 3000, 1, 0, 2, 0, 0, 0, 0, 'Es de la comunidad de San Pedro de los Eucaliptos, en Tolimán y vive con su madre y sus dos hermanos únicamente puesto que su padre falleció. Su hermano mayor tuvo que abandonar la escuela para apoyar a su familia y junto su mamá ha sacado a su familia adelante. Él quiere estudiar para mejorar sus condiciones y dar una mejor vida a su familia. '),
('BEC043', 'FAM043', 4500, 20000, 0, 0, 0, 0, 0, 0, 0, 'Estudia y trabaja, para ir al bachillerato toma un camión y hace 30 minutos de traslado, a veces se va caminando y hace una hora, su papá de Dianeli tuvo un accidente y eso le impide trabajar, era jornalero, ahora la han pasado muy difícil pues el dinero que llega a la casa no alcanza, su hermano es quien trabaja, y ella también lo ha tenido que hacer.'),
('BEC044', 'FAM044', 1501, 3000, 1, 1, 5, 0, 0, 0, 0, 'La escuela lo ha motivado mucho a superarse y mejorar sus condiciones de vida, ya que su mamá y él padecen de asma, su papá es albañil y al tener 5 hermanos más, sus recursos son muy escasos y el mismo tiene que solventar sus gastos escolares trabajando en el campo los fines de semana. '),
('BEC045', 'FAM045', 1, 1500, 1, 1, 3, 0, 1, 0, 0, 'A Luis le interesa seguir estudiando y mejorar su promedio, pero debido a que su familia es numerosa y sus padres no cuentan con recursos suficientes en vacaciones él va a trabajar como jornalero para juntar el dinero que necesita y cubrir sus gastos de inscripciones y útiles escolares, y que así sus papás puedan apoyar a sus otros hermanos. '),
('BEC046', 'FAM046', 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'Estudia el 1° semestre de la Ingeniería en Gestión Empresarial en el ITQ de Tolimán, y durante los fines de semana y vacaciones él sale a trabajar como jornalero o chalan de albañil, va muy bien en la escuela.'),
('BEC047', 'FAM047', 1, 1500, 0, 0, 0, 0, 0, 0, 0, 'Su materia favorita es biología y en un futuro le gustaría estudiar gastronomía, ella no tiene el apoyo de su papá para seguir estudiando, únicamente es su mamá quien la respalda, sin embargo tiene muchas ganas de estudiar y salir adelante, ella trabaja como jornalera, su casa tiene 2 cuartos, para llegar a su escuela tiene que caminar una hora. Necesita mucho el apoyo.'),
('BEC048', 'FAM048', 1501, 3000, 1, 1, 3, 0, 0, 0, 0, 'Marcos estudia Ingeniería en Sistemas Computacionales, a él le gusta el tema de la programación, se ha dedicado a buscar apoyos ya que en un futuro le gustaría establecer un negocio propio, actualmente ha tenido muchas dificultades para continuar estudiando ya que su familia es de escasos recursos económicos, su papá es jornalero y gana muy poco, Marcos tiene un problema en la piel (vitíligo) lo que le impide trabajar en los empleos que normalmente ofertan ya que son bajo el sol y esto le afecta en su salud, aun así ha trabajado en el mantenimiento de las carreteras a causa de la necesidad.'),
('BEC049', 'FAM049', 3000, 4500, 0, 1, 1, 0, 0, 0, 3, 'En su familia, la encargada de hacer la comida es su hermana, su madre falleció. Beatriz se encarga de la limpieza del hogar, su padre y su cuñado aportan al aseo de la casa también. Tiene dos sobrinos que los siente como hermanos, ellos son pequeños y no aportan mucho al aseo. Ella considera que su cuñado es como su segundo padre pues la apoya para seguir estudiando. Ella es tía de Elizabeth Blas, FAMeficiaria del programa, y por eso se enteró de la beca.'),
('BEC050', 'FAM050', 1, 1500, 1, 1, 2, 0, 0, 0, 0, 'Su hermana menor falleció de un problema del corazón. Actualmente él le ayuda a su mamá a las labores del hogar cuando está en la escuela y cuando no, él trabaja de chalán de albañil o en el campo.'),
('BEC060', 'FAM060', 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'En su familia se llevan muy bien, hay respeto y convivencia. Sus materias favoritas son la lectura y redacción y la educación física, le gustaría estudiar pediatría o ginecología. Comparten casa con su abuela paterna; ellos viven en dos cuartos'),
('BEC061', 'FAM061', 1501, 3000, 1, 1, 1, 0, 1, 0, 1, 'Le interesa la repostería. Su tío tiene discapacidad intelectual y vive con la familia de ella. Le gustan las materias de español, química y formación cívica y ética.'),
('BEC062', 'FAM062', 1, 1500, 0, 0, 0, 0, 1, 1, 2, 'Ella se refiere a sus abuelos como sus papás, su madre la abandonó y vive en otra comunidad, no sabe quién es su padre. Su abuelo es el ínico que aporta al gasto de la familia. Ella quiere estudiar corte y confección porque le gusta la idea de crear y hacer cosas nuevas, combinar colores y texturas.'),
('BEC063', 'FAM063', 3000, 4500, 0, 0, 0, 0, 0, 0, 0, 'Dejó de estudiar 2 añospara ayudar económicamente a su familia pero tuvo un accidente laboral, se fracturó la férula, así que hoy su familia se une para que él pueda retomar sus estudios. En verdad le gusta mucho estudiar y está muy emocionado por volver a hacerlo. Quiere hacer un proyecto en los hospitales en el que pueda motivas a las personas que se encuentran ahí a causa de algún accidente.'),
('BEC064', 'FAM064', 1501, 3000, 0, 0, 0, 0, 0, 0, 0, 'A él le apasiona todo lo relacionado con los aparatos electrónicos y la automatización. Su papá y su hermano son los únicos que aportan económicamente a su hogar, tiene mucha relción con su hermano y no hace referencia a otros familiares. Él vivirpa en Querétaro para hacer su carrera. Por ahora tiene un empleo que se relaciona un poco con su carrera.'),
('BEC065', 'FAM065', 1, 1500, 1, 0, 4, 0, 0, 0, 0, 'Quienes trabajan son sus hermanos mayores. Le gusta mucho la materia de matemáticas y química y le gustaría ser ingeniera porque le llama la atención poder construir cosas.'),
('BEC066', 'FAM066', 1, 1500, 1, 1, 3, 0, 0, 0, 0, 'Efraín dejó sus estudios en el 2014 por falta de recursos económicos y durante ese tiempo trabajó en la obra a apoyó a su familia. Su hermana ayor apoya a las labores del hogar y su hermano mayor trabaja auque no aporta diner, más bien él se cubre sus propios gastos. Le gustaría estudiar ingeniería civil.'),
('BEC067', 'FAM067', 1, 1500, 1, 1, 1, 0, 0, 0, 0, 'Le gustan las materias de españo, química e historia, quiere ser doctora para ayudar a las personas porque en el centro de salud no hay dotores con frecuencia .'),
('BEC068', 'FAM068', 1501, 3000, 0, 0, 0, 0, 0, 0, 0, 'Quiere estudiar ingeniería mecatrónica, está muy interesado en el funcionamiento de las máquinas ya que ha escuchado que algunos mexicanos han inventado han inventado varios robots que han hecho un gran avance en México, él quiere hacer un gran invento un día. Solamente aporta dinero su padre, y Jesús en vacaciones, es un chico motivado y trabajador con una familia cariñosa y que le brinda mucho apoyo.'),
('BEC069', 'FAM069', 1, 1500, 1, 1, 3, 0, 0, 0, 1, 'Su padre le ayuda con los alimentos y los útiles, su hermano aporta muy poco y sus hermanas viven aparte. Quiere ser abogada o estudiar ingeniería en sistemas. En su familia tienen problemas de despojos de terrenos. Su hermano mayor tiene problemas de alcoholismo. Elvia es creativa y soñadora.');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `familia`
--
ALTER TABLE `familia`
  ADD PRIMARY KEY (`IdFamilia`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `familia`
--
ALTER TABLE `familia`
  ADD CONSTRAINT `familia_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

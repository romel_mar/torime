﻿-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-10-2017 a las 01:52:03
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vertice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `donacion`
--



CREATE TABLE `donacion` (
  `IdDonacion` varchar(6) NOT NULL,
  `Monto` int(11) DEFAULT NULL,
  `Fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `donacion`
--

INSERT INTO `donacion` (`IdDonacion`, `Monto`, `Fecha`) VALUES
('DON001', 200, '2017-03-12'),
('DON002', 213, '2017-03-13'),
('DON003', 243, '2017-03-14'),
('DON004', 234, '2017-03-15'),
('DON005', 112, '2017-03-16'),
('DON006', 566, '2017-03-17'),
('DON007', 256, '2017-03-18'),
('DON008', 344, '2017-03-19'),
('DON009', 234, '2017-03-20'),
('DON010', 700, '2017-03-21'),
('DON020', 234, '2017-03-22'),
('DON021', 122, '2017-03-23'),
('DON022', 566, '2017-03-24'),
('DON023', 1233, '2017-03-25'),
('DON024', 233, '2017-03-26'),
('DON025', 34, '2017-03-27'),
('DON026', 56, '2017-03-28'),
('DON027', 255, '2017-03-29'),
('DON028', 233, '2017-03-30'),
('DON029', 122, '2017-03-31'),
('DON030', 7555, '2017-04-01');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `donacion`
--
ALTER TABLE `donacion`
  ADD PRIMARY KEY (`IdDonacion`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

﻿-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-10-2017 a las 02:12:03
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vertice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `benefactores`
--




CREATE TABLE `benefactores` (
  `IdBenefactores` varchar(6) NOT NULL,
  `Nombre` text NOT NULL,
  `ApellidoP` text,
  `ApellidoM` text,
  `RazonSocial` text,
  `FisicaOMoral` text,
  `FechaAlta` date DEFAULT NULL,
  `RFC` varchar(15) DEFAULT NULL,
  `Domicilio` varchar(50) DEFAULT NULL,
  `Municipio` text,
  `Estado` text,
  `Telefono` bigint(20) DEFAULT NULL,
  `EmailBenefactor` varchar(40) DEFAULT NULL,
  `EmailContacto` varchar(40) DEFAULT NULL,
  `RequiereFactura` text,
  `PeriodicidadAportacion` text,
  `Monto` int(11) DEFAULT NULL,
  `TiempoMembresia` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `benefactores`
--

INSERT INTO `benefactores` (`IdBenefactores`, `Nombre`, `ApellidoP`, `ApellidoM`, `RazonSocial`, `FisicaOMoral`, `FechaAlta`, `RFC`, `Domicilio`, `Municipio`, `Estado`, `Telefono`, `EmailBenefactor`, `EmailContacto`, `RequiereFactura`, `PeriodicidadAportacion`, `Monto`, `TiempoMembresia`) VALUES
('BFT001', 'María Alejandra', 'Daza', 'Covarrubias', 'Consorcio Invercop S.A. de C.V.', 'Moral', '2016-09-27', 'CIN080826CVA', 'Bosques Chapultepec 307 C Col Arboledas 1era Secci', 'Celaya', 'Guanajuato', 4421863957, 'alejandra.daza@alterra.com.mx ', 'laura.malagon@alterra.me', 'Si', 'Mensual', 1500, 'Emprendedor'),
('BFT002', 'Andres', 'Ortega', 'González', '', 'Fisica', '2016-08-29', 'OEGA49102092A', 'Carrillo 104 Col Villas del Mesón Juriquilla', 'Querétaro', 'Querétaro', 4422426762, 'aortega3322@live.com', 'aragomez.correo@gmail.com', 'Si', 'Mensual', 2000, 'Emprendedor'),
('BFT003', 'Moises', 'Miranda', 'Álvarez', 'Firma Sien S.C.', 'Moral', '2016-08-26', 'FSI140414NJ0', 'Bernardo Quintana 439-404 Col Centro Sur', 'Querétaro', 'Querétaro', 4424021010, 'mma@firmasien.mx', 'storres@firmasien.mx', 'Si', 'Mensual', 5000, 'Corporativo'),
('BFT004', 'Bernardo ', 'Castellanos', '', 'Comunicación Útil S.A. de C.V.', 'Moral', '2017-08-08', 'CUT0811141I5', 'Ret. Farrallón Mz. 2 Lote 8 12 SM 15A', 'Cancún', 'Quintana roo', 9982145938, 'bernardocut@gmail.com', 'castellanos.arturo@hotmail.com ', 'Si', 'Mensual', 1500, 'Emprendedor'),
('BFT005', 'Miguel Ángel', 'Feregrino', 'Feregrino', '', 'Fisica', '2017-01-09', '', '', 'Querétaro', 'Querétaro', 4422499806, 'feregrinoferegrino@yahoo.com.mx', 'feregrinoferegrino@yahoo.com.mx', 'No', 'Mensual', 5000, 'Corporativo'),
('BFT006', 'Jesús Alberto', 'Sánchez', 'Hernández', '', 'Fisica', '2016-08-22', ' SAHJ6311292M6', 'Manuel Herrera 20 Col San Javier', 'Querétaro', 'Querétaro', 4422421360, 'jesus.sanchez@alterra.com.mx', 'jesus.sanchez@alterra.com.mx', 'Si', 'Mensual', 2000, 'Emprendedor'),
('BFT007', 'Gerardo', 'Proal', 'de la Isla', '', 'Fisica', '2016-08-25', 'POIG600420EQ8', 'Blvd Gobernadores 1001- 14 Col MonteBlanco III ', 'Querétaro', 'Querétaro', 4423180380, 'gproal@codigoaureo.com', 'gproal@codigoaureo.com', 'Si', 'Semestral', 6000, 'Emprendedor'),
('BFT008', 'Mario Humberto', 'Paulin', 'Nardoni', 'PR Estudio Legal S.C.', 'Moral', '2017-10-01', 'PEL161123EX5', 'Allende Sur 27 Col Centro', 'Querétaro', 'Querétaro', 4422141476, 'mhp@paulinrivera.com', 'mng@paulinrivera.com', 'Si', 'Mensual', 2500, 'Corporativo'),
('BFT009', 'Arturo Ricardo', 'Olivares ', 'Hernández', 'Clic magnetic eyewear', 'Fisica', '2017-06-07', 'OIHA521005JJ1', 'Avellanas 30 Valle de Los Olivos ', 'Querétaro', 'Querétaro', 4424030430, 'ricardoolivares1@hotmail.com ', 'ricardoolivares1@hotmail.com', 'Si', 'Esporadica', 5000, 'Corporativo'),
('BFT010', 'Francisco Javier ', 'Sánchez', 'Hernández', 'Residencial Alterra I. S.A. de C.V', 'Fisica', '2016-07-07', 'SAHF650122NJ8', 'Río Ayutla 43 Col La Piedad', 'Querétaro', 'Querétaro', 4422421360, 'javier.sanchez@alterra.com.mx', 'juanjose.barrera@alterra.me', 'Si', 'Mensual', 32000, 'Corporativo'),
('BFT020', 'Jorge', 'Sánchez', 'Hernández', '', 'Fisica', '2017-10-01', '', '', 'Querétaro', 'Querétaro', 4424124192, '', ' jackecham@hotmail.com', 'Si', 'Trimestral', 1500, 'Junior'),
('BFT021', 'Omar', 'DeAlba', 'Garza', '', 'Moral', '2016-09-04', 'OIHA521005JJ2', 'Río Ayutla 43 Col La Piedad', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 2800, 'Junior'),
('BFT022', 'Juan', 'Aboytes', 'García', '', 'Fisica', '2017-06-05', 'OIHA521005JJ2', 'Avellanas 30 Valle de Los Olivos ', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 5000, 'Corporativo'),
('BFT023', 'David ', 'Lopez', 'Aboytes', '', 'Moral', '2017-10-09', 'OIHA521005JJ2', 'Avellanas 30 Valle de Los Olivos ', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 3000, 'emprendedor'),
('BFT024', 'Juana', 'Ruiz', 'Dominguez', '', 'Fisica', '2017-06-05', 'OIHA521005JJ2', 'Allende Sur 27 Col Centro', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 504, 'emprendedor'),
('BFT025', 'Rodrigo', 'Garcia', 'Zurita', '', 'Moral', '2008-08-08', 'OIHA521005JJ2', 'Avellanas 30 Valle de Los Olivos ', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 795, 'Junior'),
('BFT026', 'Miguel', 'Alcantar', 'Fernandez', '', 'Fisica', '2017-10-09', 'OIHA521005JJ2', 'Allende Sur 27 Col Centro', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 456, ' emprendedor'),
('BFT027', 'Joaquin', 'Lopez', 'Doriga', '', 'Moral', '2016-09-06', 'OIHA521005JJ2', '', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 123, 'Junior'),
('BFT028', 'Alejandro', 'Fernandez', 'Doriga', '', 'Fisica', '2016-09-04', 'OIHA521005JJ2', 'Río Ayutla 43 Col La Piedad', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 758, 'emprendedor'),
('BFT029', 'Javier', 'Dorantes', 'Zuñiga', '', 'Moral', '2017-10-06', 'OIHA521005JJ2', '', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 988, 'Junior'),
('BFT030', 'Ricardo', 'Cortes ', 'Martinez', '', 'Fisica', '2016-08-07', 'OIHA521005JJ2', 'Río Ayutla 43 Col La Piedad', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 566, 'Junior'),
('BFT040', 'Eduardo', 'Tejada', 'Flores', '', '', '2017-11-09', 'OIHA521005JJ2', '', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 266, 'emprendedor'),
('BFT041', 'David ', 'Martinez', 'Olvera', '', '', '2017-12-09', 'OIHA521005JJ2', 'Blvd Gobernadores 1001- 14 Col MonteBlanco III ', 'Querétaro', 'Querétaro', 4425678954, '', '', 'Si', 'Mensual', 233, 'Junior');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `benefactores`
--
ALTER TABLE `benefactores`
  ADD PRIMARY KEY (`IdBenefactores`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

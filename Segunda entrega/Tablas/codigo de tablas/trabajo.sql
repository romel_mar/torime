﻿-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-10-2017 a las 05:46:58
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `vertice`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajo`
--



CREATE TABLE `trabajo` (
  `IdBecario` varchar(6) NOT NULL,
  `IdTrabajo` varchar(6) NOT NULL,
  `NombreTrabajo` text,
  `Dias` text,
  `Periodo` text,
  `Horario` varchar(20) DEFAULT NULL,
  `TiempoLibre` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `trabajo`
--

INSERT INTO `trabajo` (`IdBecario`, `IdTrabajo`, `NombreTrabajo`, `Dias`, `Periodo`, `Horario`, `TiempoLibre`) VALUES
('BEC001', 'TRA001', '', '', '', '', ''),
('BEC002', 'TRA002', '', '', '', '', ''),
('BEC003', 'TRA003', '', '', '', '', ''),
('BEC004', 'TRA004', 'AYUDANTE ALBAÑIL', '', '', '', ''),
('BEC005', 'TRA005', 'NO ESPECIFICA', '', '', '', ''),
('BEC006', 'TRA006', '', '', '', '', ''),
('BEC007', 'TRA007', 'JORNALERO', '', '', '', ''),
('BEC008', 'TRA008', '', '', '', '', ''),
('BEC009', 'TRA009', '', '', '', '', ''),
('BEC010', 'TRA010', '', '', '', '', ''),
('BEC020', 'TRA020', '', '', '', '', ''),
('BEC021', 'TRA021', 'LIMPIEZA', '', '', '', ''),
('BEC022', 'TRA022', '', '', '', '', ''),
('BEC023', 'TRA023', '', '', '', '', ''),
('BEC024', 'TRA024', '', '', '', '', ''),
('BEC025', 'TRA025', '', '', '', '', ''),
('BEC026', 'TRA026', '', '', '', '', ''),
('BEC027', 'TRA027', 'TIENDA DE NOVEDADES', '', '', '', ''),
('BEC028', 'TRA028', '', '', '', '', ''),
('BEC029', 'TRA029', 'JORNALERA', '', '', '', ''),
('BEC030', 'TRA030', '', '', '', '', ''),
('BEC040', 'TRA040', '', '', '', '', ''),
('BEC041', 'TRA041', 'LIMPIEZA', '', '', '', ''),
('BEC042', 'TRA042', '', '', '', '', ''),
('BEC043', 'TRA043', 'LIMPIEZA', '', '', '', ''),
('BEC044', 'TRA044', 'CHALAN ALBAÑIL', '', '', '', ''),
('BEC045', 'TRA045', 'JORNALERO', '', '', '', ''),
('BEC046', 'TRA046', '', '', '', '', ''),
('BEC047', 'TRA047', 'EN EL CAMPO', '', '', '', ''),
('BEC048', 'TRA048', 'MANTENIMIENTO DE CARRETERA', '', '', '', ''),
('BEC049', 'TRA049', 'FÁBRICA, NO ESPECIFICA DE QUÉ', 'NO ESPECIFICA', 'VACACIONES', 'NO ESPECIFICA', 'LEER LIBROS Y REALIZAR TAREAS DE LA ESCUELA'),
('BEC050', 'TRA050', 'EN EL CAMPO O DE CHALÁN DE ALBAÑIL, CERCA DE QUERÉTARO', 'SÁBADO Y DOMINGO', 'A VECES EN PERIODO ESCOLAR, Y TODAS LAS VACACIONES', '5:00 AM A 5:00 PM', 'JUGAR FOOTBALL'),
('BEC060', 'TRA060', 'PAPELERÍA', 'SÁBADOS', 'TODO EL TIEMPO', '8:00-5:00 ', 'LEER, HACER EJERCICIO, PLATICAR CON SU MAMÁ, ESCUCHAR MÚSICA. PRACTICA TAEKWONDO'),
('BEC061', 'TRA061', '', '', '', '', 'AYUDA A SU MADRE A HACER LA LIMPIEZA DEL HOGAR'),
('BEC062', 'TRA062', 'PLANTAR VERDURAS EN EL CAMPO', 'MARTES Y JUEVES', 'VACACIONES', '7-5 PM', 'AYUDA A SU MADRE A HACER LA LIMPIEZA DEL HOGAR'),
('BEC063', 'TRA063', '', '', '', '', 'NO LAS MENCIONA'),
('BEC064', 'TRA064', 'EN UNA FÁBRICA, ES OPERADOR DE UNA MÁQUNA REBABEADORA', 'TODA LA SEMANA', 'VACACIONES', '7:30 PM A 7:30 AM', 'JUGAR FOOTBALL'),
('BEC065', 'TRA065', 'EN EL CAMPO Y A VECES EN UN CIBER', 'LUNES, MARTES, JUEVES Y SÁBADOS', 'VACACIONES', '8:00 AM A 5:00 PM', 'VER TELEVISIÓN, HACER TAREAS, ESTAR EN INTERNET Y AYUDAR A SU MAMÁ A HACER QUEHACERES DE LA CASA'),
('BEC066', 'TRA066', '', '', '', '', 'HACER EJERCICIO Y SALIR A CORRER'),
('BEC067', 'TRA067', '', '', '', '', 'A VECES VISITA A SUS TÍOS '),
('BEC068', 'TRA068', 'EN LA OBRA', 'LUNES A SÁBADO', 'VACACIONES', '8:00-5:00 PM', 'AYUDA EN EL HOGAR'),
('BEC069', 'TRA069', '', '', '', '', 'AYUDA EN LAS LABORES DE LA CASA, LE GUSTA ESCRIBIR CANCIONES, JUGAR CON SU HERMANA Y VER LA TELEVISIÓN');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `trabajo`
--
ALTER TABLE `trabajo`
  ADD PRIMARY KEY (`IdTrabajo`),
  ADD KEY `IdBecario` (`IdBecario`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `trabajo`
--
ALTER TABLE `trabajo`
  ADD CONSTRAINT `trabajo_ibfk_1` FOREIGN KEY (`IdBecario`) REFERENCES `becario` (`IdBecario`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
